[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=BeblueApp&metric=bugs)](https://sonarcloud.io/dashboard?id=BeblueApp)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=BeblueApp&metric=code_smells)](https://sonarcloud.io/dashboard?id=BeblueApp)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=BeblueApp&metric=coverage)](https://sonarcloud.io/dashboard?id=BeblueApp)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=BeblueApp&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=BeblueApp)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=BeblueApp&metric=ncloc)](https://sonarcloud.io/dashboard?id=BeblueApp)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=BeblueApp&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=BeblueApp)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=BeblueApp&metric=alert_status)](https://sonarcloud.io/dashboard?id=BeblueApp)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=BeblueApp&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=BeblueApp)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=BeblueApp&metric=security_rating)](https://sonarcloud.io/dashboard?id=BeblueApp)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=BeblueApp&metric=sqale_index)](https://sonarcloud.io/dashboard?id=BeblueApp)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=BeblueApp&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=BeblueApp)

---------------------------------------------------------------------------------------------------------

BUILD DO PROJETO - DOCKER

---------------------------------------------------------------------------------------------------------

- É necessário acessar a raiz do projeto via terminal. Depois precisa executar o comando abaixo para gerar o arquivo "jar"

./gradlew build

- Depois é só rodar a imagem utilizando o arquivo runDockerApp

sudo ./runDockerApp

---------------------------------------------------------------------------------------------------------

BUILD DO PROJETO

---------------------------------------------------------------------------------------------------------

- É necessário acessar a raiz do projeto via terminal. Depois precisa executar o comando abaixo para gerar o arquivo "jar"

./gradlew build

- depois é necessário ir até a pasta /build/libs. Na raiz do projeto é necessário executar o comando a seguir:

cd build/libs/

- Por fim, para funcionar a aplicação é necessário executar o comando abaixo:

java -jar beblueApp-0.0.1-SNAPSHOT.jar


---------------------------------------------------------------------------------------------------------

ENDPOINTS

---------------------------------------------------------------------------------------------------------

- Para iniciar a utilizar os endpoints, primeiro tem que utilizar o endpoint de login. O token irá retornar no header'Authorization'. 

POST http://{hostname}/api/private/v1/login

{
	"login":"ivo",
	"senha":"123456"
}
O token (Bearer) deverá ser passado em todas as requisições via header Authorization.

- Depois deverá consumir o endpoint de pesquisa de albuns

GET http://{hostname]/api/private/v1/album?nome=hoje&limit=30&offset=0

- Para adicionar o album ao carrinho, deverá chamar o endpoint de carrinho.

POST http://{hostname}/api/private/v1/carrinho

{
    "spotifyId": "2QgZNT9YkYYrBAtDkXHVv1",
    "quantidade":"1"
}

- Para renovar o tempo do carrinho, é necessário passar o id do carrinho.

PUT http://{hostname}/api/private/v1/carrinho

{
    "id": 21
}

Para realizar a compra é necessário chamar o endpoint abaixo, passando o id do carrinho, o tipo do pagamento e a quantidade de vezes.

POST http://{hostname}/api/private/v1/compras

{
	"idCarrinho":"488",
	"idTipoPagamento":"1",
	"qtdVezesPagamento":"1"
}

- Para buscar todas as compras do usuário, deverá chamar o seguinte endpoint. O usuário poderá passar como parametro o page e o limit

GET http://{hostname}/api/private/v1/compras?dataInicio=01/06/2019&dataFim=02/06/2019&page=0&limit=20

- Caso queira consultar somente uma compra, deverá chamar o endpoint abaixo

GET http://{hostname}/api/private/v1/compras/{idCompra}

