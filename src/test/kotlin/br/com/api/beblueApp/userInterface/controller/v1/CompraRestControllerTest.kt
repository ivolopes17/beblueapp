package br.com.api.beblueApp.userInterface.controller.v1

import br.com.api.beblueApp.application.dto.CompraDto
import br.com.api.beblueApp.application.dto.CompraResponseDto
import br.com.api.beblueApp.application.service.CompraService
import br.com.api.beblueApp.application.service.LoginService
import br.com.api.beblueApp.domain.model.LoginResponse
import br.com.api.beblueApp.userInterface.controller.model.CompraRequest
import br.com.api.beblueApp.userInterface.util.JwtUtil
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration
import org.springframework.cloud.openfeign.FeignAutoConfiguration
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status


@RunWith(SpringRunner::class)
@WebMvcTest(CompraRestController::class)
@ImportAutoConfiguration(classes =  [RibbonAutoConfiguration::class, FeignRibbonClientAutoConfiguration::class, FeignAutoConfiguration::class])
class CompraRestControllerTest {

    @Autowired
    private val mvc: MockMvc? = null

    @MockBean
    private val service: CompraService? = null

    @MockBean
    private val loginService:LoginService? = null

    @Test
    @Throws(Exception::class)
    fun pesquisarPorIdSemAutorizacaoTest(){

        mvc?.perform(
                MockMvcRequestBuilders.get("/api/private/v1/compras/{id}", 1)
                        .accept(MediaType.APPLICATION_JSON))
                ?.andDo(print())
                ?.andExpect(status().isForbidden)
    }

    @Test
    @Throws(Exception::class)
    fun consultarPorDatasSucesso(){

        val compra  = CompraDto()
        compra.id = 1
        compra.valorTotal = 10.0

        val response = LoginResponse()
        response.expires_in = 3600
        response.access_token = "ep39809802n0293n0nn80d0"
        Mockito.`when`(loginService?.autenticar()).thenReturn(response)

        given(service?.pesquisarCompras("ivo", null, null, null, null))
                .willReturn(CompraResponseDto(1, arrayListOf(compra)))

        val accessToken = JwtUtil.generateToken("ivo")
        mvc?.perform(
                MockMvcRequestBuilders.get("/api/private/v1/compras")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", accessToken))
                ?.andDo(print())
                ?.andExpect(status().isOk)
    }

    @Test
    @Throws(Exception::class)
    fun pesquisarPorIdSucessoTest(){

        val compra  = CompraDto()
        compra.id = 1
        compra.valorTotal = 10.0

        val response = LoginResponse()
        response.expires_in = 3600
        response.access_token = "ep39809802n0293n0nn80d0"
        Mockito.`when`(loginService?.autenticar()).thenReturn(response)

        given(service?.pesquisarPorId(1)).willReturn(compra)

        val accessToken = JwtUtil.generateToken("ivo")
        mvc?.perform(
                MockMvcRequestBuilders.get("/api/private/v1/compras/{id}", 1)
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", accessToken))
                ?.andDo(print())
                ?.andExpect(status().isOk)
                ?.andExpect(content().json("{\"id\":1,\"data\":null,\"valorTotal\":10.0,\"cashback\":null,\"qtdVezesPagamento\":null,\"tipoPagamento\":null}"))
    }

    @Test
    @Throws(Exception::class)
    fun pesquisarPorIdIgualZeroError(){

        val compra  = CompraDto()
        compra.id = 1
        compra.valorTotal = 10.0

        val response = LoginResponse()
        response.expires_in = 3600
        response.access_token = "ep39809802n0293n0nn80d0"
        Mockito.`when`(loginService?.autenticar()).thenReturn(response)

        given(service?.pesquisarPorId(1)).willReturn(compra)

        val accessToken = JwtUtil.generateToken("ivo")
        mvc?.perform(
                MockMvcRequestBuilders.get("/api/private/v1/compras/{id}", 0)
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", accessToken))
                ?.andDo(print())
                ?.andExpect(status().isBadRequest)
    }

    @Test
    @Throws(Exception::class)
    fun pesquisarPorIdCompraNullError(){

        val compra  = CompraDto()
        compra.id = 1
        compra.valorTotal = 10.0

        val response = LoginResponse()
        response.expires_in = 3600
        response.access_token = "ep39809802n0293n0nn80d0"
        Mockito.`when`(loginService?.autenticar()).thenReturn(response)

        given(service?.pesquisarPorId(1)).willReturn(null)

        val accessToken = JwtUtil.generateToken("ivo")
        mvc?.perform(
                MockMvcRequestBuilders.get("/api/private/v1/compras/{id}", 1)
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", accessToken))
                ?.andDo(print())
                ?.andExpect(status().isNotFound)
    }

    @Test
    @Throws(Exception::class)
    fun registrarCompra(){

        val compra  = CompraDto()
        compra.id = 1
        compra.valorTotal = 10.0

        val response = LoginResponse()
        response.expires_in = 3600
        response.access_token = "ep39809802n0293n0nn80d0"
        Mockito.`when`(loginService?.autenticar()).thenReturn(response)

        val request = CompraRequest()
        request.idCarrinho = 1
        request.idTipoPagamento = 1
        request.qtdVezesPagamento = 1

        given(service?.realizarCompra(request.idCarrinho!!, request.idTipoPagamento!!,
                request.qtdVezesPagamento!!)).willReturn(compra)

        val accessToken = JwtUtil.generateToken("ivo")
        mvc?.perform(
                MockMvcRequestBuilders.post("/api/private/v1/compras")
                        .content(asJsonString(request))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", accessToken))
                ?.andDo(print())
                ?.andExpect(status().isCreated)
                ?.andExpect(content().json("{\"id\":1,\"data\":null,\"valorTotal\":10.0,\"cashback\":null,\"qtdVezesPagamento\":null,\"tipoPagamento\":null}"))
    }

    private fun asJsonString(obj: Any): String {
        try {
            return ObjectMapper().writeValueAsString(obj)
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }

}