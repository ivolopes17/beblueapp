package br.com.api.beblueApp.userInterface.controller.v1

import br.com.api.beblueApp.application.service.AlbumService
import br.com.api.beblueApp.application.service.LoginService
import br.com.api.beblueApp.domain.model.AlbumModelResponse
import br.com.api.beblueApp.domain.model.LoginResponse
import br.com.api.beblueApp.domain.model.PesquisaModelResponse
import br.com.api.beblueApp.userInterface.util.JwtUtil
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration
import org.springframework.cloud.openfeign.FeignAutoConfiguration
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@RunWith(SpringRunner::class)
@WebMvcTest(AlbumRestController::class)
@ImportAutoConfiguration(classes =  [RibbonAutoConfiguration::class, FeignRibbonClientAutoConfiguration::class, FeignAutoConfiguration::class])
class AlbumRestControllerTest {

    @Autowired
    private val mvc: MockMvc? = null

    @MockBean
    private val service: AlbumService? = null

    @MockBean
    private val loginService: LoginService? = null


    @Test
    fun consultarPorNomeTest(){
        val responsePesquisa  = PesquisaModelResponse()
        responsePesquisa.albums = AlbumModelResponse()
        responsePesquisa.albums?.total = 0
        responsePesquisa.albums?.items = ArrayList()

        val response = LoginResponse()
        response.expires_in = 3600
        response.access_token = "ep39809802n0293n0nn80d0"
        Mockito.`when`(loginService?.autenticar()).thenReturn(response)

        given(service?.pesquisarAlbumSpotify("hoje", null, null))
                .willReturn(responsePesquisa)

        val accessToken = JwtUtil.generateToken("ivo")
        mvc?.perform(
                MockMvcRequestBuilders.get("/api/private/v1/album")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("nome", "hoje")
                        .header("Authorization", accessToken))
                ?.andDo(MockMvcResultHandlers.print())
                ?.andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    fun consultarPorNomeAlbumNNullTest(){
        val responsePesquisa  = PesquisaModelResponse()

        val response = LoginResponse()
        response.expires_in = 3600
        response.access_token = "ep39809802n0293n0nn80d0"
        Mockito.`when`(loginService?.autenticar()).thenReturn(response)

        given(service?.pesquisarAlbumSpotify("hoje", null, null))
                .willReturn(responsePesquisa)

        val accessToken = JwtUtil.generateToken("ivo")
        mvc?.perform(
                MockMvcRequestBuilders.get("/api/private/v1/album")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("nome", "hoje")
                        .header("Authorization", accessToken))
                ?.andDo(MockMvcResultHandlers.print())
                ?.andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    fun consultarPorNomeAlbumTotalNullTest(){
        val responsePesquisa  = PesquisaModelResponse()
        responsePesquisa.albums = AlbumModelResponse()

        val response = LoginResponse()
        response.expires_in = 3600
        response.access_token = "ep39809802n0293n0nn80d0"
        Mockito.`when`(loginService?.autenticar()).thenReturn(response)

        given(service?.pesquisarAlbumSpotify("hoje", null, null))
                .willReturn(responsePesquisa)

        val accessToken = JwtUtil.generateToken("ivo")
        mvc?.perform(
                MockMvcRequestBuilders.get("/api/private/v1/album")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("nome", "hoje")
                        .header("Authorization", accessToken))
                ?.andDo(MockMvcResultHandlers.print())
                ?.andExpect(MockMvcResultMatchers.status().isOk)
    }

}