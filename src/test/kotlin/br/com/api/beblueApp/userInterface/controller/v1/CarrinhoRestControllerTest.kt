package br.com.api.beblueApp.userInterface.controller.v1

import br.com.api.beblueApp.application.dto.CarrinhoDto
import br.com.api.beblueApp.application.service.CarrinhoService
import br.com.api.beblueApp.application.service.LoginService
import br.com.api.beblueApp.domain.model.LoginResponse
import br.com.api.beblueApp.userInterface.controller.model.AdicionarCarrinhoRequest
import br.com.api.beblueApp.userInterface.util.JwtUtil
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration
import org.springframework.cloud.openfeign.FeignAutoConfiguration
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@RunWith(SpringRunner::class)
@WebMvcTest(CarrinhoRestController::class)
@ImportAutoConfiguration(classes =  [RibbonAutoConfiguration::class, FeignRibbonClientAutoConfiguration::class, FeignAutoConfiguration::class])
class CarrinhoRestControllerTest {

    @Autowired
    private val mvc: MockMvc? = null

    @MockBean
    private val service: CarrinhoService? = null

    @MockBean
    private val loginService: LoginService? = null


    @Test
    fun adicionarAlbumCarrinhoTest(){

        val spotifyId = "08sdn0s8sdm0d0ds"
        val login = "ivo"

        val responseCarrinho  = CarrinhoDto()
        responseCarrinho.id = 1

        val request = AdicionarCarrinhoRequest()
        request.quantidade = 1
        request.spotifyId = spotifyId

        val response = LoginResponse()
        response.expires_in = 3600
        response.access_token = "ep39809802n0293n0nn80d0"
        Mockito.`when`(loginService?.autenticar()).thenReturn(response)

        BDDMockito.given(service?.adicionarAlbumCarrinho(spotifyId, login, 1, 40.0))
                .willReturn(responseCarrinho)

        val accessToken = JwtUtil.generateToken(login)
        mvc?.perform(
                MockMvcRequestBuilders.post("/api/private/v1/carrinho")
                        .content(asJsonString(request))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", accessToken))
                ?.andDo(MockMvcResultHandlers.print())
                ?.andExpect(MockMvcResultMatchers.status().isCreated)
    }

    @Test
    fun renovarTempoTest(){

        val login = "ivo"

        val carrinho  = CarrinhoDto()
        carrinho.id = 1

        val response = LoginResponse()
        response.expires_in = 3600
        response.access_token = "ep39809802n0293n0nn80d0"
        Mockito.`when`(loginService?.autenticar()).thenReturn(response)

        val accessToken = JwtUtil.generateToken(login)
        mvc?.perform(
                MockMvcRequestBuilders.put("/api/private/v1/carrinho")
                        .content(asJsonString(carrinho))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization", accessToken))
                ?.andDo(MockMvcResultHandlers.print())
                ?.andExpect(MockMvcResultMatchers.status().isOk)
    }

    private fun asJsonString(obj: Any): String {
        try {
            return ObjectMapper().writeValueAsString(obj)
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }
}