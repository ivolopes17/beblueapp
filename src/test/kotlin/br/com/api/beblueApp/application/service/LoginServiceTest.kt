package br.com.api.beblueApp.application.service

import br.com.api.beblueApp.application.infrastructurePort.LoginRepositoryPort
import br.com.api.beblueApp.application.infrastructurePort.UsuarioRepositoryPort
import br.com.api.beblueApp.domain.factory.UsuarioFactory
import br.com.api.beblueApp.infrastructure.adapter.LoginRepositoryAdapter
import br.com.api.beblueApp.infrastructure.adapter.UsuarioRepositoryAdapter
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.Mockito.`when` as mockitoWhen

class LoginServiceTest {

    private var usuarioRepository: UsuarioRepositoryPort? = null
    private var loginRepository: LoginRepositoryPort? = null
    private var service: LoginService? = null

    @Before
    fun setup() {
        usuarioRepository = Mockito.mock(UsuarioRepositoryAdapter::class.java)
        loginRepository = Mockito.mock(LoginRepositoryAdapter::class.java)
        service = LoginService(loginRepository!!, usuarioRepository!!)
    }

    @Test
    fun findByLoginSucess(){
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        mockitoWhen(usuarioRepository?.findByLogin(ArgumentMatchers.anyString())).thenReturn(usuario)
        service?.findByLogin(usuario.login!!)
    }

}