package br.com.api.beblueApp.application.events

import br.com.api.beblueApp.application.infrastructurePort.AuditoriaCashbackRepositoryPort
import br.com.api.beblueApp.domain.enums.TipoCashbackAudit
import br.com.api.beblueApp.domain.factory.*
import br.com.api.beblueApp.infrastructure.adapter.AuditoriaCashbackRepositoryAdapter
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import java.util.*
import org.mockito.Mockito.`when` as mockitoWhen

class CompraCriadaEventListenerTest {

    private var auditoriaRepository: AuditoriaCashbackRepositoryPort? = null

    private var event: CompraCriadaEventListener? = null

    @Suppress("UNCHECKED_CAST")
    private fun <T> uninitialized(): T = null as T

    private fun <T> anyObject(): T {
        Mockito.any<T>()
        return uninitialized()
    }

    @Before
    fun setup() {
        auditoriaRepository = Mockito.mock(AuditoriaCashbackRepositoryAdapter::class.java)
        event = CompraCriadaEventListener(auditoriaRepository!!)
    }

    @Test
    fun onApplicationEventCashbackMenorUmTest(){
        val pagamento = TipoPagamentoFactory.criar(2, "Cartão")
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)

        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)

        val compra = CompraFactory.criar(21, Date(), 40.0, 0.0, 1, pagamento, carrinho)

        compra.aumentarSaldoUsuario(0.0)

        val compraCriadaEvent = CompraCriadaEvent(usuario,compra)
        event?.onApplicationEvent(compraCriadaEvent)
    }

    @Test
    fun onApplicationEventCashbackTest(){
        val pagamento = TipoPagamentoFactory.criar(2, "Cartão")
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)

        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)

        val compra = CompraFactory.criar(21, Date(), 40.0, 10.0, 1, pagamento, carrinho)

        compra.aumentarSaldoUsuario(0.0)

        val compraCriadaEvent = CompraCriadaEvent(usuario,compra)

        val calendar = Calendar.getInstance(TimeZone.getTimeZone("America/Sao_Paulo"))
        val auditoria = AuditoriaCashbackFactory.criar(1, calendar.time, compra.cashback, TipoCashbackAudit.ENTRADA, compra)

        mockitoWhen(auditoriaRepository?.salvar(anyObject())).thenReturn(auditoria)

        event?.onApplicationEvent(compraCriadaEvent)
    }

}