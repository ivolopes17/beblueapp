package br.com.api.beblueApp.application.service

import br.com.api.beblueApp.application.infrastructurePort.*
import br.com.api.beblueApp.domain.entity.Ativo
import br.com.api.beblueApp.domain.entity.Carrinho
import br.com.api.beblueApp.domain.factory.*
import br.com.api.beblueApp.domain.model.AlbumItemModelResponse
import br.com.api.beblueApp.domain.model.ArtistaItemModelResponse
import br.com.api.beblueApp.infrastructure.adapter.*
import br.com.api.beblueApp.userInterface.exceptions.BeblueBadRequestException
import br.com.api.beblueApp.userInterface.exceptions.BeblueNotAcceptableException
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.modelmapper.ModelMapper
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests
import java.util.*
import org.mockito.Mockito.`when` as mockitoWhen

class CarrinhoServiceTest:AbstractTestNGSpringContextTests() {

    private var usuarioRepository: UsuarioRepositoryPort? = null
    private var albumRepository: AlbumRepositoryPort? = null
    private var artistaRepository: ArtistaRepositoryPort? = null
    private var generoRepository: GeneroRepositoryPort? = null
    private var repository: CarrinhoRepositoryPort? = null
    private var carrinhoService: CarrinhoService? = null

    @Suppress("UNCHECKED_CAST")
    private fun <T> uninitialized(): T = null as T

    private fun <T> anyObject(): T {
        Mockito.any<T>()
        return uninitialized()
    }

    @Before
    fun setup() {
        usuarioRepository = Mockito.mock(UsuarioRepositoryAdapter::class.java)
        albumRepository = Mockito.mock(AlbumRepositoryAdapter::class.java)
        artistaRepository = Mockito.mock(ArtistaRepositoryAdapter::class.java)
        generoRepository = Mockito.mock(GeneroRepositoryAdapter::class.java)
        repository = Mockito.mock(CarrinhoRepositoryAdapter::class.java)
        carrinhoService = CarrinhoService(usuarioRepository!!, albumRepository!!, artistaRepository!!, generoRepository!!,
                    repository!!, ModelMapper())
    }

    @Test
    fun adicionarSemExistirCarrinhoTest(){
        val spotifyIdAlbum = "537cWg3a0Fn9zmPllBPMGh"
        val albumResponse = AlbumItemModelResponse(spotifyIdAlbum, "Hoje")

        val artistaResponse = ArtistaItemModelResponse("938fjjdfofjdo833", "João", arrayListOf("mpb", "rock"))

        albumResponse.artists = arrayListOf(artistaResponse)
        mockitoWhen(albumRepository?.pesquisarAlbumSpotifyPorId(spotifyIdAlbum) ).thenReturn(albumResponse)

        val genero1 = GeneroFactory.criar(1, "mpb")

        val genero2 = GeneroFactory.criar(2, "rock")

        val artista = ArtistaFactory.criar(21, artistaResponse.name, artistaResponse.id)

        val artistaGenero1 = ArtistaGeneroFactory.criar(31, genero1, artista)

        val artistaGenero2 = ArtistaGeneroFactory.criar(32, genero2, artista)

        artista.adicionarGeneros(artistaGenero1)
        artista.adicionarGeneros(artistaGenero2)

        val album = AlbumFactory.criar(12, albumResponse.name, albumResponse.id, artista)

        mockitoWhen(albumRepository?.consultarPorSpotifyId(albumResponse.id!!) ).thenReturn(album)

        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)

        mockitoWhen(albumRepository?.consultarPorSpotifyId(albumResponse.id!!) ).thenReturn(album)
        mockitoWhen(usuarioRepository?.findByLogin("ivo")).thenReturn(usuario)

        val carrinho = CarrinhoFactory.criar(1, Date(), usuario)

        mockitoWhen(repository?.findByAtivoAndUsuarioId(Ativo(1), usuario.id!!)).thenReturn(null)

        mockitoWhen(repository?.salvar(anyObject())).thenReturn(carrinho)

        val result = carrinhoService?.adicionarAlbumCarrinho(spotifyIdAlbum, usuario.login!!, 2 , 40.0)

        Assertions.assertThat(result?.id).isEqualTo(carrinho.id)
    }

    @Test
    fun adicionarQauntidadeNullTest(){

        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val spotifyIdAlbum = "537cWg3a0Fn9zmPllBPMGh"

        try {

            carrinhoService?.adicionarAlbumCarrinho(spotifyIdAlbum, usuario.login!!, 0, 40.0)

        }catch (e:BeblueBadRequestException){
            Assertions.assertThat(e.message).isEqualTo("A quantidade tem que ser maior que zero")
        }

    }

    @Test
    fun adicionarAlbumIdNullTest(){
        val spotifyIdAlbum = "537cWg3a0Fn9zmPllBPMGh"
        val albumResponse = AlbumItemModelResponse(null, "Hoje")

        val artistaResponse = ArtistaItemModelResponse("938fjjdfofjdo833", "João", arrayListOf("mpb", "rock"))

        albumResponse.artists = arrayListOf(artistaResponse)
        mockitoWhen(albumRepository?.pesquisarAlbumSpotifyPorId(spotifyIdAlbum) ).thenReturn(albumResponse)

        try{
            carrinhoService?.adicionarAlbumCarrinho(spotifyIdAlbum, "ivo", 2, 20.0)
        }catch (e:BeblueBadRequestException){
            Assertions.assertThat(e.message).isEqualTo("O campo spotifyId é inválido")
        }

    }

    @Test
    fun adicionarAlbumNullTest(){
        val spotifyIdAlbum = "537cWg3a0Fn9zmPllBPMGh"
        val albumResponse = AlbumItemModelResponse(spotifyIdAlbum, "Hoje")

        val artistaResponse = ArtistaItemModelResponse("938fjjdfofjdo833", "João", arrayListOf("mpb", "rock"))

        albumResponse.artists = arrayListOf(artistaResponse)
        mockitoWhen(albumRepository?.pesquisarAlbumSpotifyPorId(spotifyIdAlbum) ).thenReturn(albumResponse)

        val genero1 = GeneroFactory.criar(1, "mpb")

        val genero2 = GeneroFactory.criar(2, "rock")

        val artista = ArtistaFactory.criar(21, artistaResponse.name, artistaResponse.id)

        val artistaGenero1 = ArtistaGeneroFactory.criar(31, genero1, artista)

        val artistaGenero2 = ArtistaGeneroFactory.criar(32, genero2, artista)

        artista.adicionarGeneros(artistaGenero1)
        artista.adicionarGeneros(artistaGenero2)

        mockitoWhen(albumRepository?.consultarPorSpotifyId(albumResponse.id!!) ).thenReturn(null)

        mockitoWhen(artistaRepository?.pesquisarArtistaSpotifyPorId(artistaResponse.id!!)).thenReturn(artistaResponse)

        mockitoWhen( artistaRepository?.consultarPorSpotifyId(artistaResponse.id!!) ).thenReturn(artista)

        val album = AlbumFactory.criar(12, albumResponse.name, albumResponse.id, artista)

        mockitoWhen( albumRepository?.salvar(anyObject()) ).thenReturn(album)

        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)

        mockitoWhen(usuarioRepository?.findByLogin("ivo")).thenReturn(usuario)

        val carrinho = CarrinhoFactory.criar(1, Date(), usuario)

        mockitoWhen(repository?.findByAtivoAndUsuarioId(Ativo(1), usuario.id!!)).thenReturn(null)

        mockitoWhen(repository?.salvar(anyObject())).thenReturn(carrinho)

        val result = carrinhoService?.adicionarAlbumCarrinho(spotifyIdAlbum, usuario.login!!, 2 , 40.0)

        Assertions.assertThat(result?.id).isEqualTo(carrinho.id)
    }

    @Test
    fun adicionarAlbumNullArtistaNullTest(){
        val spotifyIdAlbum = "537cWg3a0Fn9zmPllBPMGh"
        val albumResponse = AlbumItemModelResponse(spotifyIdAlbum, "Hoje")

        val artistaResponse = ArtistaItemModelResponse("938fjjdfofjdo833", "João", arrayListOf("mpb"))

        albumResponse.artists = arrayListOf(artistaResponse)
        mockitoWhen(albumRepository?.pesquisarAlbumSpotifyPorId(spotifyIdAlbum) ).thenReturn(albumResponse)

        val genero1 = GeneroFactory.criar(1, "mpb")

        val genero2 = GeneroFactory.criar(2, "rock")

        val artista = ArtistaFactory.criar(21, artistaResponse.name, artistaResponse.id)

        val artistaGenero1 = ArtistaGeneroFactory.criar(31, genero1, artista)

        val artistaGenero2 = ArtistaGeneroFactory.criar(32, genero2, artista)

        artista.adicionarGeneros(artistaGenero1)
        artista.adicionarGeneros(artistaGenero2)

        mockitoWhen(albumRepository?.consultarPorSpotifyId(albumResponse.id!!) ).thenReturn(null)

        mockitoWhen(artistaRepository?.pesquisarArtistaSpotifyPorId(artistaResponse.id!!)).thenReturn(artistaResponse)

        mockitoWhen( artistaRepository?.consultarPorSpotifyId(artistaResponse.id!!) ).thenReturn(null)

        mockitoWhen( artistaRepository?.salvar(anyObject()) ).thenReturn(artista)

        mockitoWhen( generoRepository?.pesquisarPorNome(anyObject()) ).thenReturn(genero1)

        val album = AlbumFactory.criar(12, albumResponse.name, albumResponse.id, artista)

        mockitoWhen( albumRepository?.salvar(anyObject()) ).thenReturn(album)

        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)

        mockitoWhen(usuarioRepository?.findByLogin("ivo")).thenReturn(usuario)

        val carrinho = CarrinhoFactory.criar(1, Date(), usuario)

        mockitoWhen(repository?.findByAtivoAndUsuarioId(Ativo(1), usuario.id!!)).thenReturn(null)

        mockitoWhen(repository?.salvar(anyObject())).thenReturn(carrinho)

        val result = carrinhoService?.adicionarAlbumCarrinho(spotifyIdAlbum, usuario.login!!, 2 , 40.0)

        Assertions.assertThat(result?.id).isEqualTo(carrinho.id)
    }

    @Test
    fun renovarTempoIdNull(){
        try{
            carrinhoService?.renovarTempo(null)
        }catch (e: BeblueBadRequestException){
            Assertions.assertThat(e.message).isEqualTo("O campo id é nulo ou inválido")
        }
    }

    @Test
    fun renovarTempoIdMenorIgualZero(){
        try{
            carrinhoService?.renovarTempo(0)
        }catch (e: BeblueBadRequestException){
            Assertions.assertThat(e.message).isEqualTo("O campo id é nulo ou inválido")
        }
    }

    @Test
    fun renovarTempoNaoEncontrado(){

        mockitoWhen(repository?.pesquisarPorId(1)).thenReturn(null)

        try{
            carrinhoService?.renovarTempo(1)
        }catch (e: BeblueNotAcceptableException){
            Assertions.assertThat(e.message).isEqualTo("Não foi encontrado nenhum carrinho para o id informado")
        }
    }

    @Test
    fun renovarTempoSucesso(){

        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(1, Date(), usuario)
        mockitoWhen(repository?.pesquisarPorId(ArgumentMatchers.anyInt(), anyObject())).thenReturn(carrinho)

        mockitoWhen(repository?.salvar(anyObject())).thenReturn(carrinho)

        carrinhoService?.renovarTempo(1)
    }

    @Test
    fun pesquisarGeneroAddNull(){
        val genero = GeneroFactory.criar(1, "mpb")
        mockitoWhen(generoRepository?.pesquisarPorNome(anyObject())).thenReturn(null)
        mockitoWhen(generoRepository?.salvar(anyObject())).thenReturn(genero)
        carrinhoService?.pesquisarGeneroAdd("mpb")
    }

    @Test
    fun pesquisarArtistaIdNull(){
        try{
            carrinhoService?.pesquisarArtista(ArtistaItemModelResponse())
        }catch (e:BeblueBadRequestException){
            Assertions.assertThat(e.message).isEqualTo("Problema ao recuperar o artista pela api da Spotify")
        }
    }

    @Test
    fun pesquisarUsuarioNull(){
        try {
            mockitoWhen(usuarioRepository?.findByLogin(anyObject())).thenReturn(null)
            carrinhoService?.pesquisarUsuario("ivo")
        }catch (e:BeblueBadRequestException){
            Assertions.assertThat(e.message).isEqualTo("Usuário inválido")
        }
    }

    @Test
    fun inserirValidandoSucesso(){
        val cal = Calendar.getInstance()
        cal.set(Calendar.YEAR, 2018)
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = Carrinho(1, Date(), usuario, Ativo(1), cal.time)

        mockitoWhen(repository?.findByAtivoAndUsuarioId(anyObject(), ArgumentMatchers.anyInt())).thenReturn(arrayListOf(carrinho))
        mockitoWhen(repository?.salvar(anyObject())).thenReturn(carrinho)

        carrinhoService?.inserirValidando(usuario)
    }

}