package br.com.api.beblueApp.application.service

import br.com.api.beblueApp.application.infrastructurePort.AlbumRepositoryPort
import br.com.api.beblueApp.domain.factory.AlbumFactory
import br.com.api.beblueApp.domain.factory.ArtistaFactory
import br.com.api.beblueApp.domain.model.AlbumModelResponse
import br.com.api.beblueApp.domain.model.PesquisaModelResponse
import br.com.api.beblueApp.infrastructure.adapter.AlbumRepositoryAdapter
import br.com.api.beblueApp.userInterface.exceptions.BeblueBadRequestException
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests
import org.mockito.Mockito.`when` as mockitoWhen

class AlbumServiceTest: AbstractTestNGSpringContextTests()  {

    private var albumRepository:AlbumRepositoryPort? = null

    private var service:AlbumService? = null

    @Suppress("UNCHECKED_CAST")
    private fun <T> uninitialized(): T = null as T

    private fun <T> anyObject(): T {
        Mockito.any<T>()
        return uninitialized()
    }

    @Before
    fun setup() {
        albumRepository = Mockito.mock(AlbumRepositoryAdapter::class.java)
        service = AlbumService(albumRepository!!)
    }

    @Test
    fun salvarTest(){
        val artista = ArtistaFactory.criar(21, "teste", "9sd8und9s8udsn9sd")
        val album = AlbumFactory.criar(1, "joao", "oisd09sdidmis0", artista)
        mockitoWhen(albumRepository?.salvar(anyObject())).thenReturn(album)
        service?.salvar(album)
    }

    @Test
    fun pesquisarAlbumSpotifyLimitNull(){
        service?.pesquisarAlbumSpotify("98e9mdme9deme8ued", null, 0)
    }

    @Test
    fun pesquisarAlbumSpotifyLimitMenorUm(){
        try{
            service?.pesquisarAlbumSpotify("98e9mdme9deme8ued", 0, 0)
        }catch (e:BeblueBadRequestException){
            Assertions.assertThat(e.message).isEqualTo("O parâmetro limit deve ser maior que 0 e menor que 51")
        }
    }

    @Test
    fun pesquisarAlbumSpotifyLimitMaior(){
        try{
            service?.pesquisarAlbumSpotify("98e9mdme9deme8ued", 51, 0)
        }catch (e:BeblueBadRequestException){
            Assertions.assertThat(e.message).isEqualTo("O parâmetro limit deve ser maior que 0 e menor que 51")
        }
    }

    @Test
    fun pesquisarAlbumSpotifyOffsetNull(){
        service?.pesquisarAlbumSpotify("98e9mdme9deme8ued", 20, null)
    }

    @Test
    fun pesquisarAlbumSpotifyOffsetMaior(){
        try{
            service?.pesquisarAlbumSpotify("98e9mdme9deme8ued", 20, 10001)
        }catch (e:BeblueBadRequestException){
            Assertions.assertThat(e.message).isEqualTo("O parâmetro offset deve ser menor ou igual a 10000")
        }
    }

    @Test
    fun pesquisarAlbumSpotifySucesso(){

        val album = PesquisaModelResponse()
        album.albums = AlbumModelResponse()
        mockitoWhen(albumRepository?.pesquisarAlbumSpotify(ArgumentMatchers.anyString(), ArgumentMatchers.anyInt()
                        , ArgumentMatchers.anyInt()) ).thenReturn(album)
        service?.pesquisarAlbumSpotify("98e9mdme9deme8ued", 20, 0)
    }

}