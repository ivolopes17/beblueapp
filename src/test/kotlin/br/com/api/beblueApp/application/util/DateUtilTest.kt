package br.com.api.beblueApp.application.util

import org.assertj.core.api.Assertions
import org.junit.Test
import java.util.*

class DateUtilTest:DateUtil(){

    @Test
    fun testDateEqual(){
        val cal1 = Calendar.getInstance()
        val cal2 = Calendar.getInstance()
        val result = isSameDay(cal1, cal2)
        Assertions.assertThat(result).isEqualTo(true)
    }

    @Test
    fun testDayDifferent(){
        val cal1 = Calendar.getInstance()
        cal1.set(Calendar.DAY_OF_MONTH, 11)
        cal1.set(Calendar.YEAR, 2010)
        val cal2 = Calendar.getInstance()
        cal2.set(Calendar.DAY_OF_MONTH, 12)
        cal2.set(Calendar.YEAR, 2010)
        val result = isSameDay(cal1, cal2)
        Assertions.assertThat(result).isEqualTo(false)
    }

    @Test
    fun testMonthDifferent(){
        val cal1 = Calendar.getInstance()
        cal1.set(Calendar.YEAR, 2010)
        val cal2 = Calendar.getInstance()
        cal2.set(Calendar.MONTH, 8)
        cal2.set(Calendar.YEAR, 2010)
        val result = isSameDay(cal1, cal2)
        Assertions.assertThat(result).isEqualTo(false)
    }

    @Test
    fun testYearDifferent(){
        val cal1 = Calendar.getInstance()
        val cal2 = Calendar.getInstance()
        cal2.set(Calendar.YEAR, 2020)
        val result = isSameDay(cal1, cal2)
        Assertions.assertThat(result).isEqualTo(false)
    }

}