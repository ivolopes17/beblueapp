package br.com.api.beblueApp.application.service

import br.com.api.beblueApp.application.infrastructurePort.*
import br.com.api.beblueApp.domain.entity.Compra
import br.com.api.beblueApp.domain.enums.TipoCashbackAudit
import br.com.api.beblueApp.domain.factory.*
import br.com.api.beblueApp.infrastructure.adapter.*
import br.com.api.beblueApp.userInterface.exceptions.BeblueBadRequestException
import br.com.api.beblueApp.userInterface.exceptions.BeblueInternalErrorException
import br.com.api.beblueApp.userInterface.exceptions.BeblueNotAcceptableException
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.modelmapper.ModelMapper
import org.springframework.context.ApplicationEventPublisher
import org.springframework.data.domain.PageImpl
import java.util.*
import org.mockito.Mockito.`when` as mockitoWhen

class CompraServiceTest {

    private var compraRepository: CompraRepositoryPort? = null

    private var carrinhoRepository: CarrinhoRepositoryPort? = null

    private var tipoPagamentoRepository: TipoPagamentoRepositoryPort? = null

    private var percentualRepository: PercentualCashbackRepositoryPort? = null

    private var auditoriaRepository: AuditoriaCashbackRepositoryPort? = null

    private var diaRepository: DiaRepositoryPort? = null

    private var usuarioRepository: UsuarioRepositoryPort? = null

    private var compraService: CompraService? = null

    private var applicationEventPublisher:ApplicationEventPublisher? = null

    @Suppress("UNCHECKED_CAST")
    private fun <T> uninitialized(): T = null as T

    private fun <T> anyObject(): T {
        Mockito.any<T>()
        return uninitialized()
    }

    @Before
    fun setup() {
        compraRepository = Mockito.mock(CompraRepositoryAdapter::class.java)
        carrinhoRepository = Mockito.mock(CarrinhoRepositoryAdapter::class.java)
        tipoPagamentoRepository = Mockito.mock(TipoPagamentoRepositoryAdapter::class.java)
        percentualRepository = Mockito.mock(PercentualCashbackRepositoryAdapter::class.java)
        auditoriaRepository = Mockito.mock(AuditoriaCashbackRepositoryAdapter::class.java)
        diaRepository = Mockito.mock(DiaRepositoryAdapter::class.java)
        usuarioRepository = Mockito.mock(UsuarioRepositoryAdapter::class.java)
        applicationEventPublisher = Mockito.mock(ApplicationEventPublisher::class.java)
        compraService = CompraService(compraRepository!!, carrinhoRepository!!, tipoPagamentoRepository!!, percentualRepository!!,
                        diaRepository!!, usuarioRepository!!, applicationEventPublisher!!, ModelMapper())
    }

    @Test
    @Throws(Exception::class)
    fun realizarCompraCarrinhoIdcarrinhoErrorTest(){

        try {
            compraService?.realizarCompra(0, 2, 1)
        }catch (e: BeblueBadRequestException){
            Assertions.assertThat(e.message).isEqualTo("O campo do idCarrinho está vazio ou é inválido")
        }
    }

    @Test
    @Throws(Exception::class)
    fun realizarCompraCarrinhoIdPagamentoErrorTest(){

        try {
            compraService?.realizarCompra(1, 0, 1)
        }catch (e: BeblueBadRequestException){
            Assertions.assertThat(e.message).isEqualTo("O campo do idTipoPagamento está vazio ou é inválido")
        }
    }

    @Test
    @Throws(Exception::class)
    fun realizarCompraCarrinhoQtdPagamentoErrorTest(){

        try {
            compraService?.realizarCompra(1, 1, 0)
        }catch (e: BeblueBadRequestException){
            Assertions.assertThat(e.message).isEqualTo("O campo do qtdVezesPagamento está vazio ou é inválido")
        }
    }

    @Test
    @Throws(Exception::class)
    fun realizarCompraPagamentoNaoEncontradoTest(){

        mockitoWhen(tipoPagamentoRepository?.pesquisarPorId(4)).thenReturn(null)

        try {
            compraService?.realizarCompra(12, 4, 1)
        }catch (ex: BeblueNotAcceptableException){
            Assertions.assertThat(true).isEqualTo(true)
        }
    }

    @Test
    @Throws(Exception::class)
    fun realizarCompraCarrinhoNaoEncontradoTest(){

        mockitoWhen(tipoPagamentoRepository?.pesquisarPorId(2)).thenReturn(TipoPagamentoFactory.criar(2, "Cartão"))
        mockitoWhen(carrinhoRepository?.pesquisarPorId(12)).thenReturn(null)

        try {
            compraService?.realizarCompra(12, 2, 1)
        }catch (ex: BeblueNotAcceptableException){
            Assertions.assertThat(true).isEqualTo(true)
        }
    }

    @Test
    @Throws(Exception::class)
    fun realizarCompraDiaNullTest(){

        val pagamento = TipoPagamentoFactory.criar(2, "Cartão")
        mockitoWhen(tipoPagamentoRepository?.pesquisarPorId(2)).thenReturn(pagamento)

        val genero = GeneroFactory.criar(1, "mpb")

        val artista = ArtistaFactory.criar(1, "Caetano", "98329088jjjj2")

        val artistaGenero = ArtistaGeneroFactory.criar(1, genero, artista)

        artista.adicionarGeneros(artistaGenero)

        val album = AlbumFactory.criar(1, "Hoje", "832hds9y2h2397sd", artista)

        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)

        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)

        carrinho.adicionarAlbum(album, 1, 40.0)

        mockitoWhen(carrinhoRepository?.pesquisarPorId(ArgumentMatchers.anyInt(), anyObject())).thenReturn(carrinho)

        val calendar = Calendar.getInstance(TimeZone.getTimeZone("America/Sao_Paulo"))

        mockitoWhen(diaRepository?.pesquisarPorId(calendar.get(Calendar.DAY_OF_WEEK))).thenReturn(null)

        try {
            compraService?.realizarCompra(12, 2, 1)
        }catch (e:java.lang.Exception){
            Assertions.assertThat(e.message).isEqualTo("Ocorreu algum problema ao realizar a compra")
        }
    }

    @Test
    @Throws(Exception::class)
    fun realizarCompraSucessoTest(){

        val pagamento = TipoPagamentoFactory.criar(2, "Cartão")
        mockitoWhen(tipoPagamentoRepository?.pesquisarPorId(2)).thenReturn(pagamento)

        val genero = GeneroFactory.criar(1, "mpb")

        val artista = ArtistaFactory.criar(1, "Caetano", "98329088jjjj2")

        val artistaGenero = ArtistaGeneroFactory.criar(1, genero, artista)

        artista.adicionarGeneros(artistaGenero)

        val album = AlbumFactory.criar(1, "Hoje", "832hds9y2h2397sd", artista)

        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)

        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)

        carrinho.adicionarAlbum(album, 1, 40.0)

        mockitoWhen(carrinhoRepository?.pesquisarPorId(ArgumentMatchers.anyInt(), anyObject())).thenReturn(carrinho)

        val calendar = Calendar.getInstance(TimeZone.getTimeZone("America/Sao_Paulo"))
        val dia = DiaFactory.criar(calendar.get(Calendar.DAY_OF_WEEK), "Dia da semana")

        mockitoWhen(diaRepository?.pesquisarPorId(calendar.get(Calendar.DAY_OF_WEEK))).thenReturn(dia)

        val percentual = PercentualCashbackFactory.criar(1, 30, dia, genero)

        mockitoWhen(percentualRepository?.pesquisarPorDiaGenero(dia, genero)).thenReturn(percentual)

        val cashback = 12.0
        val compra = CompraFactory.criar(21, Date(), 40.0, cashback, 1, pagamento, carrinho)

        compra.aumentarSaldoUsuario(cashback)

        mockitoWhen(compraRepository?.salvar(anyObject())).thenReturn(compra)

        val auditoria = AuditoriaCashbackFactory.criar(12, Date(), cashback, TipoCashbackAudit.ENTRADA, compra)

        mockitoWhen(auditoriaRepository?.salvar(anyObject())).thenReturn(auditoria)

        val compraAux = compraService?.realizarCompra(12, 2, 1)!!

        Assertions.assertThat(compraAux.id).isEqualTo(21)
    }

    @Test
    @Throws(Exception::class)
    fun realizarCompraSemAlbumTest(){

        val pagamento = TipoPagamentoFactory.criar(2, "Cartão")
        mockitoWhen(tipoPagamentoRepository?.pesquisarPorId(2)).thenReturn(pagamento)

        val genero = GeneroFactory.criar(1, "mpb")

        val artista = ArtistaFactory.criar(1, "Caetano", "98329088jjjj2")

        val artistaGenero = ArtistaGeneroFactory.criar(1, genero, artista)

        artista.adicionarGeneros(artistaGenero)

        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)

        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)

        mockitoWhen(carrinhoRepository?.pesquisarPorId(ArgumentMatchers.anyInt(), anyObject())).thenReturn(carrinho)

        val calendar = Calendar.getInstance(TimeZone.getTimeZone("America/Sao_Paulo"))
        val dia = DiaFactory.criar(calendar.get(Calendar.DAY_OF_WEEK), "Dia da semana")

        mockitoWhen(diaRepository?.pesquisarPorId(calendar.get(Calendar.DAY_OF_WEEK))).thenReturn(dia)

        val percentual = PercentualCashbackFactory.criar(1, 30, dia, genero)

        mockitoWhen(percentualRepository?.pesquisarPorDiaGenero(dia, genero)).thenReturn(percentual)

        val cashback = 12.0
        val compra = CompraFactory.criar(21, Date(), 40.0, cashback, 1, pagamento, carrinho)

        compra.aumentarSaldoUsuario(cashback)

        mockitoWhen(compraRepository?.salvar(anyObject())).thenReturn(compra)

        val auditoria = AuditoriaCashbackFactory.criar(12, Date(), cashback, TipoCashbackAudit.ENTRADA, compra)

        mockitoWhen(auditoriaRepository?.salvar(anyObject())).thenReturn(auditoria)

        try {
            compraService?.realizarCompra(12, 2, 1)!!
        }catch (e:BeblueInternalErrorException) {
            Assertions.assertThat(e.message).isEqualTo("A compra não possui album")
        }
    }

    @Test
    fun pesquisarPorIdSucesso(){
        val pagamento = TipoPagamentoFactory.criar(2, "Cartão")
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val compra = CompraFactory.criar(21, Date(), 40.0, 5.9, 1, pagamento, carrinho)
        mockitoWhen(compraRepository?.findById(ArgumentMatchers.anyInt())).thenReturn(compra)
        val compraDto = compraService?.pesquisarPorId(1)

        Assertions.assertThat(compraDto?.id).isEqualTo(21)
    }

    @Test
    fun pesquisarPorIdNull(){
        mockitoWhen(compraRepository?.findById(ArgumentMatchers.anyInt())).thenReturn(null)
        val compraDto = compraService?.pesquisarPorId(1)

        Assertions.assertThat(compraDto).isEqualTo(null)
    }

    @Test
    fun pesquisarComprasLimitMenorUm(){
        try{
            compraService?.pesquisarCompras("ivo", Date(), Date(), 0, 0)
        }catch (e:BeblueBadRequestException){
            Assertions.assertThat(e.message).isEqualTo("O parâmetro limit deve ser maior que 0")
        }
    }

    @Test
    fun pesquisarComprasPageMenorZero(){
        try{
            compraService?.pesquisarCompras("ivo", Date(), Date(), -1, 1)
        }catch (e:BeblueBadRequestException){
            Assertions.assertThat(e.message).isEqualTo("O parâmetro page deve ser maior ou igual a 0")
        }
    }

    @Test
    fun pesquisarComprasDataFimMenor(){
        val cal = Calendar.getInstance()
        cal.set(Calendar.YEAR, 2018)
        try{
            compraService?.pesquisarCompras("ivo", Date(), cal.time, 0, 1)
        }catch (e:BeblueBadRequestException){
            Assertions.assertThat(e.message).isEqualTo("O campo dataFim tem que ser maior que o campo dataInicio")
        }
    }

    @Test
    fun pesquisarComprasSucesso(){
        val cal = Calendar.getInstance()
        cal.set(Calendar.YEAR, cal.get(Calendar.YEAR)+1)

        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)

        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val pagamento = TipoPagamentoFactory.criar(2, "Cartão")
        val compra = CompraFactory.criar(21, Date(), 40.0, 5.0, 1, pagamento, carrinho)
        val list = arrayListOf(compra)
        val page = PageImpl<Compra>(list)

        mockitoWhen(usuarioRepository?.findByLogin(ArgumentMatchers.anyString())).thenReturn(usuario)

        mockitoWhen(compraRepository?.findAllByDatasAndUsuario(anyObject(), anyObject(),
                ArgumentMatchers.anyInt(), anyObject())).thenReturn(page)

        val result = compraService?.pesquisarCompras("ivo", Date(), cal.time, 0, 1)

        Assertions.assertThat(result?.compras?.size).isEqualTo(1)
    }

    @Test
    fun pesquisarComprasNullPageOffsetSucesso(){
        val cal = Calendar.getInstance()
        cal.set(Calendar.YEAR, cal.get(Calendar.YEAR)+1)

        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)

        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val pagamento = TipoPagamentoFactory.criar(2, "Cartão")
        val compra = CompraFactory.criar(21, Date(), 40.0, 5.0, 1, pagamento, carrinho)
        val list = arrayListOf(compra)
        val page = PageImpl<Compra>(list)

        mockitoWhen(usuarioRepository?.findByLogin(ArgumentMatchers.anyString())).thenReturn(usuario)

        mockitoWhen(compraRepository?.findAllByDatasAndUsuario(anyObject(), anyObject(),
                ArgumentMatchers.anyInt(), anyObject())).thenReturn(page)

        val result = compraService?.pesquisarCompras("ivo", Date(), cal.time, null, null)

        Assertions.assertThat(result?.compras?.size).isEqualTo(1)
    }


    @Test
    fun pesquisarComprasNullDatesSucesso(){
        val cal = Calendar.getInstance()
        cal.set(Calendar.YEAR, cal.get(Calendar.YEAR)+1)

        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)

        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val pagamento = TipoPagamentoFactory.criar(2, "Cartão")
        val compra = CompraFactory.criar(21, Date(), 40.0, 5.0, 1, pagamento, carrinho)
        val list = arrayListOf(compra)
        val page = PageImpl<Compra>(list)

        mockitoWhen(usuarioRepository?.findByLogin(ArgumentMatchers.anyString())).thenReturn(usuario)

        mockitoWhen(compraRepository?.findAllByUsuarioId(ArgumentMatchers.anyInt(), anyObject())).thenReturn(page)

        val result = compraService?.pesquisarCompras("ivo", null, null, 0, 10)

        Assertions.assertThat(result?.compras?.size).isEqualTo(1)
    }

    @Test
    fun pesquisarComprasNullDataFimSucesso(){
        val cal = Calendar.getInstance()
        cal.set(Calendar.YEAR, cal.get(Calendar.YEAR)+1)

        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)

        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val pagamento = TipoPagamentoFactory.criar(2, "Cartão")
        val compra = CompraFactory.criar(21, Date(), 40.0, 5.0, 1, pagamento, carrinho)
        val list = arrayListOf(compra)
        val page = PageImpl<Compra>(list)

        mockitoWhen(usuarioRepository?.findByLogin(ArgumentMatchers.anyString())).thenReturn(usuario)

        mockitoWhen(compraRepository?.findAllByUsuarioId(ArgumentMatchers.anyInt(), anyObject())).thenReturn(page)

        val result = compraService?.pesquisarCompras("ivo", null, cal.time, 0, 10)

        Assertions.assertThat(result?.compras?.size).isEqualTo(1)
    }

    @Test
    fun pesquisarComprasUsuarioNullDatasNull(){
        val cal = Calendar.getInstance()
        cal.set(Calendar.YEAR, cal.get(Calendar.YEAR)+1)

        mockitoWhen(usuarioRepository?.findByLogin(ArgumentMatchers.anyString())).thenReturn(null)

        try {
            compraService?.pesquisarCompras("ivo", null, null, 0, 10)
        }catch (e:BeblueBadRequestException) {
            Assertions.assertThat(e.message).isEqualTo("O usuário não existe")
        }
    }

    @Test
    fun pesquisarComprasUsuarioNull(){
        val cal = Calendar.getInstance()
        cal.set(Calendar.YEAR, cal.get(Calendar.YEAR)+1)

        mockitoWhen(usuarioRepository?.findByLogin(ArgumentMatchers.anyString())).thenReturn(null)

        try {
            compraService?.pesquisarCompras("ivo", Date(), cal.time, 0, 10)
        }catch (e:BeblueBadRequestException) {
            Assertions.assertThat(e.message).isEqualTo("O usuário não existe")
        }
    }

}