package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import org.assertj.core.api.Assertions
import org.junit.Test

class AtivoTest {

    @Test
    fun getAtivoTest(){
        val ativo = Ativo(1)
        Assertions.assertThat(ativo.ativo).isEqualTo(1)
    }

    @Test
    fun inativarTest(){
        val ativo = Ativo(1)
        ativo.inativar()
        Assertions.assertThat(ativo.ativo).isEqualTo(0)
    }

    @Test
    fun validarMenorZeroTest(){
        val ativo = Ativo(-1)
        try{
            ativo.isValid()
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("O campo ativo é inválido")
        }
    }

    @Test
    fun validarMaiorUmTest(){
        val ativo = Ativo(2)
        try{
            ativo.isValid()
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("O campo ativo é inválido")
        }
    }
}