package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.domain.factory.TipoPagamentoFactory
import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import org.assertj.core.api.Assertions
import org.junit.Test

class TipoPagamentoTest {

    @Test
    fun getDescricaoTest(){
        val descricao = "Cartão"
        val tipoPagamento = TipoPagamentoFactory.criar(1, descricao)
        Assertions.assertThat(tipoPagamento.descricao).isEqualTo(descricao)
    }

    @Test
    fun validarDescricaoVazio(){
        val descricao = " "

        try {
            TipoPagamentoFactory.criar(1, descricao)
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("A descrição está vazia")
        }
    }

    @Test
    fun validarDescricaoNull(){
        val descricao = null

        try {
            TipoPagamentoFactory.criar(1, descricao)
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("A descrição está vazia")
        }
    }

    @Test
    fun toStringDescricaoTest(){
        val descricao = "Cartão"
        val tipoPagamento = TipoPagamentoFactory.criar(1, descricao)
        Assertions.assertThat(tipoPagamento.toString()).isEqualTo(descricao)
    }

}