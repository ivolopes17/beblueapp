package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import org.assertj.core.api.Assertions
import org.junit.Test

class UsuarioTest {

    @Test
    fun getNomeTest(){
        val usuario = Usuario(1, Nome("teste"), "03984397447", "ivo", "123456", 10.0, Ativo(1))
        Assertions.assertThat(usuario.nome?.valor).isEqualTo("teste")
    }

    @Test
    fun getSenhaTest(){
        val usuario = Usuario(1, Nome("teste"), "03984397447", "ivo", "123456", 10.0, Ativo(1))
        Assertions.assertThat(usuario.senha).isEqualTo("123456")
    }

    @Test
    fun getCpfTest(){
        val usuario = Usuario(1, Nome("teste"), "03984397447", "ivo", "123456", 10.0, Ativo(1))
        Assertions.assertThat(usuario.cpf).isEqualTo("03984397447")
    }

    @Test
    fun getSaldoTest(){
        val usuario = Usuario(1, Nome("teste"), "03984397447", "ivo", "123456", 10.0, Ativo(1))
        Assertions.assertThat(usuario.saldo).isEqualTo(10.0)
    }

    @Test
    fun getAtivoTest(){
        val usuario = Usuario(1, Nome("teste"), "03984397447", "ivo", "123456", 10.0, Ativo(1))
        Assertions.assertThat(usuario.ativo?.ativo).isEqualTo(1)
    }

    @Test
    fun validarNomeNullTest(){
        val usuario = Usuario(1, null, "03984397447", "ivo", "123456", 10.0, Ativo(1))

        try{
            usuario.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O nome está nulo")
        }
    }

    @Test
    fun validarCpfNullTest(){
        val usuario = Usuario(1, Nome("teste"), null, "ivo", "123456", 10.0, Ativo(1))

        try{
            usuario.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O cpf do usuário está vazio")
        }
    }

    @Test
    fun validarCpfVazioTest(){
        val usuario = Usuario(1, Nome("teste"), " ", "ivo", "123456", 10.0, Ativo(1))

        try{
            usuario.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O cpf do usuário está vazio")
        }
    }

    @Test
    fun validarLoginNullTest(){
        val usuario = Usuario(1, Nome("teste"), "99083209820", null, "123456", 10.0, Ativo(1))

        try{
            usuario.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O login do usuário está vazio")
        }
    }

    @Test
    fun validarLoginVazioTest(){
        val usuario = Usuario(1, Nome("teste"), "982039832098", "  ", "123456", 10.0, Ativo(1))

        try{
            usuario.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O login do usuário está vazio")
        }
    }

    @Test
    fun validarSenhaNullTest(){
        val usuario = Usuario(1, Nome("teste"), "39823290293", "ivo", null, 10.0, Ativo(1))

        try{
            usuario.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("A senha do usuário está vazia")
        }
    }

    @Test
    fun validarSenhaVazioTest(){
        val usuario = Usuario(1, Nome("teste"), "928309238023", "ivo", "  ", 10.0, Ativo(1))

        try{
            usuario.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("A senha do usuário está vazia")
        }
    }

    @Test
    fun validarSaldoNullTest(){
        val usuario = Usuario(1, Nome("teste"), "39823290293", "ivo", "322332", null, Ativo(1))

        try{
            usuario.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O saldo do usuário é invalido")
        }
    }

    @Test
    fun validarSaldoVazioTest(){
        val usuario = Usuario(1, Nome("teste"), "928309238023", "ivo", "2332323", -2.0, Ativo(1))

        try{
            usuario.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O saldo do usuário é invalido")
        }
    }

    @Test
    fun aumentarSaldoNullTest(){
        val usuario = Usuario(1, Nome("teste"), "928309238023", "ivo", "2332323", null, Ativo(1))
        usuario.aumentarSaldo(3.0)
        Assertions.assertThat(usuario.saldo).isEqualTo(3.0)
    }

    @Test
    fun aumentarSaldoValorMenorZeroTest(){
        val usuario = Usuario(1, Nome("teste"), "928309238023", "ivo", "2332323", 10.0, Ativo(1))

        try{
        usuario.aumentarSaldo(-3.0)
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("O valor é invalido")
        }
    }

}