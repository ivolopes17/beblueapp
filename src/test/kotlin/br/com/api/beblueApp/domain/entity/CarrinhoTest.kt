package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.domain.factory.AlbumFactory
import br.com.api.beblueApp.domain.factory.ArtistaFactory
import br.com.api.beblueApp.domain.factory.CarrinhoFactory
import br.com.api.beblueApp.domain.factory.UsuarioFactory
import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import org.assertj.core.api.Assertions
import org.junit.Test
import java.util.*

class CarrinhoTest {

    @Test
    fun getDataCriacaoTest(){
        val carrinho = getCarrinho()
        Assertions.assertThat(carrinho.dataCriacao).isEqualTo(carrinho.dataCriacao)
    }

    @Test
    fun getDataValidadeTest(){
        val carrinho = getCarrinho()
        Assertions.assertThat(carrinho.dataValidade).isEqualTo(carrinho.dataValidade)
    }

    @Test
    fun getAtivoTest(){
        val carrinho = getCarrinho()
        Assertions.assertThat(carrinho.ativo?.ativo).isEqualTo(1)
    }

    @Test
    fun calcularValorTotalNullTest(){
        val carrinho = getCarrinho()
        val valor = carrinho.calcularValorTotal()
        Assertions.assertThat(valor).isEqualTo(0.0)
    }

    @Test
    fun aumentarSaldoUsuarioNullTest(){
        val carrinho = Carrinho(1, Date(), null, Ativo(1), Date())
        carrinho.aumentarSaldoUsuario(10.0)
    }

    @Test
    fun validarAtivoNullTeste(){
        val carrinho = Carrinho(1, Date(), getUsuario(), null, Date())

        try {
            carrinho.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O ativo é nulo")
        }
    }

    @Test
    fun validarUsuarioNullTeste(){
        val carrinho = Carrinho(1, Date(), null, Ativo(1), Date())

        try {
            carrinho.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O usuário é nulo")
        }
    }

    @Test
    fun validarDataCriacaoNullTeste(){
        val carrinho = Carrinho(1, null, getUsuario(), Ativo(1), Date())

        try {
            carrinho.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("A data de criação não é a data de hoje")
        }
    }

    @Test
    fun validarDataCriacaoDiferenteTeste(){
        val cal1 = Calendar.getInstance()
        cal1.set(Calendar.DAY_OF_MONTH, 10)
        cal1.set(Calendar.YEAR, 2010)
        val carrinho = Carrinho(1, cal1.time, getUsuario(), Ativo(1), Date())

        try {
            carrinho.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("A data de criação não é a data de hoje")
        }
    }

    @Test
    fun validarDataValidadeNullTeste(){
        val carrinho = Carrinho(1, Date(), getUsuario(), Ativo(1), null)

        try {
            carrinho.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("A data de validade é inválida")
        }
    }

    @Test
    fun validarDataValidadeMenorTeste(){
        val cal1 = Calendar.getInstance()
        cal1.set(Calendar.DAY_OF_MONTH, 10)
        cal1.set(Calendar.YEAR, 2010)
        val carrinho = Carrinho(1, Date(), getUsuario(), Ativo(1), cal1.time)

        try {
            carrinho.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("A data de validade é inválida")
        }
    }

    @Test
    fun validarDataValidadeIgualTeste(){

        val carrinho = Carrinho(1, Date(), getUsuario(), Ativo(1), Date())

        try {
            carrinho.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("A data de validade é inválida")
        }
    }

    @Test
    fun validarDataValidadeMaiorTeste(){
        val cal1 = Calendar.getInstance()
        cal1.set(Calendar.YEAR, cal1.get(Calendar.YEAR)+1)
        val carrinho = Carrinho(1, Date(), getUsuario(), Ativo(1), cal1.time)
        carrinho.isValid()
    }

    private fun getCarrinho():Carrinho{
        return CarrinhoFactory.criar(1, Date(), getUsuario())
    }

    private fun getUsuario():Usuario{
        return UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
    }

    private fun getAlbum():Album{
        return AlbumFactory.criar(1, "Hoje", "832hds9y2h2397sd", getArtista())
    }

    private fun getArtista(): Artista{
        return ArtistaFactory.criar(1, "Caetano", "98329088jjjj2")
    }

}