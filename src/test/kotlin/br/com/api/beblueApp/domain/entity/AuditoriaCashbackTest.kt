package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.domain.enums.TipoCashbackAudit
import br.com.api.beblueApp.domain.factory.AuditoriaCashbackFactory
import br.com.api.beblueApp.domain.factory.CarrinhoFactory
import br.com.api.beblueApp.domain.factory.TipoPagamentoFactory
import br.com.api.beblueApp.domain.factory.UsuarioFactory
import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import org.assertj.core.api.Assertions
import org.junit.Test
import java.util.*

class AuditoriaCashbackTest {

    @Test
    fun getDataTest(){
        val auditoriaCashback = AuditoriaCashback(1, Date(), 5.0, TipoCashbackAudit.ENTRADA, getCompra())
        auditoriaCashback.data
    }

    @Test
    fun getValorTest(){
        val auditoriaCashback = AuditoriaCashback(1, Date(), 5.0, TipoCashbackAudit.ENTRADA, getCompra())
        Assertions.assertThat(auditoriaCashback.valor).isEqualTo(5.0)
    }

    @Test
    fun getTipoTest(){
        val auditoriaCashback = AuditoriaCashback(1, Date(), 5.0, TipoCashbackAudit.ENTRADA, getCompra())
        Assertions.assertThat(auditoriaCashback.tipo).isEqualTo(TipoCashbackAudit.ENTRADA)
    }

    @Test
    fun getCompraTest(){
        val auditoriaCashback = AuditoriaCashback(1, Date(), 5.0, TipoCashbackAudit.ENTRADA, getCompra())
        Assertions.assertThat(auditoriaCashback.compra?.id).isEqualTo(1)
    }

    @Test
    fun validarCompraNullTest(){
        try {
            AuditoriaCashbackFactory.criar(1, Date(), 5.0, TipoCashbackAudit.ENTRADA, null)
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("A compra está nula")
        }
    }

    @Test
    fun validarCompraSemIdNullTest(){
        try {
            AuditoriaCashbackFactory.criar(Date(), 5.0, TipoCashbackAudit.ENTRADA, null)
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("A compra está nula")
        }
    }

    @Test
    fun validarDataNullTest(){
        try {
            AuditoriaCashbackFactory.criar(1, null, 5.0, TipoCashbackAudit.ENTRADA, null)
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("A data não é a de hoje")
        }
    }

    @Test
    fun validarDataDiferenteTest(){
        val cal1 = Calendar.getInstance()
        cal1.set(Calendar.DAY_OF_MONTH, 10)
        cal1.set(Calendar.YEAR, 2010)
        try {
            AuditoriaCashbackFactory.criar(1, cal1.time, 5.0, TipoCashbackAudit.ENTRADA, getCompra())
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("A data não é a de hoje")
        }
    }

    @Test
    fun validarTipoNullTest(){
        try {
            AuditoriaCashbackFactory.criar(1, Date(), 5.0, null, getCompra())
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("O tipo não foi informado")
        }
    }

    @Test
    fun validarValorNullTest(){
        try {
            AuditoriaCashbackFactory.criar(1, Date(), null, TipoCashbackAudit.ENTRADA, getCompra())
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("O valor é inválido")
        }
    }

    @Test
    fun validarValoMenorZeroTest(){
        try {
            AuditoriaCashbackFactory.criar(1, Date(), -2.0, TipoCashbackAudit.ENTRADA, getCompra())
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("O valor é inválido")
        }
    }

    private fun getCompra(): Compra{
        val pagamento = TipoPagamentoFactory.criar(1, "A vista")
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val compra = Compra(1, Date(), 10.0, 5.0, 11, Ativo(1), pagamento, carrinho)
        return compra
    }

}