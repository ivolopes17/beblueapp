package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.domain.factory.PercentualCashbackFactory
import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import org.assertj.core.api.Assertions
import org.junit.Test

class PercentualCashbackTest {

    @Test
    fun validarGeneroNull(){
        val dia = Dia(1, Nome("Domingo"))

        try {
            PercentualCashbackFactory.criar(1, 10, dia, null)
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O genero é nulo")
        }
    }

    @Test
    fun validarDiaNull(){
        val genero = Genero(1, Nome("mpb"), Ativo(1))

        try {
            PercentualCashbackFactory.criar(1, 10, null, genero)
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O dia é nulo")
        }
    }

    @Test
    fun validarPercentualNull(){
        val genero = Genero(1, Nome("mpb"), Ativo(1))
        val dia = Dia(1, Nome("Domingo"))
        try {
            PercentualCashbackFactory.criar(1, null, dia, genero)
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O percentual é inválido")
        }
    }

    @Test
    fun validarPercentualMenorZero(){
        val genero = Genero(1, Nome("mpb"), Ativo(1))
        val dia = Dia(1, Nome("Domingo"))
        try {
            PercentualCashbackFactory.criar(1, -1, dia, genero)
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O percentual é inválido")
        }
    }

    @Test
    fun validarPercentualMaiorCem(){
        val genero = Genero(1, Nome("mpb"), Ativo(1))
        val dia = Dia(1, Nome("Domingo"))
        try {
            PercentualCashbackFactory.criar(1, 101, dia, genero)
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O percentual é inválido")
        }
    }

    @Test
    fun getDiaTest(){
        val genero = Genero(1, Nome("mpb"), Ativo(1))
        val dia = Dia(1, Nome("Domingo"))
        val percentual = PercentualCashbackFactory.criar(1, 10, dia, genero)
        percentual.dia
    }

    @Test
    fun getGeneroTest(){
        val genero = Genero(1, Nome("mpb"), Ativo(1))
        val dia = Dia(1, Nome("Domingo"))
        val percentual = PercentualCashbackFactory.criar(1, 10, dia, genero)
        percentual.genero
    }
}