package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.domain.factory.CarrinhoFactory
import br.com.api.beblueApp.domain.factory.TipoPagamentoFactory
import br.com.api.beblueApp.domain.factory.UsuarioFactory
import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import org.assertj.core.api.Assertions
import org.junit.Test
import java.util.*

class CompraTest {

    @Test
    fun testIsAtivoNotNull(){
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val pagamento = TipoPagamentoFactory.criar(1, "A vista")

        try {
            val compra = Compra(1, Date(), 40.0, 5.0, 3, null, pagamento, carrinho)
            compra.isValid()
        }catch (e: BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("O campo ativo é inválido")
        }
    }

    @Test
    fun testIsAtivoNotNullWithoutId(){
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val pagamento = TipoPagamentoFactory.criar(1, "A vista")

        try {
            val compra = Compra(null, Date(), 40.0, 5.0, 3, null, pagamento, carrinho)
            compra.validarSemId()
        }catch (e: BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("O campo ativo é inválido")
        }
    }

    @Test
    fun testValorIgualZero(){
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val pagamento = TipoPagamentoFactory.criar(1, "A vista")
        val compra = Compra(null, Date(), 40.0, 5.0, 3, null, pagamento, carrinho)
        try {
            compra.adicionarCashback(0.0, 5)
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(compra.valorTotal).isEqualTo(40.0)
        }
    }

    @Test
    fun testPercentualIgualZero(){
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val pagamento = TipoPagamentoFactory.criar(1, "A vista")
        val compra = Compra(null, Date(), 40.0, 5.0, 3, null, pagamento, carrinho)
        try {
            compra.adicionarCashback(30.0, 0)
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(compra.valorTotal).isEqualTo(40.0)
        }
    }

    @Test
    fun testAdicionarValorCashbackNull(){
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val pagamento = TipoPagamentoFactory.criar(1, "A vista")
        val compra = Compra(null, Date(), 40.0, null, 3, null, pagamento, carrinho)
        compra.adicionarCashback(30.0, 10)
        Assertions.assertThat(compra.cashback).isEqualTo(3.0)
    }

    @Test
    fun testCalcularValorCarrinhoNull(){
        val pagamento = TipoPagamentoFactory.criar(1, "A vista")
        val compra = Compra(null, Date(), 40.0, 5.0, 3, null, pagamento, null)
        compra.calcularValorTotal()
        Assertions.assertThat(compra.valorTotal).isEqualTo(null)
    }

    @Test
    fun testInativarCarrinhoNull(){
        val pagamento = TipoPagamentoFactory.criar(1, "A vista")
        val compra = Compra(null, Date(), 40.0, 5.0, 3, null, pagamento, null)
        compra.inativarCarrinho()
        Assertions.assertThat(compra.ativo?.ativo).isEqualTo(null)
    }

    @Test
    fun testAumentarSaldoUsuarioCarrinhoNull(){
        val pagamento = TipoPagamentoFactory.criar(1, "A vista")
        val compra = Compra(1, Date(), 40.0, 5.0, 3, null, pagamento, null)
        compra.aumentarSaldoUsuario(10.0)
        Assertions.assertThat(compra.id).isEqualTo(1)
    }

    @Test
    fun testGetCarrinhoNull(){
        val pagamento = TipoPagamentoFactory.criar(1, "A vista")
        val compra = Compra(1, Date(), 40.0, 5.0, 3, null, pagamento, null)
        val albuns = compra.getCarrinhoAlbuns()
        Assertions.assertThat(albuns).isEqualTo(null)
    }

    @Test
    fun testDataNull(){
        val pagamento = TipoPagamentoFactory.criar(1, "A vista")
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val compra = Compra(1, null, 40.0, 5.0, 3, Ativo(1), pagamento, carrinho)

        try {
            compra.isValid()
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(compra.data).isEqualTo(null)
        }
    }

    @Test
    fun testDataNotEqual(){

        val cal1 = Calendar.getInstance()
        cal1.set(Calendar.DAY_OF_MONTH, 10)
        cal1.set(Calendar.YEAR, 2010)

        val pagamento = TipoPagamentoFactory.criar(1, "A vista")
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val compra = Compra(1, cal1.time, 40.0, 5.0, 3, Ativo(1), pagamento, carrinho)

        try {
            compra.isValid()
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(compra.data).isNotEqualTo(null)
        }
    }

    @Test
    fun testValorNull(){
        val pagamento = TipoPagamentoFactory.criar(1, "A vista")
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val compra = Compra(1, Date(), null, 5.0, 3, Ativo(1), pagamento, carrinho)

        try {
            compra.isValid()
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(compra.valorTotal).isEqualTo(null)
        }
    }

    @Test
    fun testValorMenorZero(){
        val pagamento = TipoPagamentoFactory.criar(1, "A vista")
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val compra = Compra(1, Date(), -1.0, 5.0, 3, Ativo(1), pagamento, carrinho)

        try {
            compra.isValid()
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(compra.valorTotal).isEqualTo(-1.0)
        }
    }

    @Test
    fun testCashbackMenorZero(){
        val pagamento = TipoPagamentoFactory.criar(1, "A vista")
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val compra = Compra(1, Date(), 10.0, -1.0, 3, Ativo(1), pagamento, carrinho)

        try {
            compra.isValid()
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(compra.cashback).isEqualTo(-1.0)
        }
    }

    @Test
    fun testCashbackNull(){
        val pagamento = TipoPagamentoFactory.criar(1, "A vista")
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val compra = Compra(1, Date(), 10.0, null, 3, Ativo(1), pagamento, carrinho)

        try {
            compra.isValid()
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(compra.cashback).isEqualTo(null)
        }
    }

    @Test
    fun testTipoPagamentoNull(){
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val compra = Compra(1, Date(), 10.0, 5.0, 3, Ativo(1), null, carrinho)

        try {
            compra.isValid()
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(true).isEqualTo(true)
        }
    }

    @Test
    fun testQtdPagamentoNull(){
        val pagamento = TipoPagamentoFactory.criar(1, "A vista")
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val compra = Compra(1, Date(), 10.0, 5.0, null, Ativo(1), pagamento, carrinho)

        try {
            compra.isValid()
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(compra.qtdVezesPagamento).isEqualTo(null)
        }
    }

    @Test
    fun testQtdPagamentoMenorUm(){
        val pagamento = TipoPagamentoFactory.criar(1, "A vista")
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val compra = Compra(1, Date(), 10.0, 5.0, 0, Ativo(1), pagamento, carrinho)

        try {
            compra.isValid()
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(compra.qtdVezesPagamento).isEqualTo(0)
        }
    }

    @Test
    fun testQtdPagamentoMaiorDez(){
        val pagamento = TipoPagamentoFactory.criar(1, "A vista")
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(12, Date(), usuario)
        val compra = Compra(1, Date(), 10.0, 5.0, 11, Ativo(1), pagamento, carrinho)

        try {
            compra.isValid()
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(compra.qtdVezesPagamento).isEqualTo(11)
        }
    }

}