package br.com.api.beblueApp.domain.factory

import org.assertj.core.api.Assertions
import org.junit.Test

class GeneroFactoryTest:GeneroFactory() {

    @Test
    fun testCreateWithoutIdSucess(){
        val generoText = "mpb"
        val genero = criar(generoText)
        Assertions.assertThat(genero.nome?.valor).isEqualTo(generoText)
    }
}