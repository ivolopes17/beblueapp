package br.com.api.beblueApp.domain.factory

import br.com.api.beblueApp.domain.model.ArtistaItemModelResponse
import org.assertj.core.api.Assertions
import org.junit.Test

class ArtistaFactoryTest:ArtistaFactory() {

    @Test
    fun testCreateWithoutIdByResponseSucess(){
        criar(createresponse())
        Assertions.assertThat(true).isEqualTo(true)
    }

    private fun createresponse():ArtistaItemModelResponse{
        val response = ArtistaItemModelResponse()
        response.id = "843j40334jjj3"
        response.name = "Cd"
        val generos = ArrayList<String>()
        generos.add("mpb")
        response.genres = generos
        return response
    }

}