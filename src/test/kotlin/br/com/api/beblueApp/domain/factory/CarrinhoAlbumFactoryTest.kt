package br.com.api.beblueApp.domain.factory

import org.assertj.core.api.Assertions
import org.junit.Test
import java.util.*

class CarrinhoAlbumFactoryTest:CarrinhoAlbumFactory() {

    @Test
    fun testCreateWithIdSuccess(){
        val artista = ArtistaFactory.criar(21, "João", "idus0sd9usdu")
        val album = AlbumFactory.criar(12, "Cd", "80d88sd0sd8j8", artista)
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        val carrinho = CarrinhoFactory.criar(1, Date(), usuario)
        val carrinhoAlbum = criar(1, 30.0, 2, album, carrinho)
        Assertions.assertThat(carrinhoAlbum.id).isEqualTo(1)
    }

}