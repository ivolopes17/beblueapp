package br.com.api.beblueApp.domain.factory

import br.com.api.beblueApp.domain.model.AlbumItemModelResponse
import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import org.assertj.core.api.Assertions
import org.junit.Test

class AlbumFactoryTest:AlbumFactory() {

    @Test
    fun testCreateWithIdNull(){
        val artista = ArtistaFactory.criar(1,"João", "98j30ddud03u")

        try {
            criar(0, "Cd", "9083en3n3", artista)
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O id é inválido")
        }
    }

    @Test
    fun testCreateWithoutIdSucess(){
        val artista = ArtistaFactory.criar(1,"João", "98j30ddud03u")
        criar("Cd", "9083en3n3", artista)
        Assertions.assertThat(true).isEqualTo(true)
    }

    @Test
    fun testCreateWithoutIdByResponseSucess(){

        val artista = ArtistaFactory.criar(1,"João", "98j30ddud03u")
        criar(createresponse(), artista)
        Assertions.assertThat(true).isEqualTo(true)
    }

    private fun createresponse():AlbumItemModelResponse{
        val response = AlbumItemModelResponse()
        response.id = "843j40334jjj3"
        response.name = "Cd"
        return response
    }

}