package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import org.assertj.core.api.Assertions
import org.junit.Test

class ArtistaGeneroTest {

    @Test
    fun testArtistaNullValidation(){
        val genero = Genero(1, Nome("mpb"), Ativo((1)))
        val atistaGenero = ArtistaGenero(1, genero, null)

        try{
            atistaGenero.isValid()
        }catch (e: BeblueValidationException){
            Assertions.assertThat(atistaGenero.artista).isEqualTo(null)
        }
    }

    @Test
    fun testGeneroNullValidation(){
        val artista = Artista(1, Nome("João"), "isdj0sdsdj0sdj", Ativo((1)))
        val atistaGenero = ArtistaGenero(1, null, artista)

        try{
            atistaGenero.isValid()
        }catch (e: BeblueValidationException){
            Assertions.assertThat(atistaGenero.genero).isEqualTo(null)
        }
    }

}