package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.domain.factory.AlbumFactory
import br.com.api.beblueApp.domain.factory.ArtistaFactory
import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import org.assertj.core.api.Assertions
import org.junit.Test

class AlbumTest {

    @Test
    fun getNomeTest(){
        val album = getAlbum()
        Assertions.assertThat(album.nome?.valor).isEqualTo("Hoje")
    }

    @Test
    fun getSpotifyIdTest(){
        val album = getAlbum()
        Assertions.assertThat(album.spotifyId).isEqualTo("832hds9y2h2397sd")
    }

    @Test
    fun getAtivoTest(){
        val album = getAlbum()
        Assertions.assertThat(album.ativo.ativo).isEqualTo(1)
    }

    @Test
    fun validarSpotifyIdNullTest(){

        try {
            AlbumFactory.criar(1, "Hoje", null, getArtista())
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O campo spotifyId do artista é inválido")
        }
    }

    @Test
    fun validarNomeNullTest(){

        try {
            val album = Album(1, null, "832hds9y2h2397sd", getArtista(), Ativo(1))
            album.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O nome está nulo")
        }
    }

    @Test
    fun validarArtistaNullTest(){

        try {
            val album = Album(1, Nome("Teste"), "832hds9y2h2397sd", null, Ativo(1))
            album.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O artista está nulo")
        }
    }

    @Test
    fun validarNomeNullWihtoutIdTest(){

        try {
            val album = Album(null, null, "832hds9y2h2397sd", getArtista(), Ativo(1))
            album.validarSemId()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O nome está nulo")
        }
    }

    @Test
    fun validarArtistaNullWihtoutIdTest(){

        try {
            val album = Album(null, Nome("Teste"), "832hds9y2h2397sd", null, Ativo(1))
            album.validarSemId()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O artista está nulo")
        }
    }

    @Test
    fun validarSpotifyIdEmptyTest(){

        try {
            AlbumFactory.criar(1, "Hoje", " ", getArtista())
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O campo spotifyId do artista é inválido")
        }
    }

    private fun getAlbum():Album{
        return AlbumFactory.criar(1, "Hoje", "832hds9y2h2397sd", getArtista())
    }

    private fun getArtista(): Artista{
        return ArtistaFactory.criar(1, "Caetano", "98329088jjjj2")
    }
}