package br.com.api.beblueApp.domain.factory

import br.com.api.beblueApp.domain.enums.TipoCashbackAudit
import org.assertj.core.api.Assertions
import org.junit.Test
import java.util.*

class AuditoriaCashbackFactoryTest:AuditoriaCashbackFactory() {

    @Test
    fun testCreateWithoutIdSucess(){
        val tipoPagamento = TipoPagamentoFactory.criar(1, "Cartão de crédito")
        val usuario = UsuarioFactory.criarParaTeste(1, "Ivo", "ivo", 40.0)
        val carrinho = CarrinhoFactory.criar(1, Date(), usuario)
        val compra = CompraFactory.criar(1, Date(), 50.0, 5.0, 1, tipoPagamento, carrinho)
        criar(Date(), 20.0, TipoCashbackAudit.ENTRADA, compra)

        Assertions.assertThat(true).isEqualTo(true)
    }

}