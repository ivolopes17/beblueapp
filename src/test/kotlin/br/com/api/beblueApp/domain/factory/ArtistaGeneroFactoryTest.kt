package br.com.api.beblueApp.domain.factory

import org.assertj.core.api.Assertions
import org.junit.Test

class ArtistaGeneroFactoryTest:ArtistaGeneroFactory() {

    @Test
    fun testCreateWithoutIdSucess(){
        val genero = GeneroFactory.criar(1, "mpb")

        val artista = ArtistaFactory.criar(1, "Caetano", "98329088jjjj2")

        val artistaGenero = criar(genero, artista)

        Assertions.assertThat(artistaGenero.id).isEqualTo(null)
    }

}