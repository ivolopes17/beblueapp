package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import org.assertj.core.api.Assertions
import org.junit.Test

class GeneroTest {

    @Test
    fun testAtivoNullValidation(){
        val genero = Genero(1, Nome("mpb"), null)

        try{
            genero.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(genero.ativo).isEqualTo(null)
        }
    }

    @Test
    fun testNomeNullValidation(){
        val genero = Genero(1, null, Ativo(1))

        try{
            genero.isValid()
        }catch (e:BeblueValidationException){
            Assertions.assertThat(genero.nome).isEqualTo(null)
        }
    }

}