package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.domain.factory.ArtistaFactory
import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import org.assertj.core.api.Assertions
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
class ArtistaTest {

    @Test
    fun testGetNomeSucess(){
        val nomeText = "Ivo"
        val artista = ArtistaFactory.criar(nomeText, "osiduosdusod")
        Assertions.assertThat(artista.nome?.valor).isEqualTo(nomeText)
    }

    @Test
    fun testGetAtivoSucess(){
        val nomeText = "Ivo"
        val spotifyId = "osiduosdusod"
        val artista = ArtistaFactory.criar(1, nomeText, spotifyId)
        Assertions.assertThat(artista.ativo.ativo).isEqualTo(1)
    }

    @Test
    fun testGetSpotifyIdSucess(){
        val nomeText = "Ivo"
        val spotifyId = "osiduosdusod"
        val artista = ArtistaFactory.criar(nomeText, spotifyId)
        Assertions.assertThat(artista.spotifyId).isEqualTo(spotifyId)
    }

    @Test
    fun validartSpotifyIdVazio(){
        val nomeText = "Ivo"
        val spotifyId = " "

        try {
            ArtistaFactory.criar(nomeText, spotifyId)
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("O campo spotifyId do artista é inválido")
        }
    }

    @Test
    fun validartSpotifyIdNulo(){
        val nomeText = "Ivo"
        val spotifyId = null

        try {
            ArtistaFactory.criar(nomeText, spotifyId)
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("O campo spotifyId do artista é inválido")
        }
    }

    @Test
    fun validartNomeNulo(){

        val spotifyId = "304303j04jsjjss"

        try {
            val artista = Artista(1, null, spotifyId, Ativo(1))
            artista.isValid()
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("O nome está nulo")
        }
    }

}


