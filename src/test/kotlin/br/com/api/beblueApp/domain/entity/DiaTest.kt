package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.domain.factory.DiaFactory
import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import org.assertj.core.api.Assertions
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
class DiaTest {

    @Test
    fun testNomeIsValidSucess(){
        val diaText = "Domingo"
        val dia = DiaFactory.criar(1, diaText)
        dia.isValid()
        Assertions.assertThat(dia.nome?.valor).isEqualTo(diaText)
    }

    @Test
    fun testIdIsValidFail(){
        val diaText = "Domingo"
        try {
            DiaFactory.criar(0, diaText)
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("O id é inválido")
        }
    }

    @Test
    fun testIdIsValidEmpty(){
        val diaText = "Domingo"
        try {
            DiaFactory.criar(null, diaText)
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("O id está vazio")
        }
    }

    @Test
    fun testIsValidNomeEmpty(){

        try {
            DiaFactory.criar(1, null)
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("O nome está vazio")
        }
    }

    @Test
    fun testIsValidNomeNull(){

        try {
           val dia = Dia(1, null)
            dia.isValid()
        }catch (e:BeblueValidationException) {
            Assertions.assertThat(e.message).isEqualTo("O valor está vazio")
        }
    }

}