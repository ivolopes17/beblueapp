package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.domain.factory.*
import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import org.assertj.core.api.Assertions
import org.junit.Test
import java.util.*

class CarrinhoAlbumTest {


    @Test
    fun getCarrinhoTest(){

        val carrinho = CarrinhoAlbumFactory.criar( 1, 20.0, 2, getAlbum(), getCarrinho())
        Assertions.assertThat(carrinho.carrinho?.id).isEqualTo(1)

    }

    @Test
    fun validarAlbumNuloTest(){
        try {
            CarrinhoAlbumFactory.criar(1, 20.0, 2, null, getCarrinho())
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O album é nulo")
        }
    }

    @Test
    fun validarAlbumNuloSemIdTest(){
        try {
            CarrinhoAlbumFactory.criar( 20.0, 2, null, getCarrinho())
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O album é nulo")
        }
    }

    @Test
    fun validarCarrinhoNuloTest(){
        try {
            CarrinhoAlbumFactory.criar(1, 20.0, 2, getAlbum(), null)
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O carrinho é nulo")
        }
    }

    @Test
    fun validarCarrinhoNuloSemIdTest(){
        try {
            CarrinhoAlbumFactory.criar( 20.0, 2, getAlbum(), getCarrinho())
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O carrinho é nulo")
        }
    }

    @Test
    fun validarQuantidadeNuloTest(){
        try {
            CarrinhoAlbumFactory.criar( 1, 20.0, null, getAlbum(), getCarrinho())
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("A quantidade é inválida")
        }
    }

    @Test
    fun validarQuantidadeMenorUmTest(){
        try {
            CarrinhoAlbumFactory.criar( 1, 20.0, 0, getAlbum(), getCarrinho())
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("A quantidade é inválida")
        }
    }

    @Test
    fun validarQuantidadeMaiorDezTest(){
        try {
            CarrinhoAlbumFactory.criar( 1, 20.0, 11, getAlbum(), getCarrinho())
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("A quantidade é inválida")
        }
    }

    @Test
    fun validarValorNullTest(){
        try {
            CarrinhoAlbumFactory.criar( 1, null, 9, getAlbum(), getCarrinho())
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O valor é inválido")
        }
    }

    @Test
    fun validarValorMenorZeroTest(){
        try {
            CarrinhoAlbumFactory.criar( 1, -2.0, 9, getAlbum(), getCarrinho())
        }catch (e:BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O valor é inválido")
        }
    }

    private fun getCarrinho():Carrinho{
        val usuario = UsuarioFactory.criarParaTeste(1, "ivo", "ivo", 0.0)
        return CarrinhoFactory.criar(1, Date(), usuario)
    }

    private fun getAlbum():Album{
        return AlbumFactory.criar(1, "Hoje", "832hds9y2h2397sd", getArtista())
    }

    private fun getArtista(): Artista{
        return ArtistaFactory.criar(1, "Caetano", "98329088jjjj2")
    }

}