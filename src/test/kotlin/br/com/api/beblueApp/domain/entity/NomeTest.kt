package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import org.assertj.core.api.Assertions
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
class NomeTest {

    @Test
    fun testNomeIsValidSucess(){
        val nomeText = "Ivo"
        val nome = Nome(nomeText)
        nome.isValid()
        Assertions.assertThat(nome.valor).isEqualTo(nomeText)
    }

    @Test
    fun testNomeIsValidFailNull(){
        val nomeText = null
        val nome = Nome(nomeText)

        try {
            nome.isValid()
        }catch (e: BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O nome está vazio")
        }

    }

    @Test
    fun testNomeIsValidFailEmpty(){
        val nomeText = "  "
        val nome = Nome(nomeText)

        try {
            nome.isValid()
        }catch (e: BeblueValidationException){
            Assertions.assertThat(e.message).isEqualTo("O nome está vazio")
        }

    }

}