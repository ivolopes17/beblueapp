package br.com.api.beblueApp.infrastructure.repository

import br.com.api.beblueApp.application.service.LoginService
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration
import org.springframework.cloud.openfeign.FeignAutoConfiguration
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@DataJpaTest
@ImportAutoConfiguration(classes =  [RibbonAutoConfiguration::class, FeignRibbonClientAutoConfiguration::class, FeignAutoConfiguration::class])
class CompraRepositoryTest {

    @Autowired
    private val compraRepository: CompraRepository? = null

    @MockBean
    private val loginService: LoginService? = null

    @Test
    @Throws(Exception::class)
    fun findByIdTestNotFound(){
        val result = compraRepository?.findById(0)

        assertThat(result?.isPresent).isEqualTo(false)
    }

}