package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.application.infrastructurePort.GeneroRepositoryPort
import br.com.api.beblueApp.domain.entity.Nome
import br.com.api.beblueApp.domain.factory.GeneroFactory
import br.com.api.beblueApp.infrastructure.repository.GeneroRepository
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration
import org.springframework.cloud.openfeign.FeignAutoConfiguration
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(properties = ["job.autorun.enabled=false"])
@ImportAutoConfiguration(classes =  [RibbonAutoConfiguration::class, FeignRibbonClientAutoConfiguration::class, FeignAutoConfiguration::class])
class GeneroRepositoryAdapterTest {

    @Autowired
    private lateinit var generoRepository: GeneroRepository

    private var adapter: GeneroRepositoryPort? = null

    @Before
    fun setup(){
        adapter = GeneroRepositoryAdapter(generoRepository)
    }

    @Test
    fun pesquisarTodosTest(){
        val lista = adapter?.pesquisarTodos()

        Assertions.assertThat(lista?.size!! > 0).isEqualTo(true)
    }

    @Test
    fun pesquisarPorNome(){
        val nome = "mpb"
        val genero = adapter?.pesquisarPorNome(Nome(nome))
        Assertions.assertThat(genero?.nome?.valor).isEqualTo(nome)
    }

    @Test
    fun pesquisarPorNomeNotFound(){
        val nome = "abc"
        val genero = adapter?.pesquisarPorNome(Nome(nome))
        Assertions.assertThat(genero).isEqualTo(null)
    }

    @Test
    fun salvar(){
        val genero = GeneroFactory.criar("sertanejo")
        adapter?.salvar(genero)

        Assertions.assertThat(genero.id != null).isEqualTo(true)
    }
}