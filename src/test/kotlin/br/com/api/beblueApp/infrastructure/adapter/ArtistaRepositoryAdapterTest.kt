package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.application.infrastructurePort.ArtistaRepositoryPort
import br.com.api.beblueApp.application.service.LoginService
import br.com.api.beblueApp.domain.entity.Artista
import br.com.api.beblueApp.domain.factory.ArtistaFactory
import br.com.api.beblueApp.infrastructure.repository.ArtistaRepository
import br.com.api.beblueApp.infrastructure.spotifyClient.api.ArtistaSpotifyClient
import br.com.api.beblueApp.userInterface.security.SecurityConstants
import org.assertj.core.api.Assertions
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration
import org.springframework.cloud.openfeign.FeignAutoConfiguration
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(properties = ["job.autorun.enabled=false"])
@ImportAutoConfiguration(classes =  [RibbonAutoConfiguration::class, FeignRibbonClientAutoConfiguration::class, FeignAutoConfiguration::class])
class ArtistaRepositoryAdapterTest {

    @Autowired
    private lateinit var loginService: LoginService

    @Autowired
    private lateinit var artistaRepository: ArtistaRepository

    @Autowired
    lateinit var artistaClient: ArtistaSpotifyClient

    private var adapter:ArtistaRepositoryPort? = null

    private var tokenSpotify: String? = null

    private val id = "123456abc"

    private var artista: Artista? = null

    @Before
    fun setup(){
        tokenSpotify = SecurityConstants.TOKEN_PREFIX+loginService.autenticar().access_token
        adapter = ArtistaRepositoryAdapter(artistaRepository, artistaClient, tokenSpotify!!)

        artista = ArtistaFactory.criar("Mateus e Kaua", "123456abc")
        adapter?.salvar(artista!!)
    }

    @After
    fun removeEntity(){
        artistaRepository.delete(artista!!)
    }

    @Test
    fun pesquisarArtistaSpotifyPorIdSucesso(){
        val id = "2Z0lRIqr997lIUiPtrpKCr"
        val response = adapter?.pesquisarArtistaSpotifyPorId(id)
        Assertions.assertThat(response?.id).isEqualTo(id)
    }

    @Test
    fun consultarPorSpotifyIdSucesso(){
        val art = adapter?.consultarPorSpotifyId(id)
        Assertions.assertThat(art?.spotifyId).isEqualTo(id)
    }

}