package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.application.infrastructurePort.PercentualCashbackRepositoryPort
import br.com.api.beblueApp.domain.entity.Dia
import br.com.api.beblueApp.domain.entity.Genero
import br.com.api.beblueApp.domain.entity.Nome
import br.com.api.beblueApp.infrastructure.repository.DiaRepository
import br.com.api.beblueApp.infrastructure.repository.GeneroRepository
import br.com.api.beblueApp.infrastructure.repository.PercentualCashbackRepository
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration
import org.springframework.cloud.openfeign.FeignAutoConfiguration
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(properties = ["job.autorun.enabled=false"])
@ImportAutoConfiguration(classes =  [RibbonAutoConfiguration::class, FeignRibbonClientAutoConfiguration::class, FeignAutoConfiguration::class])
class PercentualCashbackRepositoryAdapterTest {

    @Autowired
    private lateinit var repository: PercentualCashbackRepository

    @Autowired
    private lateinit var diaRepository: DiaRepository

    @Autowired
    private lateinit var generoRepository: GeneroRepository

    private var adapter:PercentualCashbackRepositoryPort? = null

    private var genero: Genero? = null

    private var dia: Dia? = null


    @Before
    fun setup() {
        adapter = PercentualCashbackRepositoryAdapter(repository)
        genero = generoRepository.findByNome(Nome("mpb"))
        dia = diaRepository.findById(1).get()
    }

    @Test
    fun pesquisarPorDiaGeneroSucesso(){
        val percentual = adapter?.pesquisarPorDiaGenero(dia!!, genero!!)
        Assertions.assertThat(percentual?.dia?.id).isEqualTo(dia?.id)
    }
}