package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.application.infrastructurePort.UsuarioRepositoryPort
import br.com.api.beblueApp.infrastructure.repository.UsuarioRepository
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration
import org.springframework.cloud.openfeign.FeignAutoConfiguration
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(properties = ["job.autorun.enabled=false"])
@ImportAutoConfiguration(classes =  [RibbonAutoConfiguration::class, FeignRibbonClientAutoConfiguration::class, FeignAutoConfiguration::class])
class UsuarioRepositoryAdapterTest {

    @Autowired
    private lateinit var repository: UsuarioRepository

    private var adapter:UsuarioRepositoryPort? = null

    @Before
    fun setup() {
        adapter = UsuarioRepositoryAdapter(repository)
    }

    @Test
    fun findByLoginTest(){
        val login = "ivo"
        val usuario = adapter?.findByLogin(login)
        Assertions.assertThat(usuario?.login).isEqualTo(login)
    }

}