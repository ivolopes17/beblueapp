package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.application.infrastructurePort.TipoPagamentoRepositoryPort
import br.com.api.beblueApp.infrastructure.repository.TipoPagamentoRepository
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration
import org.springframework.cloud.openfeign.FeignAutoConfiguration
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(properties = ["job.autorun.enabled=false"])
@ImportAutoConfiguration(classes =  [RibbonAutoConfiguration::class, FeignRibbonClientAutoConfiguration::class, FeignAutoConfiguration::class])
class TipoPagamentoRepositoryAdapterTest {

    @Autowired
    private lateinit var repository: TipoPagamentoRepository

    private var adapter: TipoPagamentoRepositoryPort? = null

    @Before
    fun setup(){
        adapter = TipoPagamentoRepositoryAdapter(repository)
    }

    @Test
    fun pesquisarTodosTest(){
        val lista = adapter?.pesquisarTodos()

        Assertions.assertThat(lista?.size!! > 0).isEqualTo(true)
    }

    @Test
    fun pesquisarPorId(){
        val pagamento = adapter?.pesquisarPorId(1)
        Assertions.assertThat(pagamento?.id).isEqualTo(1)
    }

    @Test
    fun pesquisarPorIdNotFound(){
        val pagamento = adapter?.pesquisarPorId(0)
        Assertions.assertThat(pagamento).isEqualTo(null)
    }

}