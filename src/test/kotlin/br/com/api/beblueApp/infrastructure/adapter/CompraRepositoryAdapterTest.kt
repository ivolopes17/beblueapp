package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.application.infrastructurePort.CompraRepositoryPort
import br.com.api.beblueApp.domain.entity.Ativo
import br.com.api.beblueApp.domain.entity.Carrinho
import br.com.api.beblueApp.domain.entity.Compra
import br.com.api.beblueApp.domain.factory.CompraFactory
import br.com.api.beblueApp.infrastructure.repository.CarrinhoRepository
import br.com.api.beblueApp.infrastructure.repository.CompraRepository
import br.com.api.beblueApp.infrastructure.repository.TipoPagamentoRepository
import br.com.api.beblueApp.infrastructure.repository.UsuarioRepository
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration
import org.springframework.cloud.openfeign.FeignAutoConfiguration
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration
import org.springframework.data.domain.PageRequest
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest(properties = ["job.autorun.enabled=false"])
@ImportAutoConfiguration(classes =  [RibbonAutoConfiguration::class, FeignRibbonClientAutoConfiguration::class, FeignAutoConfiguration::class])
class CompraRepositoryAdapterTest {

    @Autowired
    private lateinit var compraRepository: CompraRepository

    @Autowired
    private lateinit var usuarioRepository: UsuarioRepository

    @Autowired
    private lateinit var carrinhoRepository: CarrinhoRepository

    @Autowired
    private lateinit var tipoPagamentoRepository: TipoPagamentoRepository

    private var adapter: CompraRepositoryPort? = null

    @Before
    fun setup() {
        adapter = CompraRepositoryAdapter(compraRepository)

        val usuario = usuarioRepository.findByLoginAndAtivo("ivo")

        var carrinho = Carrinho(null, Date(), usuario, Ativo(1), null)
        carrinho.adicionarTempo()
        carrinho = carrinhoRepository.save(carrinho)
        val pagamento = tipoPagamentoRepository.findById(1)
        val compra:Compra? = CompraFactory.criar(Date(), 1, pagamento.get(), carrinho)
        adapter?.salvar(compra!!)
    }

    @Test
    fun findByIdNotFoundSucesso(){
        val id = 0
        val compra = adapter?.findById(id)
        Assertions.assertThat(compra?.id).isEqualTo(null)
    }

    @Test
    fun findByIdSucesso(){
        val id = 1
        val compra = adapter?.findById(id)
        Assertions.assertThat(compra?.id).isEqualTo(id)
    }

    @Test
    fun findAllByDatasAndUsuarioNotFound(){
        val paginacao = PageRequest.of(0, 10)
        val compras = adapter?.findAllByDatasAndUsuario(Date(), Date(), 1, paginacao)
        Assertions.assertThat(compras?.isEmpty).isEqualTo(true)
    }

    @Test
    fun findAllByDatasAndUsuario(){
        val paginacao = PageRequest.of(0, 10)
        val compras = adapter?.findAllByDatasAndUsuario(Date(), Date(), 1, paginacao)
        Assertions.assertThat(compras?.isEmpty).isEqualTo(true)
    }

    @Test
    fun findAllByUsuarioIdNotFoundSucesso(){
        val id = 0
        val paginacao = PageRequest.of(0, 10)
        val compra = adapter?.findAllByUsuarioId(id, paginacao)
        Assertions.assertThat(compra?.isEmpty).isEqualTo(true)
    }

    @Test
    fun findAllByUsuarioIdSucesso(){
        val usuario = usuarioRepository.findByLoginAndAtivo("ivo")
        val paginacao = PageRequest.of(0, 10)
        val compra = adapter?.findAllByUsuarioId(usuario?.id!!, paginacao)
        Assertions.assertThat(compra?.isEmpty).isEqualTo(false)
    }

    @Test
    fun salvarSucesso(){
        val usuario = usuarioRepository.findByLoginAndAtivo("ivo")

        var carrinho = Carrinho(null, Date(), usuario, Ativo(1), null)
        carrinho.adicionarTempo()
        carrinho = carrinhoRepository.save(carrinho)
        val pagamento = tipoPagamentoRepository.findById(1)
        var compra:Compra? = CompraFactory.criar(Date(), 1, pagamento.get(), carrinho)
        compra = adapter?.salvar(compra!!)
        Assertions.assertThat(compra?.qtdVezesPagamento).isEqualTo(1)
    }
}