package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.application.infrastructurePort.AuditoriaCashbackRepositoryPort
import br.com.api.beblueApp.domain.entity.Ativo
import br.com.api.beblueApp.domain.entity.Carrinho
import br.com.api.beblueApp.domain.entity.Compra
import br.com.api.beblueApp.domain.enums.TipoCashbackAudit
import br.com.api.beblueApp.domain.factory.AuditoriaCashbackFactory
import br.com.api.beblueApp.domain.factory.CompraFactory
import br.com.api.beblueApp.infrastructure.repository.*
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration
import org.springframework.cloud.openfeign.FeignAutoConfiguration
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest(properties = ["job.autorun.enabled=false"])
@ImportAutoConfiguration(classes =  [RibbonAutoConfiguration::class, FeignRibbonClientAutoConfiguration::class, FeignAutoConfiguration::class])
class AuditoriaCashbackRepositoryAdapterTest {

    @Autowired
    private lateinit var auditoriaCashbackRepository: AuditoriaCashbackRepository

    @Autowired
    private lateinit var usuarioRepository: UsuarioRepository

    @Autowired
    private lateinit var carrinhoRepository: CarrinhoRepository

    @Autowired
    private lateinit var tipoPagamentoRepository: TipoPagamentoRepository

    @Autowired
    private lateinit var compraRepository:CompraRepository

    private var adapter:AuditoriaCashbackRepositoryPort? = null

    private var compra:Compra? = null

    @Before
    fun setup(){

        adapter = AuditoriaCashbackRepositoryAdapter(auditoriaCashbackRepository)

        val usuario = usuarioRepository.findByLoginAndAtivo("ivo")

        val carrinho = Carrinho(null, Date(), usuario, Ativo(1), null)
        carrinho.adicionarTempo()
        carrinhoRepository.save(carrinho)

        val pagamento = this.tipoPagamentoRepository.findById(1)
        compra  = CompraFactory.criar(Date(), 1, pagamento.get(), carrinho)
        compra = this.compraRepository.save(compra!!)
    }

    @Test
    fun salvarTest(){
        val auditoriaCashback = AuditoriaCashbackFactory.criar(Date(), 20.0, TipoCashbackAudit.ENTRADA, compra)
        adapter?.salvar(auditoriaCashback)
        Assertions.assertThat(auditoriaCashback.id != null).isEqualTo(true)
    }

}