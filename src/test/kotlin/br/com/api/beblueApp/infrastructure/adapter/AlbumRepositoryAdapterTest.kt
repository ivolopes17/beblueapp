package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.application.infrastructurePort.AlbumRepositoryPort
import br.com.api.beblueApp.application.service.LoginService
import br.com.api.beblueApp.domain.entity.Album
import br.com.api.beblueApp.domain.entity.Artista
import br.com.api.beblueApp.domain.factory.AlbumFactory
import br.com.api.beblueApp.domain.factory.ArtistaFactory
import br.com.api.beblueApp.infrastructure.repository.AlbumRepository
import br.com.api.beblueApp.infrastructure.repository.ArtistaRepository
import br.com.api.beblueApp.infrastructure.spotifyClient.api.AlbumSpotifyClient
import br.com.api.beblueApp.infrastructure.spotifyClient.api.PesquisarSpotifyClient
import br.com.api.beblueApp.userInterface.security.SecurityConstants
import org.assertj.core.api.Assertions
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration
import org.springframework.cloud.openfeign.FeignAutoConfiguration
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(properties = ["job.autorun.enabled=false"])
@ImportAutoConfiguration(classes =  [RibbonAutoConfiguration::class, FeignRibbonClientAutoConfiguration::class, FeignAutoConfiguration::class])
class AlbumRepositoryAdapterTest {

    @Autowired
    private lateinit var albumRepository: AlbumRepository

    @Autowired
    private lateinit var artistaRepository: ArtistaRepository

    @Autowired
    private lateinit var pesquisarClient: PesquisarSpotifyClient

    @Autowired
    lateinit var albumClient: AlbumSpotifyClient

    @Autowired
    private lateinit var loginService: LoginService

    private var tokenSpotify: String? = null

    private var adapter:AlbumRepositoryPort? = null

    private val id = "123456abc"

    private var artista: Artista? = null

    private var album: Album? = null

    @Before
    fun setup(){
        tokenSpotify = SecurityConstants.TOKEN_PREFIX+loginService.autenticar().access_token
        adapter = AlbumRepositoryAdapter(albumRepository, pesquisarClient, albumClient, tokenSpotify!!)
        artista = ArtistaFactory.criar("Mateus e Kaua", id)
        artistaRepository.save(artista!!)
        album = AlbumFactory.criar("Teste", id, artista)
        adapter?.salvar(album!!)
    }

    @After
    fun removeEntity(){
        albumRepository.delete(album!!)
        artistaRepository.delete(artista!!)
    }

    @Test
    fun pesquisarAlbumSpotifySucesso(){
        val response = adapter?.pesquisarAlbumSpotify("Hoje", 10, 0)
        Assertions.assertThat(response?.albums?.total!! > 0).isEqualTo(true)
    }

    @Test
    fun pesquisarAlbumSpotifyPorIdSucesso(){
        val id = "537cWg3a0Fn9zmPllBPMGh"
        val response = adapter?.pesquisarAlbumSpotifyPorId(id)
        Assertions.assertThat(response?.id).isEqualTo(id)
    }

    @Test
    fun consultarPorSpotifyIdSucesso(){
        val albumResponse = adapter?.consultarPorSpotifyId(id)
        Assertions.assertThat(albumResponse?.spotifyId).isEqualTo(id)
    }

}