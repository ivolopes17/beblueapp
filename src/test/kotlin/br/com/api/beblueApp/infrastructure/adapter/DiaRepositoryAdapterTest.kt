package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.application.infrastructurePort.DiaRepositoryPort
import br.com.api.beblueApp.infrastructure.repository.DiaRepository
import org.assertj.core.api.Assertions
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration
import org.springframework.cloud.openfeign.FeignAutoConfiguration
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(properties = ["job.autorun.enabled=false"])
@ImportAutoConfiguration(classes =  [RibbonAutoConfiguration::class, FeignRibbonClientAutoConfiguration::class, FeignAutoConfiguration::class])
class DiaRepositoryAdapterTest {

    @Autowired
    private lateinit var diaRepository:DiaRepository

    private var adapter:DiaRepositoryPort? = null

    @Before
    fun setup(){
        adapter = DiaRepositoryAdapter(diaRepository)
    }

    @Test
    fun pesquisarPorIdSucesso(){
        val dia = adapter?.pesquisarPorId(1)
        Assertions.assertThat(dia?.id).isEqualTo(1)
    }

    @Test
    fun pesquisarPorIdNotFound(){
        val dia = adapter?.pesquisarPorId(9)
        Assertions.assertThat(dia).isEqualTo(null)
    }
}