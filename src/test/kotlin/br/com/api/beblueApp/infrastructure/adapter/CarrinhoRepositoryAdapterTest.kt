package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.application.infrastructurePort.CarrinhoRepositoryPort
import br.com.api.beblueApp.domain.entity.Ativo
import br.com.api.beblueApp.domain.entity.Carrinho
import br.com.api.beblueApp.domain.entity.Usuario
import br.com.api.beblueApp.infrastructure.repository.CarrinhoRepository
import br.com.api.beblueApp.infrastructure.repository.UsuarioRepository
import org.assertj.core.api.Assertions
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration
import org.springframework.cloud.openfeign.FeignAutoConfiguration
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest(properties = ["job.autorun.enabled=false"])
@ImportAutoConfiguration(classes =  [RibbonAutoConfiguration::class, FeignRibbonClientAutoConfiguration::class, FeignAutoConfiguration::class])
class CarrinhoRepositoryAdapterTest {

    @Autowired
    private lateinit var carrinhoRepository: CarrinhoRepository

    @Autowired
    private lateinit var usuarioRepository: UsuarioRepository

    private var adapter:CarrinhoRepositoryPort? = null

    private var carrinho:Carrinho? = null

    private var usuario:Usuario? = null

    @Before
    fun setup(){

        adapter = CarrinhoRepositoryAdapter(this.carrinhoRepository)

        usuario = usuarioRepository.findByLoginAndAtivo("ivo")

        carrinho = Carrinho(null, Date(), usuario, Ativo(1), null)
        carrinho?.adicionarTempo()
        adapter?.salvar(carrinho!!)
    }

    @After
    fun removeEntity(){
        this.carrinhoRepository.delete(carrinho!!)
    }

    @Test
    fun findByAtivoAndUsuarioIdNotFound(){
        val car = adapter?.findByAtivoAndUsuarioId(Ativo(2), usuario?.id!!)
        Assertions.assertThat(car?.size == 0).isEqualTo(true)
    }

    @Test
    fun findByAtivoAndUsuarioId(){
        val car = adapter?.findByAtivoAndUsuarioId(Ativo(1), usuario?.id!!)
        Assertions.assertThat(car!= null).isEqualTo(true)
    }

    @Test
    fun pesquisarPorId() {
        val car = adapter?.pesquisarPorId(carrinho?.id!!, Ativo(1))
        Assertions.assertThat(car?.id).isEqualTo(carrinho?.id!!)
    }

}