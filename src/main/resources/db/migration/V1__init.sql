--Usuario

DROP TABLE IF EXISTS usuario;

CREATE TABLE usuario (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  nome VARCHAR(250) NOT NULL,
  cpf VARCHAR(11) NOT NULL unique,
  login VARCHAR(50) NOT NULL unique,
  senha VARCHAR(250) NOT NULL,
  saldo DOUBLE NOT NULL,
  ativo INT NOT NULL
);

INSERT INTO usuario (nome, cpf, login, senha, saldo, ativo) VALUES
  ('Joao', '94174412561', 'joao', '$2a$10$S7kfl6cVriCcWmZYQZZdfOgHS1pDIOXNdzvFj06srqZIIgnwVmJBi', 0.0, 1),
  ('Ivo', '91153010526', 'ivo', '$2a$10$S7kfl6cVriCcWmZYQZZdfOgHS1pDIOXNdzvFj06srqZIIgnwVmJBi', 0.0, 1);

--Gênero

DROP TABLE IF EXISTS genero;

CREATE TABLE genero (
  id INT AUTO_INCREMENT PRIMARY KEY,
  nome VARCHAR(50) NOT NULL,
  ativo INT NOT NULL
);

INSERT INTO genero (nome, ativo) VALUES
  ('pop', 1),
  ('mpb', 1),
  ('classic', 1),
  ('rock', 1);

--Dia

DROP TABLE IF EXISTS dia;

CREATE TABLE dia (
  id INT PRIMARY KEY,
  nome VARCHAR(50) NOT NULL,
  ativo INT NOT NULL
);

INSERT INTO dia (id, nome, ativo) VALUES
  (1, 'Domingo', 1),
  (2, 'Segunda-feira', 1),
  (3, 'Terça-feira', 1),
  (4, 'Quarta-feira', 1),
  (5, 'Quinta-feira', 1),
  (6, 'Sexta-feira', 1),
  (7, 'Sabado', 1);

--Percentual de Cashback

DROP TABLE IF EXISTS percentual_cashback;

CREATE TABLE percentual_cashback (
  id INT AUTO_INCREMENT PRIMARY KEY,
  percentual INT NOT NULL,
  ativo INT NOT NULL,
  idgenero INT,
  iddia INT,
  FOREIGN KEY (idgenero) REFERENCES genero(id),
  FOREIGN KEY (iddia) REFERENCES dia(id)
);

INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
 select 25, id, 1, 1 from genero where nome = 'pop';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 7, id, 2, 1 from genero where nome = 'pop';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 6, id, 3, 1 from genero where nome = 'pop';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 2, id, 4, 1 from genero where nome = 'pop';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 10, id, 5, 1 from genero where nome = 'pop';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 15, id, 6, 1 from genero where nome = 'pop';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 20, id, 7, 1 from genero where nome = 'pop';

INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 30, id, 1, 1 from genero where nome = 'mpb';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 5, id, 2, 1 from genero where nome = 'mpb';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 10, id, 3, 1 from genero where nome = 'mpb';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 15, id, 4, 1 from genero where nome = 'mpb';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 20, id, 5, 1 from genero where nome = 'mpb';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 25, id, 6, 1 from genero where nome = 'mpb';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 30, id, 7, 1 from genero where nome = 'mpb';

INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 35, id, 1, 1 from genero where nome = 'classic';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 3, id, 2, 1 from genero where nome = 'classic';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 5, id, 3, 1 from genero where nome = 'classic';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 8, id, 4, 1 from genero where nome = 'classic';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 13, id, 5, 1 from genero where nome = 'classic';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 18, id, 6, 1 from genero where nome = 'classic';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 25, id, 7, 1 from genero where nome = 'classic';

INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 40, id, 1, 1 from genero where nome = 'rock';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 10, id, 2, 1 from genero where nome = 'rock';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 15, id, 3, 1 from genero where nome = 'rock';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 15, id, 4, 1 from genero where nome = 'rock';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 15, id, 5, 1 from genero where nome = 'rock';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 20, id, 6, 1 from genero where nome = 'rock';
INSERT INTO percentual_cashback (percentual, idGenero, idDia, ativo)
select 40, id, 7, 1 from genero where nome = 'rock';

--Artista

DROP TABLE IF EXISTS artista;

CREATE TABLE artista (
  id INT AUTO_INCREMENT PRIMARY KEY,
  nome VARCHAR(50) NOT NULL,
  spotifyid VARCHAR(100) NOT NULL unique,
  ativo INT NOT NULL
);

--Artista Genero
DROP TABLE IF EXISTS artista_genero;

CREATE TABLE artista_genero (
 id INT AUTO_INCREMENT PRIMARY KEY,
 idgenero INT,
 idartista INT,
 FOREIGN KEY (idgenero) REFERENCES genero(id),
 FOREIGN KEY (idartista) REFERENCES artista(id)
);

--Album

DROP TABLE IF EXISTS album;

CREATE TABLE album (
  id INT AUTO_INCREMENT PRIMARY KEY,
  nome VARCHAR(50) NOT NULL,
  ativo INT NOT NULL,
  spotifyid VARCHAR(100) NOT NULL unique,
  idartista INT,
  FOREIGN KEY (idartista) REFERENCES artista(id)
);

--Tipo de pagamento

DROP TABLE IF EXISTS tipo_pagamento;

CREATE TABLE tipo_pagamento (
  id INT PRIMARY KEY,
  descricao VARCHAR(50) NOT NULL,
  ativo INT NOT NULL
);

INSERT INTO tipo_pagamento (id, descricao, ativo) VALUES
  (1, 'À vista', 1),
  (2, 'Crédito', 1),
  (3, 'Débito', 1);

--Carrinho
DROP TABLE IF EXISTS carrinho;

CREATE TABLE carrinho (
    id INT AUTO_INCREMENT PRIMARY KEY,
    datacriacao TIMESTAMP NOT NULL,
    datavalidade TIMESTAMP NOT NULL,
    idusuario INT NOT NULL,
    ativo INT NOT NULL,
    FOREIGN KEY (idusuario) REFERENCES usuario(id)
);

--Carrinho Album

DROP TABLE IF EXISTS carrinho_album;

CREATE TABLE carrinho_album (
 id INT AUTO_INCREMENT PRIMARY KEY,
 valor DOUBLE NOT NULL,
 quantidade INT NOT NULL,
 idalbum INT NOT NULL,
 idcarrinho INT NOT NULL,
 FOREIGN KEY (idalbum) REFERENCES album(id),
 FOREIGN KEY (idcarrinho) REFERENCES carrinho(id)
);

--Compra

DROP TABLE IF EXISTS compra;

CREATE TABLE compra (
  id INT AUTO_INCREMENT PRIMARY KEY,
  data TIMESTAMP NOT NULL,
  valortotal DOUBLE NOT NULL,
  cashback DOUBLE NOT NULL,
  qtdvezespagamento INT NOT NULL,
  idtipopagamento INT NOT NULL,
  idcarrinho INT NOT NULL,
  ativo INT NOT NULL,
  FOREIGN KEY (idtipopagamento) REFERENCES tipo_pagamento(id),
  FOREIGN KEY (idcarrinho) REFERENCES carrinho(id)
);

DROP TABLE IF EXISTS auditoria_cashback;

CREATE TABLE auditoria_cashback (
    id INT AUTO_INCREMENT PRIMARY KEY,
    data TIMESTAMP NOT NULL,
    valor DOUBLE NOT NULL,
    tipo CHAR NOT NULL,
    idcompra INT NOT NULL,
    FOREIGN KEY (idcompra) REFERENCES compra(id)
);