package br.com.api.beblueApp

import br.com.api.beblueApp.userInterface.security.SecurityConstants
import br.com.api.beblueApp.application.service.LoginService
import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.Bean
import org.springframework.hateoas.config.EnableHypermediaSupport
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.util.*
import javax.annotation.PostConstruct

@EnableHypermediaSupport(type = [EnableHypermediaSupport.HypermediaType.HAL])
@SpringBootApplication
@EnableFeignClients
class BeblueAppApplication: SpringBootServletInitializer(){

    var tokenSpotify:String? = null

    var dataExpiracao = Date()

    @Autowired
    private lateinit var service:LoginService

    @PostConstruct
    fun init(){
        try {
			val response = service.autenticar()
            dataExpiracao = Date(System.currentTimeMillis() + ((response.expires_in!!-120) * 1000) )
            tokenSpotify = SecurityConstants.TOKEN_PREFIX+response.access_token
        }catch (e:Exception){
            e.printStackTrace()
        }

    }

    @Bean
    fun tokenSpotify():String?{
        return tokenSpotify
    }

    @Bean
    fun dataExpiracao():Date?{
        return dataExpiracao
    }

    @Bean
    fun bCryptPasswordEncoder(): BCryptPasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
    fun modelMapper(): ModelMapper {
        return ModelMapper()
    }

    override fun configure(application: SpringApplicationBuilder): SpringApplicationBuilder {
        return application.sources(BeblueAppApplication::class.java)
    }


}

fun main(args: Array<String>) {
	runApplication<BeblueAppApplication>(*args)
}