package br.com.api.beblueApp.domain.model

class ImagemItemModelResponse {

    var height: Int? = null

    var url: String? = null

    var width: Int? = null

}