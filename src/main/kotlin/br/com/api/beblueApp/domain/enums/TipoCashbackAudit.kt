package br.com.api.beblueApp.domain.enums

enum class TipoCashbackAudit(val tipo: String) {
    ENTRADA("E"), SAIDA("S")
}