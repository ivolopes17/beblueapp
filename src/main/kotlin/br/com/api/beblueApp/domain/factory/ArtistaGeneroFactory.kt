package br.com.api.beblueApp.domain.factory

import br.com.api.beblueApp.domain.entity.Artista
import br.com.api.beblueApp.domain.entity.ArtistaGenero
import br.com.api.beblueApp.domain.entity.Genero

abstract class ArtistaGeneroFactory {


    companion object Factory{
        fun criar(genero: Genero?, artista: Artista?): ArtistaGenero {
            val enidade = ArtistaGenero(null, genero, artista)
            enidade.validarSemId()

            return enidade
        }

        fun criar(id:Int, genero: Genero?, artista: Artista?): ArtistaGenero {
            val enidade = ArtistaGenero(id, genero, artista)
            enidade.isValid()
            return enidade
        }

    }

}