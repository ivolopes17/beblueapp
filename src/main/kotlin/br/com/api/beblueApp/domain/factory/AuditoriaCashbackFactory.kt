package br.com.api.beblueApp.domain.factory

import br.com.api.beblueApp.domain.entity.AuditoriaCashback
import br.com.api.beblueApp.domain.entity.Compra
import br.com.api.beblueApp.domain.enums.TipoCashbackAudit
import java.util.*

abstract class AuditoriaCashbackFactory {


    companion object Factory{
        fun criar(id: Int?, data: Date?, valor: Double?, tipo: TipoCashbackAudit?, compra: Compra?): AuditoriaCashback {
            val auditoria = AuditoriaCashback(id, data, valor, tipo, compra)
            auditoria.isValid()
            return auditoria
        }

        fun criar(data: Date?, valor: Double?, tipo: TipoCashbackAudit?, compra: Compra?): AuditoriaCashback {
            val auditoria = AuditoriaCashback(null, data, valor, tipo, compra)
            auditoria.validarSemId()
            return auditoria
        }
    }

}