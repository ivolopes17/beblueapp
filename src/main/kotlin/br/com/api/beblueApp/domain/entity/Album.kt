package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "album")
class Album(id: Int?, nome: Nome?, spotifyId: String?, artista: Artista?, ativo: Ativo) : AbstractEntity(id), Serializable {

    @Embedded
    val nome:Nome? = nome

    @Column(name = "spotifyid", nullable = false, unique = true)
    val spotifyId:String? = spotifyId

    @Embedded
    val ativo: Ativo = ativo

    @ManyToOne
    @JoinColumn(name = "idartista")
    val artista: Artista? = artista

    fun isValid(){
        isIdValido()
        validarNome()
        ativo.isValid()
        validarArtista()
        isSpotifyIdValid()
    }

    fun validarSemId(){
        validarNome()
        ativo.isValid()
        validarArtista()
        isSpotifyIdValid()
    }

    private fun validarNome(){
        nome?.isValid() ?: throw BeblueValidationException("O nome está nulo")
    }

    private fun validarArtista(){
        artista?.isValid() ?: throw BeblueValidationException("O artista está nulo")
    }

    private fun isSpotifyIdValid(){
        if( spotifyId == null || spotifyId.trim() == ""){
            throw BeblueValidationException("O campo spotifyId do artista é inválido")
        }
    }
}