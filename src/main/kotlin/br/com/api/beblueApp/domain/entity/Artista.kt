package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "artista")
class Artista(id: Int?, nome: Nome?, spotifyId:String?, ativo: Ativo): AbstractEntity(id), Serializable{

    @Embedded
    val nome:Nome? = nome

    @Column(name = "spotifyid", nullable = false, unique = true)
    val spotifyId:String? = spotifyId

    @Column(name = "ativo", nullable = false)
    val ativo: Ativo = ativo

    @OneToMany(mappedBy = "artista", fetch = FetchType.EAGER, cascade = [CascadeType.ALL],
            orphanRemoval = true)
    var generos: List<ArtistaGenero>? = null
        private set(value){
            field = value
        }

    fun adicionarGeneros(artistaGenero: ArtistaGenero){

        if( generos == null ){
            generos = ArrayList()
        }

        (generos as ArrayList).add(artistaGenero)
    }

    fun isValid(){

        isIdValido()
        validarNome()
        ativo.isValid()
        validarSpotifyId()

    }

    fun validarSemId(){

        validarNome()
        ativo.isValid()
        validarSpotifyId()

    }

    private fun validarSpotifyId(){
        if( spotifyId == null || spotifyId.trim() == ""){
            throw BeblueValidationException("O campo spotifyId do artista é inválido")
        }
    }

    private fun validarNome(){
        nome?.isValid() ?: throw BeblueValidationException("O nome está nulo")
    }

}