package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import br.com.api.beblueApp.application.util.DateUtil
import br.com.api.beblueApp.domain.factory.CarrinhoAlbumFactory
import org.hibernate.collection.internal.PersistentBag
import java.io.Serializable
import java.util.*
import javax.persistence.*
import kotlin.collections.ArrayList

@Entity
@Table(name = "carrinho")
class Carrinho(id: Int?, dataCriacao: Date?, auxUsuario: Usuario?, ativo: Ativo?, dataValidade:Date?): AbstractEntity(id), Serializable {

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "datacriacao", nullable = false)
    val dataCriacao: Date? = dataCriacao

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "datavalidade", nullable = false)
    var dataValidade: Date? = dataValidade
        private set(value){
            field = value
        }

    @Embedded
    var ativo:Ativo? = ativo
        private set(value){
            field = value
        }

    @ManyToOne
    @JoinColumn(name = "idusuario")
    private var usuario: Usuario? = auxUsuario

    @OneToMany(mappedBy = "carrinho", fetch = FetchType.LAZY, cascade = [CascadeType.ALL],
            orphanRemoval = true)
    var carrinhoAlbuns: List<CarrinhoAlbum>? = null
        private set(value){
            field = value
        }

    companion object{
        const val CARRINHO_TEMPO = 60000 * 60
    }

    fun calcularValorTotal():Double{
        var valorTotal = 0.0
        carrinhoAlbuns?.forEach {
            valorTotal = valorTotal.plus(it.valor!! * it.quantidade!!)
        }
        return valorTotal
    }

    fun aumentarSaldoUsuario(cashback:Double){
        usuario?.aumentarSaldo(cashback)
    }

    fun inativar(){
        ativo = Ativo(0)
    }

    fun adicionarTempo(){
        dataValidade = Date(System.currentTimeMillis() + CARRINHO_TEMPO)
    }

    fun adicionarAlbum(album: Album, quantidade: Int, valor: Double){

        val carrinhoAlbum = CarrinhoAlbumFactory.criar(valor, quantidade, album, this)
        carrinhoAlbum.validarSemId()

        if( carrinhoAlbuns == null ){
            carrinhoAlbuns = ArrayList()
            (carrinhoAlbuns as ArrayList).add(carrinhoAlbum)
        }else {
            (carrinhoAlbuns as PersistentBag).add(carrinhoAlbum)
        }

    }

    fun isValid(){
        isIdValido()
        validarAtivo()
        validarUsuario()
        isDataCriacaoValida()
        isDataValidadeValida()
    }

    fun validarSemId(){
        validarAtivo()
        validarUsuario()
        isDataCriacaoValida()
        isDataValidadeValida()
    }

    private fun validarAtivo(){
        ativo?.isValid() ?: throw BeblueValidationException("O ativo é nulo")
    }

    private fun validarUsuario(){
        usuario?.isValid() ?: throw BeblueValidationException("O usuário é nulo")
    }

    private fun isDataCriacaoValida(){
        if(dataCriacao == null || !DateUtil.isSameDay(dataCriacao, Date()) ){
            throw BeblueValidationException("A data de criação não é a data de hoje")
        }
    }

    private fun isDataValidadeValida(){
        if( dataValidade == null || !dataValidade!!.after( Date() ) ){
            throw BeblueValidationException("A data de validade é inválida")
        }
    }

}