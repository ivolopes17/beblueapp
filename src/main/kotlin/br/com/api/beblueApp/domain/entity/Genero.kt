package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "genero")
class Genero(id:Int?, nome:Nome?, ativo: Ativo?): AbstractEntity(id), Serializable {

    val nome:Nome? = nome

    @Embedded
    val ativo:Ativo? = ativo

    fun isValid(){
        isIdValido()
        validarNome()
        validarAtivo()
    }

    fun validarSemId(){
        validarNome()
        validarAtivo()
    }

    fun validarNome(){
        nome?.isValid() ?: throw BeblueValidationException("O campo nome é nulo")
    }

    fun validarAtivo(){
        ativo?.isValid() ?: throw BeblueValidationException("O campo ativo é nulo")
    }
}