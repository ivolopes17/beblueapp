package br.com.api.beblueApp.domain.entity

import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "dia")
class Dia(id: Int?, nome: Nome?): AbstractEntity(id), Serializable {

    @Embedded
    val nome:Nome? = nome

    fun isValid(){
        isIdValido()
        nome?.isValid()
    }

}