package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class Nome(auxNome:String?): Serializable {

    @Column(name = "nome", nullable = false)
    val valor:String? = auxNome

    fun isValid(){

        if( valor == null || valor.trim() == "" ){
            throw BeblueValidationException("O nome está vazio")
        }

    }
}