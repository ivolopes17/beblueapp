package br.com.api.beblueApp.domain.factory

import br.com.api.beblueApp.domain.entity.Album
import br.com.api.beblueApp.domain.entity.Artista
import br.com.api.beblueApp.domain.entity.Ativo
import br.com.api.beblueApp.domain.entity.Nome
import br.com.api.beblueApp.domain.model.AlbumItemModelResponse

abstract class AlbumFactory {

    companion object Factory{
        fun criar(nome: String?, spotifyId: String?, artista: Artista?): Album {
            val album = Album(null, Nome(nome), spotifyId, artista, Ativo(1))
            album.validarSemId()
            return album
        }

        fun criar(albumResponse: AlbumItemModelResponse, artista: Artista?):Album{
            val album = Album(null, Nome(albumResponse.name), albumResponse.id, artista, Ativo(1) )
            album.validarSemId()
            return album
        }

        fun criar(id: Int?, nome: String?, spotifyId: String?, artista: Artista?): Album {

            val album = Album(id, Nome(nome), spotifyId, artista, Ativo(1))
            album.isValid()
            return album
        }
    }

}