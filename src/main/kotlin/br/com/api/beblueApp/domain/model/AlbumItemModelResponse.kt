package br.com.api.beblueApp.domain.model

class AlbumItemModelResponse {

    var id: String? = null

    var name: String? = null

    var artists: List<ArtistaItemModelResponse>? = null

    var images: List<ImagemItemModelResponse>? = null

    constructor()
    constructor(id: String?, name: String?) {
        this.id = id
        this.name = name
    }


}