package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.application.util.DateUtil
import br.com.api.beblueApp.domain.enums.TipoCashbackAudit
import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "auditoria_cashback")
class AuditoriaCashback(id: Int?, data: Date?, valor: Double?, tipo: TipoCashbackAudit?, compra: Compra?): AbstractEntity(id), Serializable {

    @Column(name = "data", nullable = false)
    val data: Date? = data

    @Column(name = "valor", nullable = false)
    val valor: Double? = valor

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo", nullable = false)
    val tipo: TipoCashbackAudit? = tipo

    @ManyToOne
    @JoinColumn(name = "idcompra")
    val compra: Compra? = compra

    fun isValid(){
        isIdValido()
        isDataValida()
        isTipoValida()
        isValorValido()
        validarCompra()
    }

    fun validarSemId(){
        isDataValida()
        isTipoValida()
        isValorValido()
        validarCompra()
    }

    private fun validarCompra(){
        compra?.isValid() ?: throw BeblueValidationException("A compra está nula")
    }

    private fun isDataValida(){
        if(data == null || !DateUtil.isSameDay(data, Date()) ){
            throw BeblueValidationException("A data não é a de hoje")
        }
    }

    private fun isTipoValida(){
        if( tipo == null ){
            throw BeblueValidationException("O tipo não foi informado")
        }
    }

    private fun isValorValido(){
        if( valor == null || valor < 0){
            throw BeblueValidationException("O valor é inválido")
        }
    }

}