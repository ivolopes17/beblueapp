package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import javax.persistence.*

@MappedSuperclass
abstract class AbstractEntity(id:Int?) {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    val id: Int? = id

    fun isIdValido(){

        if( id == null ){
            throw BeblueValidationException("O id está vazio")
        }else if (id <= 0){
            throw BeblueValidationException("O id é inválido")
        }

    }
}