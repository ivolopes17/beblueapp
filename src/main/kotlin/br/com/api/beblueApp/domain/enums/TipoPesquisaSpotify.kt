package br.com.api.beblueApp.domain.enums

enum class TipoPesquisaSpotify(val tipo: String) {
    ALBUM("album"), ARTISTA("artist")
}