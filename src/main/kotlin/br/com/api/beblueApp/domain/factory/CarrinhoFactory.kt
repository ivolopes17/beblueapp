package br.com.api.beblueApp.domain.factory

import br.com.api.beblueApp.domain.entity.Ativo
import br.com.api.beblueApp.domain.entity.Carrinho
import br.com.api.beblueApp.domain.entity.Usuario
import java.util.*

abstract class CarrinhoFactory {

    companion object{

        fun criar(id: Int?, dataCriacao: Date?, usuario: Usuario?): Carrinho {
            val carrinho = Carrinho(id, dataCriacao, usuario, Ativo(1), null)
            carrinho.adicionarTempo()
            carrinho.isValid()
            return carrinho
        }

        fun criar(dataCriacao: Date?, usuario: Usuario?): Carrinho {
            val carrinho = Carrinho(null, dataCriacao, usuario, Ativo(1), null)
            carrinho.adicionarTempo()
            carrinho.validarSemId()
            return carrinho
        }
    }


}