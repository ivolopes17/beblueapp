package br.com.api.beblueApp.domain.model

class ArtistaItemModelResponse {

    var id: String? = null

    var name: String? = null

    var genres: List<String> = ArrayList()

    constructor()
    constructor(id: String?, name: String?, genres: List<String>) {
        this.id = id
        this.name = name
        this.genres = genres
    }

}