package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class Ativo(auxAtivo:Int): Serializable {

    @Column(name = "ativo", nullable = false)
    var ativo:Int = auxAtivo
        private set(value){
            field = value
        }

    fun inativar(){
        ativo = 0
    }

    fun isValid(){
        if( this.ativo < 0 || ativo > 1 ){
            throw BeblueValidationException("O campo ativo é inválido")
        }
    }
}