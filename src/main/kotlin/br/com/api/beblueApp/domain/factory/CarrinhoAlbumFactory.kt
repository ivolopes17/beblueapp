package br.com.api.beblueApp.domain.factory

import br.com.api.beblueApp.domain.entity.Album
import br.com.api.beblueApp.domain.entity.Carrinho
import br.com.api.beblueApp.domain.entity.CarrinhoAlbum

abstract class CarrinhoAlbumFactory {

    companion object Factory{
        fun criar(id:Int?, valor: Double?, quantidade: Int?, album: Album?, carrinho: Carrinho?): CarrinhoAlbum {
            val carrinhoAlbum = CarrinhoAlbum(id, valor, quantidade, album, carrinho)
            carrinhoAlbum.isValid()
            return carrinhoAlbum
        }

        fun criar(valor: Double?, quantidade: Int?, album: Album?, carrinho: Carrinho?): CarrinhoAlbum {
            val carrinhoAlbum = CarrinhoAlbum(null, valor, quantidade, album, carrinho)
            carrinhoAlbum.validarSemId()
            return carrinhoAlbum
        }
    }

}