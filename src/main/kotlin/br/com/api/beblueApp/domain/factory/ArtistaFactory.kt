package br.com.api.beblueApp.domain.factory

import br.com.api.beblueApp.domain.entity.Artista
import br.com.api.beblueApp.domain.entity.Ativo
import br.com.api.beblueApp.domain.entity.Nome
import br.com.api.beblueApp.domain.model.ArtistaItemModelResponse

abstract class ArtistaFactory {

    companion object Factory{
        fun criar(nome: String?, spotifyId:String?): Artista {
            val value = Artista(null, Nome(nome), spotifyId, Ativo(1))
            value.validarSemId()
            return value
        }

        fun criar(artistaResponse:ArtistaItemModelResponse): Artista {
            val value = Artista(null, Nome(artistaResponse.name), artistaResponse.id, Ativo(1))
            value.validarSemId()
            return value
        }

        fun criar(id: Int?, nome: String?, spotifyId:String?): Artista {
            val value = Artista(id, Nome(nome), spotifyId, Ativo(1))
            value.isValid()
            return value
        }
    }

}