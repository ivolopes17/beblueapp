package br.com.api.beblueApp.domain.factory

import br.com.api.beblueApp.domain.entity.Ativo
import br.com.api.beblueApp.domain.entity.Genero
import br.com.api.beblueApp.domain.entity.Nome

abstract class GeneroFactory {

    companion object Factory{
        fun criar(nome: String?): Genero {
            val entidade = Genero(null, Nome(nome), Ativo(1))
            entidade.validarSemId()
            return entidade
        }

        fun criar(id:Int?, nome: String?): Genero {
            val entidade = Genero(id, Nome(nome), Ativo(1))
            entidade.validarSemId()
            return entidade
        }
    }

}