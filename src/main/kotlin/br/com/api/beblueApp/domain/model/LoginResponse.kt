package br.com.api.beblueApp.domain.model

class LoginResponse {

    var access_token:String? = null

    var token_type:String? = null

    var expires_in: Int? = null

    var scope:String? = null
}