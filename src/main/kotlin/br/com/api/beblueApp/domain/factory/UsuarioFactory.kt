package br.com.api.beblueApp.domain.factory

import br.com.api.beblueApp.domain.entity.Ativo
import br.com.api.beblueApp.domain.entity.Nome
import br.com.api.beblueApp.domain.entity.Usuario

abstract class UsuarioFactory {

    companion object Factory{
        fun criarParaTeste(id: Int?, nome: String?, login: String?, saldo: Double?): Usuario {
            val usuario = Usuario(id, Nome(nome), "09876458733", login, "3443830", saldo, Ativo(1))
            usuario.isValid()
            return usuario
        }
    }

}