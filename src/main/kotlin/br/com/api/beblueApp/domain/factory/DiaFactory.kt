package br.com.api.beblueApp.domain.factory

import br.com.api.beblueApp.domain.entity.Dia
import br.com.api.beblueApp.domain.entity.Nome

abstract class DiaFactory {

    companion object Factory{
        fun criar(id: Int?, nome: String?): Dia {
            val dia = Dia(id, Nome(nome))
            dia.isValid()
            return dia
        }
    }

}