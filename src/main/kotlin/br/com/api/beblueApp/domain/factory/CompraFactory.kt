package br.com.api.beblueApp.domain.factory

import br.com.api.beblueApp.domain.entity.Ativo
import br.com.api.beblueApp.domain.entity.Carrinho
import br.com.api.beblueApp.domain.entity.Compra
import br.com.api.beblueApp.domain.entity.TipoPagamento
import java.util.*

abstract class CompraFactory {

    companion object Factory{

        fun criar(id: Int?, data: Date?, valorTotal: Double?, cashback: Double?, qtdVezesPagamento: Int?, tipoPagamento: TipoPagamento?, carrinho: Carrinho?): Compra {
            val compra = Compra(id, data, valorTotal, cashback, qtdVezesPagamento, Ativo(1), tipoPagamento, carrinho)
            compra.isValid()
            return compra
        }

        fun criar(data: Date?, qtdVezesPagamento: Int?, tipoPagamento: TipoPagamento?, carrinho: Carrinho?): Compra {
            val compra = Compra(null, data, 0.0, 0.0, qtdVezesPagamento,
                                    Ativo(1), tipoPagamento, carrinho)
            compra.validarSemId()
            return compra
        }

    }

}