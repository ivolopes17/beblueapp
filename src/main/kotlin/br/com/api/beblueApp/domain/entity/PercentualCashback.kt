package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "percentual_cashback")
class PercentualCashback(id: Int?, percentual: Int?, dia:Dia?, genero: Genero?): AbstractEntity(id), Serializable {

    @Column(name = "percentual", nullable = false)
    val percentual:Int? = percentual

    @ManyToOne
    @JoinColumn(name = "idgenero")
    val genero: Genero? = genero

    @ManyToOne
    @JoinColumn(name = "iddia")
    val dia: Dia? = dia

    fun isValid(){
        isIdValido()
        validarGenero()
        validarDia()
        validarPercentual()
    }

    fun validarGenero(){
        genero?.isValid() ?: throw BeblueValidationException("O genero é nulo")
    }

    fun validarPercentual(){
        if( percentual == null || percentual < 0 || percentual > 100 ){
            throw BeblueValidationException("O percentual é inválido")
        }
    }

    fun validarDia(){
        dia?.isValid() ?: throw BeblueValidationException("O dia é nulo")
    }
}