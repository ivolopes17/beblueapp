package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "tipo_pagamento")
class TipoPagamento(id: Int?, descricao: String?): AbstractEntity(id), Serializable {

    @Column(name = "descricao", nullable = false)
    val descricao:String? = descricao

    fun isValid(){
        isIdValido()

        if( descricao == null || descricao.trim() == "" ){
            throw BeblueValidationException("A descrição está vazia")
        }
    }

    override fun toString(): String {
        return "$descricao"
    }


}