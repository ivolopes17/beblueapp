package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "usuario")
class Usuario(id: Int?, nome: Nome?, cpf:String?, login: String?, senha:String?, saldo: Double?, ativo: Ativo?): AbstractEntity(id), Serializable {

    @Embedded
    val nome:Nome? = nome

    @Column(name = "cpf", nullable = false, unique = true)
    val cpf:String? = cpf

    @Column(name = "login", nullable = false, unique = true)
    val login:String? = login

    @Column(name = "senha", nullable = false)
    val senha:String? = senha

    @Column(name = "saldo", nullable = false)
    var saldo:Double? = saldo
        private set(value){
            field = value
        }

    @Embedded
    val ativo:Ativo? = ativo

    fun isValid(){
        isIdValido()
        nome?.isValid() ?: throw BeblueValidationException("O nome está nulo")

        if( cpf == null || cpf.trim() == ""){
            throw BeblueValidationException("O cpf do usuário está vazio")
        }

        if( login == null || login.trim() == ""){
            throw BeblueValidationException("O login do usuário está vazio")
        }

        if( senha == null || senha.trim() == ""){
            throw BeblueValidationException("A senha do usuário está vazia")
        }

        if( saldo == null || saldo!! < 0 ){
            throw BeblueValidationException("O saldo do usuário é invalido")
        }
    }

    fun aumentarSaldo(valor:Double){

        if( valor >= 0 ) {
            saldo = saldo?.plus(valor) ?: valor
        }else{
            throw BeblueValidationException("O valor é invalido")
        }
    }

}