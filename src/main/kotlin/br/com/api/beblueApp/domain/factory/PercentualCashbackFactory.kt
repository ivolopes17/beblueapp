package br.com.api.beblueApp.domain.factory

import br.com.api.beblueApp.domain.entity.Dia
import br.com.api.beblueApp.domain.entity.Genero
import br.com.api.beblueApp.domain.entity.PercentualCashback

abstract class PercentualCashbackFactory {

    companion object Factory{
        fun criar (id: Int?, percentual: Int?, dia:Dia?, genero: Genero?): PercentualCashback {
            val percentualCashback = PercentualCashback(id, percentual, dia, genero)
            percentualCashback.isValid()
            return percentualCashback
        }
    }

}