package br.com.api.beblueApp.domain.factory

import br.com.api.beblueApp.domain.entity.TipoPagamento

abstract class TipoPagamentoFactory {

    companion object Factory{
        fun criar(id: Int?, descricao: String?): TipoPagamento {
            val tipoPagamento = TipoPagamento(id, descricao)
            tipoPagamento.isValid()
            return tipoPagamento
        }
    }

}