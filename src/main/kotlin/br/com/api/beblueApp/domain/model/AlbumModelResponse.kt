package br.com.api.beblueApp.domain.model

class AlbumModelResponse {

    var href: String? = null

    var items: List<AlbumItemModelResponse>? = null

    var limit: Int? = null

    var next: String? = null

    var offset: Int? = null

    var previous: String? = null

    var total: Int? = null

}