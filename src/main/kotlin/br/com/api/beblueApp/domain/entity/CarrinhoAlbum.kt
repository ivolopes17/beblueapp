package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "carrinho_album")
class CarrinhoAlbum(id:Int?, valor: Double?, quantidade: Int?, album: Album?, carrinho: Carrinho?): AbstractEntity(id), Serializable {

    @Column(name = "valor", nullable = false)
    val valor: Double? = valor

    @Column(name = "quantidade", nullable = false)
    val quantidade: Int? = quantidade

    @ManyToOne
    @JoinColumn(name = "idalbum")
    val album: Album? = album

    @ManyToOne
    @JoinColumn(name = "idcarrinho")
    val carrinho: Carrinho? = carrinho

    fun isValid(){
        isIdValido()
        isQuantidadeValida()
        validarAlbum()
        validarCarrinho()
        isValorValido()
    }

    fun validarSemId(){
        isQuantidadeValida()
        validarAlbum()
        validarCarrinho()
        isValorValido()
    }

    fun calcularValorTotal():Double{
        isQuantidadeValida()
        isValorValido()
        return valor!!*quantidade!!
    }

    fun validarAlbum(){
        album?.isValid() ?: throw BeblueValidationException("O album é nulo")
    }

    fun validarCarrinho(){
        carrinho?.isValid() ?: throw BeblueValidationException("O carrinho é nulo")
    }

    private fun isQuantidadeValida(){
        if( quantidade == null || quantidade < 1 || quantidade > 10){
            throw BeblueValidationException("A quantidade é inválida")
        }
    }

    private fun isValorValido(){
        if( valor == null || valor < 0){
            throw BeblueValidationException("O valor é inválido")
        }
    }

}