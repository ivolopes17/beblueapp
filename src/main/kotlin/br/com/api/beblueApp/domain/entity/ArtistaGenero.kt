package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "artista_genero")
class ArtistaGenero(id:Int?, genero: Genero?, artista: Artista?): AbstractEntity(id), Serializable {

    @ManyToOne
    @JoinColumn(name = "idgenero")
    val genero: Genero? = genero

    @ManyToOne
    @JoinColumn(name = "idartista")
    val artista: Artista? = artista

    fun isValid(){
        isIdValido()
        validarGenero()
        validarArtista()
    }

    fun validarSemId(){
        validarGenero()
        validarArtista()
    }

    fun validarGenero(){
        genero?.isValid() ?: throw BeblueValidationException("O genero é nulo")
    }

    fun validarArtista(){
        artista?.validarSemId() ?: throw BeblueValidationException("O artista é nulo")
    }
}