package br.com.api.beblueApp.domain.entity

import br.com.api.beblueApp.userInterface.exceptions.BeblueValidationException
import br.com.api.beblueApp.application.util.DateUtil
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "compra")
class Compra(id: Int?, data: Date?, valorTotal: Double?, cashback: Double?, qtdVezesPagamento: Int?, ativo: Ativo?,
             auxTipoPagamento: TipoPagamento?, auxCarrinho: Carrinho?): AbstractEntity(id), Serializable {

    @Column(name = "data", nullable = false)
    val data: Date? = data

    @Column(name = "valortotal", nullable = false)
    var valorTotal: Double? = valorTotal
        private set(value){
            field = value
        }

    @Column(name = "cashback", nullable = false)
    var cashback: Double? = cashback
        private set(value){
            field = value
        }

    @Column(name = "qtdvezespagamento", nullable = false)
    val qtdVezesPagamento: Int? = qtdVezesPagamento

    @ManyToOne
    @JoinColumn(name = "idtipopagamento")
    private var tipoPagamento: TipoPagamento? = auxTipoPagamento

    @ManyToOne
    @JoinColumn(name = "idcarrinho")
    private var carrinho: Carrinho? = auxCarrinho


    @Column(name = "ativo", nullable = false)
    val ativo:Ativo? = ativo

    fun isValid(){
        isIdValido()
        ativo?.isValid()
        isDataValida()
        isValorValido()
        isCashbackValido()
        isQtdPagamentoValido()
        isTipoPagamentoValido()
    }

    fun validarSemId(){
        ativo?.isValid()
        isDataValida()
        isValorValido()
        isCashbackValido()
        isQtdPagamentoValido()
        isTipoPagamentoValido()
    }

    fun calcularValorTotal(){
        this.valorTotal = carrinho?.calcularValorTotal()
    }

    fun adicionarCashback(valor:Double, percentual:Int){

        if( valor > 0 && percentual > 0 ) {
            val calculado = (valor * percentual) / 100

            this.cashback =  this.cashback?.plus(calculado) ?: calculado
        }else{
            throw BeblueValidationException("O campo valor ou percentual tem que ser maior que zero")
        }

    }

    fun inativarCarrinho(){
        carrinho?.inativar()
    }

    fun aumentarSaldoUsuario(cashback:Double){
        carrinho?.aumentarSaldoUsuario(cashback)
    }

    fun getCarrinhoAlbuns():List<CarrinhoAlbum>?{
        return carrinho?.carrinhoAlbuns
    }

    private fun isDataValida(){
        if(data == null || !DateUtil.isSameDay(data, Date()) ){
            throw BeblueValidationException("A data não é a de hoje")
        }
    }

    private fun isValorValido(){
        if( valorTotal == null || valorTotal!! < 0){
            throw BeblueValidationException("O valor total é inválido")
        }
    }

    private fun isCashbackValido(){
        if( cashback == null || cashback!! < 0){
            throw BeblueValidationException("O valor do cashback é inválido")
        }
    }

    private fun isQtdPagamentoValido(){
        if( qtdVezesPagamento == null || qtdVezesPagamento < 1 || qtdVezesPagamento > 10){
            throw BeblueValidationException("O valor do parcelamento é inválido")
        }
    }

    private fun isTipoPagamentoValido(){
        if( tipoPagamento == null ){
            throw BeblueValidationException("O tipo de pagamento é inválido")
        }
    }

}