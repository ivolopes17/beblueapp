package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.domain.entity.TipoPagamento
import br.com.api.beblueApp.application.infrastructurePort.TipoPagamentoRepositoryPort
import br.com.api.beblueApp.infrastructure.repository.TipoPagamentoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TipoPagamentoRepositoryAdapter @Autowired constructor(private var repository: TipoPagamentoRepository):TipoPagamentoRepositoryPort {

    override fun pesquisarTodos(): List<TipoPagamento> {
        return repository.findAll().toList()
    }

    override fun pesquisarPorId(id: Int): TipoPagamento? {
        val result = repository.findById(id)

        if(result.isPresent){
            return result.get()
        }

        return null
    }

}