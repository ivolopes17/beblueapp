package br.com.api.beblueApp.infrastructure.repository

import br.com.api.beblueApp.domain.entity.Genero
import br.com.api.beblueApp.domain.entity.Nome
import org.springframework.data.repository.CrudRepository

interface GeneroRepository:CrudRepository<Genero, Int> {

    fun findByNome(nome:Nome): Genero?

}