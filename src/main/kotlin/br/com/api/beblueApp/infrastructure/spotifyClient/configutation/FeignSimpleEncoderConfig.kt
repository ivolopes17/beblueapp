package br.com.api.beblueApp.infrastructure.spotifyClient.configutation

import feign.codec.Encoder
import feign.codec.ErrorDecoder
import feign.form.FormEncoder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class FeignSimpleEncoderConfig {

    @Bean
    fun encoder(): Encoder {
        return FormEncoder()
    }

    @Bean
    fun errorDecoder(): ErrorDecoder {
        return FeignErrorDecoder()
    }
}