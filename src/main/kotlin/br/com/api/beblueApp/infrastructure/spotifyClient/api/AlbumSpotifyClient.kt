package br.com.api.beblueApp.infrastructure.spotifyClient.api

import br.com.api.beblueApp.infrastructure.spotifyClient.configutation.FeignSimpleEncoderConfig
import br.com.api.beblueApp.domain.model.AlbumItemModelResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestHeader

@FeignClient(name = "albumSpotifyClient", url = "https://api.spotify.com/v1/albums",
        configuration = [FeignSimpleEncoderConfig::class])
interface AlbumSpotifyClient {

    @GetMapping("/{id}")
    fun pesquisarPorId(@RequestHeader("Authorization") token: String,
                       @PathVariable(value = "id") id:String): AlbumItemModelResponse

}