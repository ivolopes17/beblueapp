package br.com.api.beblueApp.infrastructure.repository

import br.com.api.beblueApp.domain.entity.Ativo
import br.com.api.beblueApp.domain.entity.Carrinho
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param

interface CarrinhoRepository:CrudRepository<Carrinho, Int> {

    @Query("select c from Carrinho c join fetch c.usuario u where c.ativo = :ativo and u.id = :idUsuario")
    fun pesquisarPorAtivoAndUsuario(@Param("ativo") ativo:Ativo, @Param("idUsuario") usuarioId:Int): List<Carrinho>

    fun findByIdAndAtivo(id:Int, ativo:Ativo = Ativo(1)): Carrinho?

}