package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.domain.entity.Album
import br.com.api.beblueApp.domain.enums.TipoPesquisaSpotify
import br.com.api.beblueApp.application.infrastructurePort.AlbumRepositoryPort
import br.com.api.beblueApp.infrastructure.repository.AlbumRepository
import br.com.api.beblueApp.infrastructure.spotifyClient.api.AlbumSpotifyClient
import br.com.api.beblueApp.infrastructure.spotifyClient.api.PesquisarSpotifyClient
import br.com.api.beblueApp.domain.model.AlbumItemModelResponse
import br.com.api.beblueApp.domain.model.PesquisaModelResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AlbumRepositoryAdapter @Autowired constructor(private var albumRepository: AlbumRepository,
                                                    private var pesquisarClient: PesquisarSpotifyClient,
                                                    private var albumClient: AlbumSpotifyClient,
                                                    private var tokenSpotify: String): AlbumRepositoryPort {

    override fun pesquisarAlbumSpotify(nome:String, limit:Int, offset:Int): PesquisaModelResponse {
        return pesquisarClient.pesquisar(tokenSpotify, nome, TipoPesquisaSpotify.ALBUM.tipo, limit, offset)
    }

    override fun pesquisarAlbumSpotifyPorId(id: String): AlbumItemModelResponse {
        return albumClient.pesquisarPorId(tokenSpotify,id)
    }

    override fun salvar(album: Album): Album {
        return albumRepository.save(album)
    }

    override fun consultarPorSpotifyId(spotifyId: String): Album? {
        return albumRepository.findBySpotifyId(spotifyId)
    }

}