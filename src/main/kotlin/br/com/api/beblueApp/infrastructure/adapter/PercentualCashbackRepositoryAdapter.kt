package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.domain.entity.Dia
import br.com.api.beblueApp.domain.entity.Genero
import br.com.api.beblueApp.domain.entity.PercentualCashback
import br.com.api.beblueApp.application.infrastructurePort.PercentualCashbackRepositoryPort
import br.com.api.beblueApp.infrastructure.repository.PercentualCashbackRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PercentualCashbackRepositoryAdapter @Autowired constructor(private var repository:PercentualCashbackRepository):PercentualCashbackRepositoryPort {

    override fun pesquisarPorDiaGenero(dia: Dia, genero: Genero): PercentualCashback? {
        return repository.findByDiaAndGenero(dia, genero)
    }

}