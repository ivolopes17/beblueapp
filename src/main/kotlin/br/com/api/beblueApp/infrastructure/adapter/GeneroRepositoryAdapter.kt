package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.domain.entity.Genero
import br.com.api.beblueApp.domain.entity.Nome
import br.com.api.beblueApp.application.infrastructurePort.GeneroRepositoryPort
import br.com.api.beblueApp.infrastructure.repository.GeneroRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class GeneroRepositoryAdapter @Autowired constructor(private var generoRepository: GeneroRepository):GeneroRepositoryPort {

    override fun pesquisarTodos(): List<Genero>? {
        return generoRepository.findAll().toList()
    }

    override fun pesquisarPorNome(nome: Nome): Genero? {
        return generoRepository.findByNome(nome)
    }

    override fun salvar(genero: Genero): Genero {
        return generoRepository.save(genero)
    }


}