package br.com.api.beblueApp.infrastructure.repository

import br.com.api.beblueApp.domain.entity.Ativo
import br.com.api.beblueApp.domain.entity.Usuario
import org.springframework.data.repository.CrudRepository

interface UsuarioRepository:CrudRepository<Usuario, Int> {

    fun findByLoginAndAtivo(login: String, ativo: Ativo = Ativo(1)): Usuario?

}