package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.domain.entity.Artista
import br.com.api.beblueApp.application.infrastructurePort.ArtistaRepositoryPort
import br.com.api.beblueApp.infrastructure.repository.ArtistaRepository
import br.com.api.beblueApp.infrastructure.spotifyClient.api.ArtistaSpotifyClient
import br.com.api.beblueApp.domain.model.ArtistaItemModelResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ArtistaRepositoryAdapter @Autowired constructor(private var artistaRepository: ArtistaRepository,
                                                      private var artistaClient: ArtistaSpotifyClient,
                                                      private var tokenSpotify: String                                                  ): ArtistaRepositoryPort {

    override fun pesquisarArtistaSpotifyPorId(id: String): ArtistaItemModelResponse {
        return artistaClient.pesquisarPorId(tokenSpotify,id)
    }

    override fun consultarPorSpotifyId(spotifyId: String): Artista? {
        return artistaRepository.findBySpotifyId(spotifyId)
    }

    override fun salvar(artista: Artista): Artista {
        return artistaRepository.save(artista)
    }

}