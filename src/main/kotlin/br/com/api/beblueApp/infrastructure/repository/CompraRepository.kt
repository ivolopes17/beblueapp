package br.com.api.beblueApp.infrastructure.repository

import br.com.api.beblueApp.domain.entity.Compra
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface CompraRepository:PagingAndSortingRepository<Compra, Int> {

    @Query("select c from Compra c " +
            " join c.carrinho ca join ca.usuario u " +
            "where u.id = :idUsuario and c.data between :dataInicio and :dataFim order by c.data desc")
    fun findAllByDatasAndUsuario(@Param("dataInicio") dataInicio:Date,
                                 @Param("dataFim") dataFim: Date,
                                 @Param("idUsuario") idUsuario:Int,
                                 paginacao: Pageable): Page<Compra>

    @Query("select c from Compra c " +
            "join c.carrinho ca join ca.usuario u " +
            "where u.id = :idUsuario order by c.data desc")
    fun findAllByUsuarioId(@Param("idUsuario") idUsuario:Int,
                           paginacao: Pageable): Page<Compra>

}