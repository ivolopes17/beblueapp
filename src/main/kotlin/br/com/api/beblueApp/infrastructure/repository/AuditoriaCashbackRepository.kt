package br.com.api.beblueApp.infrastructure.repository

import br.com.api.beblueApp.domain.entity.AuditoriaCashback
import org.springframework.data.repository.CrudRepository

interface AuditoriaCashbackRepository:CrudRepository<AuditoriaCashback, Int> {
}