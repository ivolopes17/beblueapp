package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.domain.entity.Dia
import br.com.api.beblueApp.application.infrastructurePort.DiaRepositoryPort
import br.com.api.beblueApp.infrastructure.repository.DiaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DiaRepositoryAdapter @Autowired constructor(private var diaRepository:DiaRepository):DiaRepositoryPort {

    override fun pesquisarPorId(id: Int): Dia? {
        val result =  diaRepository.findById(id)

        if( result.isPresent ){
            return result.get()
        }

        return null
    }

}