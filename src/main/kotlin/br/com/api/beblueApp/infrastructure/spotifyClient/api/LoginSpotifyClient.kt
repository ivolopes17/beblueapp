package br.com.api.beblueApp.infrastructure.spotifyClient.api

import br.com.api.beblueApp.infrastructure.spotifyClient.configutation.FeignClientConfiguration
import br.com.api.beblueApp.infrastructure.spotifyClient.configutation.FeignSimpleEncoderConfig
import br.com.api.beblueApp.domain.model.LoginModelRequest
import br.com.api.beblueApp.domain.model.LoginResponse
import feign.Headers
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody

@FeignClient(name = "loginSpotifyClient", url = "https://accounts.spotify.com/api",
        configuration = [FeignClientConfiguration::class, FeignSimpleEncoderConfig::class])
interface LoginSpotifyClient {

    @PostMapping("/token", consumes = [MediaType.APPLICATION_FORM_URLENCODED_VALUE])
    @Headers("Content-Type: userInterface/x-www-form-urlencoded")
    fun autenticar(@RequestBody model: LoginModelRequest): LoginResponse

}