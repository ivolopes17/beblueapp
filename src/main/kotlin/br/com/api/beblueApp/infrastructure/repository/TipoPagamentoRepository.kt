package br.com.api.beblueApp.infrastructure.repository

import br.com.api.beblueApp.domain.entity.TipoPagamento
import org.springframework.data.repository.CrudRepository

interface TipoPagamentoRepository:CrudRepository<TipoPagamento, Int> {
}