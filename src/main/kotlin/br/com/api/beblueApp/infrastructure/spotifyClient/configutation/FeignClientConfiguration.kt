package br.com.api.beblueApp.infrastructure.spotifyClient.configutation

import feign.auth.BasicAuthRequestInterceptor
import org.springframework.context.annotation.Bean

class FeignClientConfiguration {

    @Bean
    fun basicAuthRequestInterceptor(): BasicAuthRequestInterceptor {
        return BasicAuthRequestInterceptor("b1df09a2a44e4bf4b355ac2e8db0e2fc", "9361ec62e3bd4024a3736bb26af88345")
    }

}