package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.domain.entity.Usuario
import br.com.api.beblueApp.application.infrastructurePort.UsuarioRepositoryPort
import br.com.api.beblueApp.infrastructure.repository.UsuarioRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UsuarioRepositoryAdapter @Autowired constructor(private var repository:UsuarioRepository):UsuarioRepositoryPort {

    override fun findByLogin(login: String): Usuario? {
        return repository.findByLoginAndAtivo(login)
    }

}