package br.com.api.beblueApp.infrastructure.spotifyClient.api

import br.com.api.beblueApp.infrastructure.spotifyClient.configutation.FeignSimpleEncoderConfig
import br.com.api.beblueApp.domain.model.ArtistaItemModelResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestHeader

@FeignClient(name = "artistaSpotifyClient", url = "https://api.spotify.com/v1/artists",
        configuration = [FeignSimpleEncoderConfig::class])
interface ArtistaSpotifyClient {

    @GetMapping("/{id}")
    fun pesquisarPorId(@RequestHeader("Authorization") token: String,
                       @PathVariable(value = "id") id:String): ArtistaItemModelResponse

}