package br.com.api.beblueApp.infrastructure.repository

import br.com.api.beblueApp.domain.entity.Dia
import br.com.api.beblueApp.domain.entity.Genero
import br.com.api.beblueApp.domain.entity.PercentualCashback
import org.springframework.data.repository.CrudRepository

interface PercentualCashbackRepository:CrudRepository<PercentualCashback, Int> {

    fun findByDiaAndGenero(dia: Dia, genero: Genero): PercentualCashback?

}