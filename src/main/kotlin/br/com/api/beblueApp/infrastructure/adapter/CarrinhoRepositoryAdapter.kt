package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.domain.entity.Ativo
import br.com.api.beblueApp.domain.entity.Carrinho
import br.com.api.beblueApp.application.infrastructurePort.CarrinhoRepositoryPort
import br.com.api.beblueApp.infrastructure.repository.CarrinhoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CarrinhoRepositoryAdapter @Autowired constructor(private var carrinhoRepository: CarrinhoRepository):CarrinhoRepositoryPort {

    override fun findByAtivoAndUsuarioId(ativo:Ativo, usuarioId:Int): List<Carrinho> {
        return carrinhoRepository.pesquisarPorAtivoAndUsuario(ativo, usuarioId)
    }

    override fun pesquisarPorId(id: Int, ativo: Ativo): Carrinho? {
        return carrinhoRepository.findByIdAndAtivo(id, ativo)
    }

    override fun salvar(carrinho: Carrinho): Carrinho {
        return carrinhoRepository.save(carrinho)
    }

}