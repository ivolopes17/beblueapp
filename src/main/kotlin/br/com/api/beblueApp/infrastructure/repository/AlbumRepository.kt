package br.com.api.beblueApp.infrastructure.repository

import br.com.api.beblueApp.domain.entity.Album
import org.springframework.data.repository.CrudRepository

interface AlbumRepository:CrudRepository<Album, Int> {

    fun findBySpotifyId(spotifyId: String) : Album?

}