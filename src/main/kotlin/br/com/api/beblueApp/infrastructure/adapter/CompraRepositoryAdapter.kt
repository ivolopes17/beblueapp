package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.domain.entity.Compra
import br.com.api.beblueApp.application.infrastructurePort.CompraRepositoryPort
import br.com.api.beblueApp.infrastructure.repository.CompraRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.util.*

@Service
class CompraRepositoryAdapter @Autowired constructor(private var compraRepository: CompraRepository):CompraRepositoryPort {

    override fun findById(id: Int): Compra? {
        val result = compraRepository.findById(id)

        if( result.isPresent ){
            return result.get()
        }

        return null
    }

    override fun findAllByDatasAndUsuario(dataInicio: Date, dataFim: Date, idUsuario: Int, paginacao: Pageable): Page<Compra> {
        val calendar = Calendar.getInstance()
        calendar.time = dataFim
        calendar.set(Calendar.HOUR, 23)
        calendar.set(Calendar.MINUTE, 59)
        calendar.set(Calendar.SECOND, 59)

        return compraRepository.findAllByDatasAndUsuario(dataInicio, calendar.time, idUsuario, paginacao)
    }

    override fun findAllByUsuarioId(idUsuario: Int, paginacao: Pageable): Page<Compra> {
        return compraRepository.findAllByUsuarioId(idUsuario, paginacao)
    }

    override fun salvar(compra: Compra): Compra {
        return compraRepository.save(compra)
    }


}