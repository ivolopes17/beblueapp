package br.com.api.beblueApp.infrastructure.repository

import br.com.api.beblueApp.domain.entity.Artista
import org.springframework.data.repository.CrudRepository

interface ArtistaRepository:CrudRepository<Artista, Int> {

    fun findBySpotifyId(spotifyId:String): Artista?

}