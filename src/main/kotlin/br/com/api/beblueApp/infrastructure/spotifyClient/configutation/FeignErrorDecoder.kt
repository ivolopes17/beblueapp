package br.com.api.beblueApp.infrastructure.spotifyClient.configutation

import br.com.api.beblueApp.userInterface.exceptions.BeblueBadRequestException
import br.com.api.beblueApp.userInterface.exceptions.BeblueInternalErrorException
import feign.FeignException.errorStatus
import feign.Response
import feign.codec.ErrorDecoder

class FeignErrorDecoder : ErrorDecoder {

    override fun decode(methodKey: String, response: Response): Exception {
        if (response.status() in 400..499) {
            return BeblueBadRequestException(
                    response.reason()
            )
        }
        return if (response.status() in 500..599) {
            BeblueInternalErrorException(
                    response.reason()
            )
        } else errorStatus(methodKey, response)
    }
}