package br.com.api.beblueApp.infrastructure.repository

import br.com.api.beblueApp.domain.entity.Dia
import org.springframework.data.repository.CrudRepository

interface DiaRepository:CrudRepository<Dia, Int> {
}