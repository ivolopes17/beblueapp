package br.com.api.beblueApp.infrastructure.spotifyClient.api

import br.com.api.beblueApp.infrastructure.spotifyClient.configutation.FeignSimpleEncoderConfig
import br.com.api.beblueApp.domain.model.PesquisaModelResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestParam

@FeignClient(name = "pesquisarSpotifyClient", url = "https://api.spotify.com/v1/search",
        configuration = [FeignSimpleEncoderConfig::class])
interface PesquisarSpotifyClient {

    @GetMapping
    fun pesquisar(@RequestHeader("Authorization") token: String,
                  @RequestParam q:String,
                  @RequestParam type: String,
                  @RequestParam limit:Int,
                  @RequestParam offset:Int): PesquisaModelResponse
}