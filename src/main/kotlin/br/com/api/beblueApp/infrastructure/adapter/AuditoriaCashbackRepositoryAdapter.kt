package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.domain.entity.AuditoriaCashback
import br.com.api.beblueApp.application.infrastructurePort.AuditoriaCashbackRepositoryPort
import br.com.api.beblueApp.infrastructure.repository.AuditoriaCashbackRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AuditoriaCashbackRepositoryAdapter @Autowired constructor(private var auditoriaCashbackRepository: AuditoriaCashbackRepository):AuditoriaCashbackRepositoryPort {

    override fun salvar(auditoriaCashback: AuditoriaCashback): AuditoriaCashback {
        return auditoriaCashbackRepository.save(auditoriaCashback)
    }

}