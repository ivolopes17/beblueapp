package br.com.api.beblueApp.infrastructure.adapter

import br.com.api.beblueApp.application.infrastructurePort.LoginRepositoryPort
import br.com.api.beblueApp.domain.model.LoginModelRequest
import br.com.api.beblueApp.domain.model.LoginResponse
import br.com.api.beblueApp.infrastructure.spotifyClient.api.LoginSpotifyClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class LoginRepositoryAdapter:LoginRepositoryPort {

    @Autowired
    private lateinit var client: LoginSpotifyClient

    override fun autenticar(): LoginResponse {
        val model = LoginModelRequest()
        model.grant_type = "client_credentials"
        return client.autenticar(model)
    }


}