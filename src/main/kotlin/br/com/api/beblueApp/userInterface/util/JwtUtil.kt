package br.com.api.beblueApp.userInterface.util

import br.com.api.beblueApp.userInterface.security.SecurityConstants
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import java.util.*
import javax.servlet.http.HttpServletResponse


object JwtUtil {

    fun getLoginAuth(token: String): Authentication? {

        val usuario = Jwts.parser()
                .setSigningKey(SecurityConstants.SECRET.toByteArray())
                .parseClaimsJws(token.replace(SecurityConstants.TOKEN_PREFIX, ""))
                .body
                .subject

        if( usuario != null){
            return UsernamePasswordAuthenticationToken(usuario, null, Collections.emptyList())
        }

        return null
    }

    fun getUsuarioAuth(token: String): String? {

        return Jwts.parser()
                .setSigningKey(SecurityConstants.SECRET.toByteArray())
                .parseClaimsJws(token.replace(SecurityConstants.TOKEN_PREFIX, ""))
                .body
                .subject

    }

    fun addAuthentication(res: HttpServletResponse, username: String) {

        val token = generateToken(username)
        res.addHeader(SecurityConstants.HEADER_STRING, token)

    }

    @Suppress("DEPRECATION")
    fun generateToken(username: String):String{
        val jwt = Jwts.builder()
                .setSubject(username)
                .setExpiration(Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.SECRET.toByteArray())
                .compact()
        return SecurityConstants.TOKEN_PREFIX + " " + jwt
    }

}
