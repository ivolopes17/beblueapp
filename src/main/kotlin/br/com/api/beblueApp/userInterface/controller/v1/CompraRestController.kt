package br.com.api.beblueApp.userInterface.controller.v1

import br.com.api.beblueApp.userInterface.controller.model.CompraRequest
import br.com.api.beblueApp.userInterface.exceptions.BeblueBadRequestException
import br.com.api.beblueApp.application.dto.CompraDto
import br.com.api.beblueApp.application.service.CompraService
import br.com.api.beblueApp.application.uiPort.CompraUIPort
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/private/v1/compras")
class CompraRestController: AbstractController(), CompraUIPort {

    @Autowired
    private lateinit var compraService: CompraService

    @GetMapping
    override fun consultarPorDatas(@RequestHeader("Authorization") authorization: String,
                          @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") dataInicio:Date?,
                            @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") dataFim:Date?,
                          @RequestParam(required = false) limit:Int?,
                          @RequestParam(required = false) page:Int?):ResponseEntity<List<CompraDto>>{

        val login = getLoginAuth(authorization)

        val result = compraService.pesquisarCompras(login!!, dataInicio, dataFim, page, limit)

        val httpHeaders = HttpHeaders()
        httpHeaders.add("Total", result.total.toString())

        return ResponseEntity(result.compras, httpHeaders, HttpStatus.OK)
    }

    @GetMapping("/{id}")
    override fun consultarPorId(@PathVariable id:Int):ResponseEntity<CompraDto>?{

        if( id <= 0 ){
            throw BeblueBadRequestException("O id tem que ser maior que 0")
        }

        val compra = compraService.pesquisarPorId(id) ?: return ResponseEntity(HttpStatus.NOT_FOUND)

        return ResponseEntity(compra, HttpStatus.OK)
    }

    @PostMapping
    override fun registrarCompra(@RequestBody compra:CompraRequest):ResponseEntity<CompraDto>{

        val compra1 = compraService.realizarCompra(compra.idCarrinho!!, compra.idTipoPagamento!!, compra.qtdVezesPagamento!!)

        return ResponseEntity(compra1, HttpStatus.CREATED)

    }

}