package br.com.api.beblueApp.userInterface.controller.v1

import br.com.api.beblueApp.userInterface.exceptions.BeblueBadRequestException
import br.com.api.beblueApp.userInterface.controller.model.AdicionarCarrinhoRequest
import br.com.api.beblueApp.application.dto.CarrinhoDto
import br.com.api.beblueApp.application.service.CarrinhoService
import br.com.api.beblueApp.application.uiPort.CarrinhoUIPort
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/private/v1/carrinho")
class CarrinhoRestController: CarrinhoUIPort, AbstractController() {

    @Autowired
    private lateinit var carrinhoService: CarrinhoService

    @PostMapping
    override fun adicionarAlbumCarrinho(@RequestHeader("Authorization") authorization: String,
                               @RequestBody album:AdicionarCarrinhoRequest):ResponseEntity<CarrinhoDto>{

        val login = getLoginAuth(authorization)

        val carrinho = carrinhoService.adicionarAlbumCarrinho(album.spotifyId!!, login!!, album.quantidade, 40.0)

        return ResponseEntity(carrinho, HttpStatus.CREATED)
    }

    @PutMapping
    override fun renovarTempoCarrinho(@RequestBody carrinho: CarrinhoDto): ResponseEntity<CarrinhoDto>{

        carrinhoService.renovarTempo(carrinho.id)

        return ResponseEntity(HttpStatus.OK)
    }

}