package br.com.api.beblueApp.userInterface.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
class BeblueInternalErrorException(exception: String) : RuntimeException(exception) {
}