package br.com.api.beblueApp.userInterface.configuration

import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport
import springfox.documentation.service.ApiInfo
import springfox.documentation.builders.ApiInfoBuilder
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.context.annotation.Configuration
import springfox.documentation.swagger2.annotations.EnableSwagger2

@Configuration
@EnableSwagger2
class SwaggerConfig:WebMvcConfigurationSupport() {
	
	override fun addResourceHandlers(registry:ResourceHandlerRegistry){
		registry.addResourceHandler("swagger-ui.html")
		.addResourceLocations("classpath:/META-INF/resources/")
		
		registry.addResourceHandler("/webjars/**")
        .addResourceLocations("classpath:/META-INF/resources/webjars/")
	}
	
}