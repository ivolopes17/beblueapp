package br.com.api.beblueApp.userInterface.controller.v1

import br.com.api.beblueApp.application.service.AlbumService
import br.com.api.beblueApp.application.uiPort.AlbumUIPort
import br.com.api.beblueApp.domain.model.AlbumItemModelResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/private/v1/album")
class AlbumRestController: AlbumUIPort, AbstractController(){

    @Autowired
    private lateinit var albumService: AlbumService

    @GetMapping
    override fun consultarPorNome(@RequestParam(required = true) nome:String,
                            @RequestParam(required = false) limit:Int?,
                                @RequestParam(required = false) offset:Int?): ResponseEntity<List<AlbumItemModelResponse>> {

        val pesquisaModelResponse = albumService.pesquisarAlbumSpotify(nome, limit, offset)

        val httpHeaders = HttpHeaders()
        httpHeaders.add("Total", pesquisaModelResponse.albums?.total?.toString() ?: "0")

        return ResponseEntity(pesquisaModelResponse.albums?.items, httpHeaders, HttpStatus.OK)

    }

}