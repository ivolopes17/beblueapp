package br.com.api.beblueApp.userInterface.exceptions

import java.util.*

class ErrorDetails{
    var timestamp: Date? = null

    var message: String? = null

    var details: String? = null

    constructor(timestamp: Date, message: String, details: String){
        this.details = details
        this.message = message
        this.timestamp = timestamp
    }
}