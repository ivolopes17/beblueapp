package br.com.api.beblueApp.userInterface.security

object SecurityConstants {
    val SECRET = "SecretKeyToGenJWTsAppBeblueApiPassword12938#$1289329732SJjkhdfkhdfkddf%%ldflkjdflkjfdljkdf"
    val EXPIRATION_TIME: Long = 1200000
    val TOKEN_PREFIX = "Bearer "
    val HEADER_STRING = "Authorization"
}
