package br.com.api.beblueApp.userInterface.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.BAD_REQUEST)
class BeblueBadRequestException (exception: String) : RuntimeException(exception){
}