package br.com.api.beblueApp.userInterface.controller.v1

import br.com.api.beblueApp.userInterface.util.JwtUtil
import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired

abstract class AbstractController {

    companion object {
        const val PAGE = 0
        const val LIMIT = 20
    }

    protected fun getLoginAuth(token:String):String?{
        return JwtUtil.getUsuarioAuth(token)
    }

}