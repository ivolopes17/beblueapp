package br.com.api.beblueApp.userInterface.controller.model

class AdicionarCarrinhoRequest {

    var spotifyId: String? = null

    var quantidade: Int = 1
}