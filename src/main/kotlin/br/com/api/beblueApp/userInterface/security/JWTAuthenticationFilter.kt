package br.com.api.beblueApp.userInterface.security

import br.com.api.beblueApp.userInterface.util.JwtUtil
import br.com.api.beblueApp.application.service.LoginService
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.GenericFilterBean
import java.io.IOException
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest


class JWTAuthenticationFilter(var dataExpiracao:Date, var tokenSpotify: String, var loginService: LoginService) : GenericFilterBean() {

    @Throws(IOException::class, ServletException::class)
    override fun doFilter(request: ServletRequest, response: ServletResponse, filterChain: FilterChain) {

        val req = request as HttpServletRequest
        val token = req.getHeader(SecurityConstants.HEADER_STRING)

        if( token != null && token.isNotEmpty()) {

            val authentication:Authentication?
            try {
                authentication = JwtUtil.getLoginAuth(token)
            }catch (e:Exception){
                filterChain.doFilter(request, response)
                return
            }

            if( dataExpiracao.before(Date()) ){
                getToken()
            }

            SecurityContextHolder.getContext().authentication = authentication
            filterChain.doFilter(request, response)

        }else{
            filterChain.doFilter(request, response)
        }
    }

    private fun getToken(){
        val response = loginService.autenticar()
        dataExpiracao = Date(System.currentTimeMillis() + ((response.expires_in!!-120) * 1000) )
        tokenSpotify = SecurityConstants.TOKEN_PREFIX +response.access_token
    }

}