package br.com.api.beblueApp.userInterface.security

import br.com.api.beblueApp.application.service.LoginService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service


@Service
class UserDetailsServiceImpl(@field:Autowired
                             private val loginService: LoginService) : UserDetailsService {

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(login: String): UserDetails {
        val usuario = loginService.findByLogin(login) ?: throw UsernameNotFoundException(login)
        return User(usuario.login, usuario.senha, emptyList())
    }
}
