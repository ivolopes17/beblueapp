package br.com.api.beblueApp.userInterface.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
class BeblueNotAcceptableException(exception: String) : RuntimeException(exception)