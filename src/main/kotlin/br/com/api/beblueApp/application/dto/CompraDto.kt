package br.com.api.beblueApp.application.dto

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*

class CompraDto {

    var id:Int? = null

    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    var data: Date? = null

    var valorTotal: Double? = null

    var cashback: Double? = null

    var qtdVezesPagamento: Int? = null

    var tipoPagamento: String? = null

}