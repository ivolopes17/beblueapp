package br.com.api.beblueApp.application.infrastructurePort

import br.com.api.beblueApp.domain.entity.Artista
import br.com.api.beblueApp.domain.model.ArtistaItemModelResponse

interface ArtistaRepositoryPort {

    fun pesquisarArtistaSpotifyPorId(id: String): ArtistaItemModelResponse

    fun consultarPorSpotifyId(spotifyId: String): Artista?

    fun salvar(artista: Artista): Artista

}