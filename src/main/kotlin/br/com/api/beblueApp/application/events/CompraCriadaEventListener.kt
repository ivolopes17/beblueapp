package br.com.api.beblueApp.application.events

import br.com.api.beblueApp.application.infrastructurePort.AuditoriaCashbackRepositoryPort
import br.com.api.beblueApp.domain.enums.TipoCashbackAudit
import br.com.api.beblueApp.domain.factory.AuditoriaCashbackFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import java.util.*

@Component
class CompraCriadaEventListener @Autowired constructor(private var auditoriaRepository: AuditoriaCashbackRepositoryPort): ApplicationListener<CompraCriadaEvent> {

    override fun onApplicationEvent(event: CompraCriadaEvent) {
        val calendar = Calendar.getInstance(TimeZone.getTimeZone("America/Sao_Paulo"))
        val compra = event.getCompra()

        if( compra.cashback!! > 0){

            val auditoria = AuditoriaCashbackFactory.criar(calendar.time, compra.cashback, TipoCashbackAudit.ENTRADA, compra)

            auditoriaRepository.salvar(auditoria)
        }
    }

}