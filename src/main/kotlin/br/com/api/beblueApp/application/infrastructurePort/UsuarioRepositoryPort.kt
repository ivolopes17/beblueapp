package br.com.api.beblueApp.application.infrastructurePort

import br.com.api.beblueApp.domain.entity.Usuario

interface UsuarioRepositoryPort {

    fun findByLogin(login: String): Usuario?

}