package br.com.api.beblueApp.application.infrastructurePort

import br.com.api.beblueApp.domain.model.LoginResponse

interface LoginRepositoryPort {

    fun autenticar(): LoginResponse

}