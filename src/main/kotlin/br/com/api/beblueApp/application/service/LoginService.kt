package br.com.api.beblueApp.application.service

import br.com.api.beblueApp.application.infrastructurePort.LoginRepositoryPort
import br.com.api.beblueApp.application.infrastructurePort.UsuarioRepositoryPort
import br.com.api.beblueApp.domain.entity.Usuario
import br.com.api.beblueApp.domain.model.LoginResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional

@Service
class LoginService @Autowired constructor(private var loginRepository:LoginRepositoryPort,
                                          private var usuarioRepository: UsuarioRepositoryPort){

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    fun findByLogin(login: String): Usuario? {
        return usuarioRepository.findByLogin(login)
    }

    fun autenticar(): LoginResponse {
        return loginRepository.autenticar()
    }

}