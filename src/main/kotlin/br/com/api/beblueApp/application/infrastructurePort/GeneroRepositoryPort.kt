package br.com.api.beblueApp.application.infrastructurePort

import br.com.api.beblueApp.domain.entity.Genero
import br.com.api.beblueApp.domain.entity.Nome

interface GeneroRepositoryPort {

    fun pesquisarTodos(): List<Genero>?

    fun pesquisarPorNome(nome: Nome): Genero?

    fun salvar(genero: Genero): Genero

}