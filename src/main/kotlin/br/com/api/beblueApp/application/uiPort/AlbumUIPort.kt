package br.com.api.beblueApp.application.uiPort

import br.com.api.beblueApp.domain.model.AlbumItemModelResponse
import org.springframework.http.ResponseEntity

interface AlbumUIPort {

    fun consultarPorNome(nome:String, limit:Int?, offset:Int?): ResponseEntity<List<AlbumItemModelResponse>>

}