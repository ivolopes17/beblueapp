package br.com.api.beblueApp.application.infrastructurePort

import br.com.api.beblueApp.domain.entity.TipoPagamento

interface TipoPagamentoRepositoryPort {

    fun pesquisarTodos():List<TipoPagamento>

    fun pesquisarPorId(id:Int): TipoPagamento?

}