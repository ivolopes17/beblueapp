package br.com.api.beblueApp.application.service

import br.com.api.beblueApp.application.dto.CompraDto
import br.com.api.beblueApp.application.dto.CompraResponseDto
import br.com.api.beblueApp.application.events.CompraCriadaEvent
import br.com.api.beblueApp.application.infrastructurePort.*
import br.com.api.beblueApp.domain.entity.Compra
import br.com.api.beblueApp.domain.entity.Dia
import br.com.api.beblueApp.domain.factory.CompraFactory
import br.com.api.beblueApp.userInterface.controller.v1.AbstractController
import br.com.api.beblueApp.userInterface.exceptions.BeblueBadRequestException
import br.com.api.beblueApp.userInterface.exceptions.BeblueInternalErrorException
import br.com.api.beblueApp.userInterface.exceptions.BeblueNotAcceptableException
import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEventPublisher
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.util.*
import java.util.stream.Collectors

@Service
class CompraService @Autowired constructor(private var compraRepository: CompraRepositoryPort,
                                           private var carrinhoRepository: CarrinhoRepositoryPort,
                                           private var tipoPagamentoRepository: TipoPagamentoRepositoryPort,
                                           private var percentualRepository: PercentualCashbackRepositoryPort,
                                           private var diaRepository: DiaRepositoryPort,
                                           private var usuarioRepository: UsuarioRepositoryPort,
                                           private var applicationEventPublisher:ApplicationEventPublisher,
                                           private var modelMapper: ModelMapper) {

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    fun pesquisarPorId(id:Int): CompraDto? {
        val result = compraRepository.findById(id)

        return if (result != null) {
            convertToDto(result)
        }else{
            null
        }

    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    fun pesquisarCompras(login:String, dataInicio: Date?, dataFim: Date?, page: Int?, limit: Int?):CompraResponseDto {

        val pageAux = page ?: AbstractController.PAGE
        val auxLimit = limit ?: AbstractController.LIMIT

        if( auxLimit < 1 ){
            throw BeblueBadRequestException("O parâmetro limit deve ser maior que 0")
        }

        if( pageAux < 0 ){
            throw BeblueBadRequestException("O parâmetro page deve ser maior ou igual a 0")
        }

        return if( dataFim != null && dataInicio != null) {

            if(dataFim < dataInicio ){
                throw BeblueBadRequestException("O campo dataFim tem que ser maior que o campo dataInicio")
            }

            pesquisarPorDatas(login, dataInicio, dataFim, pageAux, auxLimit)

        }else{
            pesquisarTodos(login, pageAux, auxLimit)
        }

    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    fun pesquisarPorDatas(login:String, dataInicio: Date, dataFim: Date, page: Int, limit: Int):CompraResponseDto {

        //pesquisar o usuário pelo login
        val  usuario = usuarioRepository.findByLogin(login) ?: throw BeblueBadRequestException("O usuário não existe")

        val paginacao = PageRequest.of(page, limit)

        val compras = compraRepository.findAllByDatasAndUsuario(dataInicio, dataFim, usuario.id!!, paginacao)

        val result = getCompraDtoList(compras)

        return CompraResponseDto(compras.totalElements.toInt(), result!!)
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    fun pesquisarTodos(login: String, page: Int, limit: Int): CompraResponseDto {
        //pesquisar o usuário pelo login
        val  usuario = usuarioRepository.findByLogin(login) ?: throw BeblueBadRequestException("O usuário não existe")

        val paginacao = PageRequest.of(page, limit)

        val compras = compraRepository.findAllByUsuarioId(usuario.id!!, paginacao)

        val result = getCompraDtoList(compras)

        return CompraResponseDto(compras.totalElements.toInt(), result!!)

    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun realizarCompra(idCarrinho:Int, idTipoPagamento:Int, qtdVezesPagamento: Int): CompraDto {

        if( idCarrinho <= 0){
            throw BeblueBadRequestException("O campo do idCarrinho está vazio ou é inválido")
        }

        if( idTipoPagamento <= 0){
            throw BeblueBadRequestException("O campo do idTipoPagamento está vazio ou é inválido")
        }

        if( qtdVezesPagamento <= 0){
            throw BeblueBadRequestException("O campo do qtdVezesPagamento está vazio ou é inválido")
        }

        //Pesquisar o tipo de pagamento
        val tipoPagamento = tipoPagamentoRepository.pesquisarPorId(idTipoPagamento) ?: throw BeblueNotAcceptableException("Tipo de pagamento não encontrado")

        //Pesquisar o carrinho
        val carrinho = carrinhoRepository.pesquisarPorId(idCarrinho) ?: throw BeblueNotAcceptableException("Carrinho não encontrado")

        val calendar = Calendar.getInstance(TimeZone.getTimeZone("America/Sao_Paulo"))

        var compra = CompraFactory.criar(calendar.time, qtdVezesPagamento, tipoPagamento, carrinho)

        val dia = diaRepository.pesquisarPorId(calendar.get(Calendar.DAY_OF_WEEK)) ?: throw Exception("Ocorreu algum problema ao realizar a compra")

        //realizar a compra
        compra = salvarCompraCarrinho(compra, dia)

        //dispara um evento de que um compra foi realizada
        val compraEvent = CompraCriadaEvent(this, compra)
        applicationEventPublisher.publishEvent(compraEvent)

        return convertToDto(compra)
    }

    @Transactional(propagation = Propagation.REQUIRED)
    fun salvar(compra: Compra): Compra {
        return compraRepository.salvar(compra)
    }

    private fun salvarCompraCarrinho(compra: Compra, dia: Dia): Compra {

        compra.getCarrinhoAlbuns()?.forEachIndexed { _, c ->

            val valorAlbum = c.calcularValorTotal()

            c.album?.artista?.generos?.forEach {

                val percentualAux = percentualRepository.pesquisarPorDiaGenero(dia, it.genero!!)

                if( percentualAux != null ) {
                    compra.adicionarCashback(valorAlbum,percentualAux.percentual!!)
                    return@forEach
                }

            }

        } ?: throw BeblueInternalErrorException("A compra não possui album")

        compra.calcularValorTotal()

        compra.aumentarSaldoUsuario(compra.cashback!!)
        compra.inativarCarrinho()

        return this.salvar(compra)

    }

    private fun convertToDto(entity: Compra): CompraDto {
        return modelMapper.map(entity, CompraDto::class.java)
    }

    private fun getCompraDtoList(compras: Page<Compra>): List<CompraDto>?{
        return compras.content.stream()
                .map { compra -> convertToDto(compra) }
                ?.collect(Collectors.toList<CompraDto>())
    }

}