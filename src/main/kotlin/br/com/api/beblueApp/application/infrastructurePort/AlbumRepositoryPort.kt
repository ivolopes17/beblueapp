package br.com.api.beblueApp.application.infrastructurePort

import br.com.api.beblueApp.domain.entity.Album
import br.com.api.beblueApp.domain.model.AlbumItemModelResponse
import br.com.api.beblueApp.domain.model.PesquisaModelResponse

interface AlbumRepositoryPort {

    fun pesquisarAlbumSpotify(nome:String, limit:Int, offset:Int): PesquisaModelResponse

    fun pesquisarAlbumSpotifyPorId(id:String): AlbumItemModelResponse

    fun salvar(album: Album): Album

    fun consultarPorSpotifyId(spotifyId:String): Album?

}