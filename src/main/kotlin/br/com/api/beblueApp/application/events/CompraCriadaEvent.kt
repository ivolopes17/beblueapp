package br.com.api.beblueApp.application.events

import br.com.api.beblueApp.domain.entity.Compra
import org.springframework.context.ApplicationEvent

class CompraCriadaEvent(source: Any, compraAux: Compra): ApplicationEvent(source) {

    private var compra: Compra = compraAux

    fun getCompra():Compra{
        return compra
    }

}