package br.com.api.beblueApp.application.util

import java.util.*
import java.util.Calendar



abstract class DateUtil{

    companion object{

        fun isSameDay(data1: Date, data2: Date): Boolean{

            val calendar1 = Calendar.getInstance()
            calendar1.time = data1

            val calendar2 = Calendar.getInstance()
            calendar2.time = data2

            return isSameDay(calendar1, calendar2)
        }

        fun isSameDay(cal1: Calendar, cal2: Calendar): Boolean {

            return cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH) &&
                    cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                    cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR)
        }

    }

}