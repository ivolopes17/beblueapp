package br.com.api.beblueApp.application.infrastructurePort

import br.com.api.beblueApp.domain.entity.Dia

interface DiaRepositoryPort {

    fun pesquisarPorId(id: Int): Dia?

}