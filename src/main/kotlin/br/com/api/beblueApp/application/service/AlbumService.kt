package br.com.api.beblueApp.application.service

import br.com.api.beblueApp.application.infrastructurePort.AlbumRepositoryPort
import br.com.api.beblueApp.domain.entity.Album
import br.com.api.beblueApp.domain.model.PesquisaModelResponse
import br.com.api.beblueApp.userInterface.controller.v1.AbstractController
import br.com.api.beblueApp.userInterface.exceptions.BeblueBadRequestException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional

@Service
class AlbumService @Autowired constructor(private var albumRepository: AlbumRepositoryPort){

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    fun pesquisarAlbumSpotify(nome: String, limit: Int?, offset: Int?):PesquisaModelResponse {

        val limitAux = limit ?: AbstractController.LIMIT
        val offsetAux = offset ?: 0

        if( limitAux < 1 || limitAux > 50 ){
            throw BeblueBadRequestException("O parâmetro limit deve ser maior que 0 e menor que 51")
        }

        if( offsetAux > 10000 ){
            throw BeblueBadRequestException("O parâmetro offset deve ser menor ou igual a 10000")
        }

        return albumRepository.pesquisarAlbumSpotify(nome, limitAux, offsetAux)
    }

    @Transactional(propagation = Propagation.REQUIRED)
    fun salvar(album: Album): Album {
        return albumRepository.salvar(album)
    }

}