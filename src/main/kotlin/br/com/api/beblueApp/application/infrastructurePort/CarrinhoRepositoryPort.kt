package br.com.api.beblueApp.application.infrastructurePort

import br.com.api.beblueApp.domain.entity.Ativo
import br.com.api.beblueApp.domain.entity.Carrinho

interface CarrinhoRepositoryPort {

    fun findByAtivoAndUsuarioId(ativo: Ativo, usuarioId:Int): List<Carrinho>

    fun pesquisarPorId(id:Int, ativo:Ativo = Ativo(1)): Carrinho?

    fun salvar(carrinho: Carrinho): Carrinho

}