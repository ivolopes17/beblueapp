package br.com.api.beblueApp.application.infrastructurePort

import br.com.api.beblueApp.domain.entity.Compra
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import java.util.*

interface CompraRepositoryPort {

    fun findById(id: Int): Compra?

    fun findAllByDatasAndUsuario(dataInicio:Date, dataFim: Date, idUsuario:Int, paginacao: Pageable): Page<Compra>

    fun findAllByUsuarioId(idUsuario:Int, paginacao: Pageable): Page<Compra>

    fun salvar(compra: Compra): Compra

}