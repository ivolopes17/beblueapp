package br.com.api.beblueApp.application.infrastructurePort

import br.com.api.beblueApp.domain.entity.Dia
import br.com.api.beblueApp.domain.entity.Genero
import br.com.api.beblueApp.domain.entity.PercentualCashback

interface PercentualCashbackRepositoryPort {

    fun pesquisarPorDiaGenero(dia: Dia, genero: Genero): PercentualCashback?
    
}