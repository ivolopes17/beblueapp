package br.com.api.beblueApp.application.service

import br.com.api.beblueApp.application.dto.CarrinhoDto
import br.com.api.beblueApp.application.infrastructurePort.*
import br.com.api.beblueApp.domain.entity.*
import br.com.api.beblueApp.domain.factory.*
import br.com.api.beblueApp.domain.model.AlbumItemModelResponse
import br.com.api.beblueApp.domain.model.ArtistaItemModelResponse
import br.com.api.beblueApp.userInterface.exceptions.BeblueBadRequestException
import br.com.api.beblueApp.userInterface.exceptions.BeblueNotAcceptableException
import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class CarrinhoService @Autowired constructor(private var usuarioRepository: UsuarioRepositoryPort,
                                             private var albumRepository: AlbumRepositoryPort,
                                             private var artistaRepository: ArtistaRepositoryPort,
                                             private var generoRepository: GeneroRepositoryPort,
                                             private var repository: CarrinhoRepositoryPort,
                                             private var modelMapper: ModelMapper){

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun adicionarAlbumCarrinho(spotifyId: String, loginUsuario: String, quantidade: Int, valor: Double): CarrinhoDto {

        if( quantidade <= 0){
            throw BeblueBadRequestException("A quantidade tem que ser maior que zero")
        }

        //Validar se o id do spotify é valido. Caso não tenha o album na base local deve incluir
        val albumResponse = pesquisarAlbumSpotify(spotifyId)

        var album = albumRepository.consultarPorSpotifyId(albumResponse.id!!)

        if ( album == null){
            album = adicionarAlbum(albumResponse)
        }

        //pesquisar o usuário pelo login
        val  usuario = pesquisarUsuario(loginUsuario)

        //validar se já tem um carrinho ativo. Se não tiver deverá criar um
        var carrinho = inserirValidando(usuario)

        //Adicionar o album no carrinho
        carrinho = adicionarAlbumCarrinho(carrinho, album, quantidade, valor)

        return this.convertToDto(carrinho)!!
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun renovarTempo(id:Int?) {

        if( id == null || id <= 0 ){
            throw BeblueBadRequestException("O campo id é nulo ou inválido")
        }

        val carrinho = repository.pesquisarPorId(id) ?: throw BeblueNotAcceptableException("Não foi encontrado nenhum carrinho para o id informado")

        carrinho.adicionarTempo()

        salvar(carrinho)
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    fun findByAtivoAndUsuarioId(ativo: Ativo, usuarioId:Int): List<Carrinho> {
        return repository.findByAtivoAndUsuarioId(ativo, usuarioId)
    }

    @Transactional(propagation = Propagation.REQUIRED)
    fun salvar(carrinho: Carrinho): Carrinho {
        return repository.salvar(carrinho)
    }

    private fun novoCarrinho(usuario: Usuario): Carrinho {
        val carrinho = CarrinhoFactory.criar(Date(), usuario)
        return salvar(carrinho)
    }

    private fun pesquisarAlbumSpotify(spotifyId:String): AlbumItemModelResponse {
        val albumResponse = albumRepository.pesquisarAlbumSpotifyPorId(spotifyId)

        if( albumResponse.id == null){
            throw BeblueBadRequestException("O campo spotifyId é inválido")
        }

        return albumResponse
    }

    private fun adicionarAlbum(albumResponse: AlbumItemModelResponse): Album {
        val artistaResponse = pesquisarArtista(albumResponse.artists?.get(0)!!)

        var artista = artistaRepository.consultarPorSpotifyId(artistaResponse?.id!!)

        if( artista == null) {
            artista = ArtistaFactory.criar(artistaResponse)

            artistaResponse.genres.forEach {
                val genero = pesquisarGeneroAdd(it)

                artista!!.adicionarGeneros(ArtistaGeneroFactory.criar(genero, artista))

            }

            artista = artistaRepository.salvar(artista)
        }

        var album = AlbumFactory.criar(albumResponse, artista)
        album = albumRepository.salvar(album)

        return album
    }

    fun pesquisarGeneroAdd(nome:String): Genero {
        var genero = generoRepository.pesquisarPorNome(Nome(nome))

        if( genero == null){
            genero = generoRepository.salvar(GeneroFactory.criar(nome))
        }

        return genero
    }

    fun pesquisarArtista(artistaResponse: ArtistaItemModelResponse): ArtistaItemModelResponse?{

        val response: ArtistaItemModelResponse?

        if(artistaResponse.id != null) {
            response = artistaRepository.pesquisarArtistaSpotifyPorId(artistaResponse.id!!)
        }else{
            throw BeblueBadRequestException("Problema ao recuperar o artista pela api da Spotify")
        }

        return response
    }

    fun pesquisarUsuario(loginUsuario: String): Usuario {
        val usuario = usuarioRepository.findByLogin(loginUsuario) ?: throw BeblueBadRequestException("Usuário inválido")

        return usuario
    }

    fun inserirValidando(usuario: Usuario): Carrinho {
        val lista = findByAtivoAndUsuarioId(Ativo(1), usuario.id!!)
        var carrinho:Carrinho? = null

        if( lista.size == 0 ){
            carrinho = novoCarrinho(usuario)
        }else if( lista.get(0).dataValidade?.before(Date())!! ){
            carrinho = lista.get(0)
            carrinho.inativar()
            salvar(carrinho)

            carrinho = novoCarrinho(usuario)
        }

        return carrinho!!
    }

    private fun adicionarAlbumCarrinho(carrinho: Carrinho, album: Album, quantidade: Int, valor: Double): Carrinho {

        carrinho.adicionarAlbum(album, quantidade, valor)
        salvar(carrinho)
        return carrinho
    }

    private fun convertToDto(entity: Carrinho): CarrinhoDto? {
        return modelMapper.map(entity, CarrinhoDto::class.java)
    }

}