package br.com.api.beblueApp.application.uiPort

import br.com.api.beblueApp.userInterface.controller.model.AdicionarCarrinhoRequest
import br.com.api.beblueApp.application.dto.CarrinhoDto
import org.springframework.http.ResponseEntity

interface CarrinhoUIPort {

    fun adicionarAlbumCarrinho(authorization: String, album: AdicionarCarrinhoRequest): ResponseEntity<CarrinhoDto>

    fun renovarTempoCarrinho(carrinho: CarrinhoDto): ResponseEntity<CarrinhoDto>

}