package br.com.api.beblueApp.application.uiPort

import br.com.api.beblueApp.userInterface.controller.model.CompraRequest
import br.com.api.beblueApp.application.dto.CompraDto
import org.springframework.http.ResponseEntity
import java.util.*

interface CompraUIPort {

    fun consultarPorDatas(authorization: String, dataInicio: Date?, dataFim: Date?, limit:Int?, page:Int?): ResponseEntity<List<CompraDto>>?

    fun consultarPorId(id:Int):ResponseEntity<CompraDto>?

    fun registrarCompra(compra: CompraRequest):ResponseEntity<CompraDto>

}