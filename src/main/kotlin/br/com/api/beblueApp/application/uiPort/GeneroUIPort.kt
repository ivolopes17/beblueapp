package br.com.api.beblueApp.application.uiPort

import br.com.api.beblueApp.application.dto.GeneroDto
import org.springframework.http.ResponseEntity

interface GeneroUIPort {

    fun consultarTodos():ResponseEntity<List<GeneroDto>>

}