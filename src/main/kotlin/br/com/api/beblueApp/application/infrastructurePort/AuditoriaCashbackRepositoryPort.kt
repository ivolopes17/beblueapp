package br.com.api.beblueApp.application.infrastructurePort

import br.com.api.beblueApp.domain.entity.AuditoriaCashback

interface AuditoriaCashbackRepositoryPort {

    fun salvar(auditoriaCashback: AuditoriaCashback): AuditoriaCashback

}