import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	kotlin("plugin.jpa") version "1.2.71"
	id("org.springframework.boot") version "2.1.5.RELEASE"
	id("io.spring.dependency-management") version "1.0.7.RELEASE"
	id("jacoco")
	id("org.sonarqube") version "2.7"
	kotlin("jvm") version "1.2.71"
	kotlin("plugin.spring") version "1.2.71"
}

group = "br.com.api"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

jacoco {
	toolVersion = "0.8.3"
	reportsDir = file("$buildDir/customJacocoReportDir")
}

repositories {
	mavenCentral()
}

extra["springCloudVersion"] = "Greenwich.SR1"

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.flywaydb:flyway-core")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.springframework.cloud:spring-cloud-starter-openfeign")
	implementation("com.auth0:java-jwt:3.8.1")
	implementation("io.jsonwebtoken:jjwt-api:0.10.5")
	implementation("io.jsonwebtoken:jjwt-impl:0.10.5")
	implementation("io.jsonwebtoken:jjwt-jackson:0.10.5")
	implementation("org.springframework.boot:spring-boot-starter-hateoas")
	implementation("org.testng:testng:6.14.3")
	implementation("io.springfox:springfox-swagger2:2.8.0")
    implementation("io.springfox:springfox-swagger-ui:2.8.0")
	implementation("org.modelmapper:modelmapper:2.3.0")
	runtimeOnly("com.h2database:h2")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.springframework.security:spring-security-test")
	testImplementation("org.mockito:mockito-core:2.+")
	testImplementation("org.assertj:assertj-core:3.11.1")
}

dependencyManagement {
	imports {
		mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
	}
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "1.8"
	}
}

val excludesList = arrayOf("br.com.api.beblueApp.domain.enums.*",
		"br.com.api.beblueApp.domain.model.*",
		"br.com.api.beblueApp.application.dto.*",
		"br.com.api.beblueApp.infrastructure.spotifyClient.configutation.*").toMutableList()

tasks{
	jacocoTestReport {
		reports {
			html.isEnabled = true
			xml.isEnabled = true
			csv.isEnabled = false
			html.destination = file("${buildDir}/jacocoHtml")
		}
	}
	check{
		dependsOn(jacocoTestCoverageVerification)
	}
	jacocoTestCoverageVerification {
		violationRules {
			rule {
				element = "CLASS"
				excludes = excludesList

			}
		}
	}
	test{
		finalizedBy(jacocoTestReport)
	}
	afterEvaluate{
		sonarqube{
			properties{
				property("sonar.coverage.jacoco.xmlReportPaths","$buildDir/customJacocoReportDir/test/jacocoTestReport.xml")
				property("sonar.jacoco.reportPath","${buildDir}/jacoco/test.exec")
				property("sonar.java.binaries", "$projectDir/build/classes/kotlin")
				property("sonar.coverage.exclusions",excludesList)

			}
		}
	}
}